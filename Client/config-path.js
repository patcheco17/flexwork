//Bank hours API paths
export const WEEKLY_TASKS_1 = `tasks/weekly?date=`
export const WEEKLY_TASKS_2 = `&userId=`
export const BASE_URL = 'https://hours-bank-api-4hupxege7q-uc.a.run.app/'
export const TASKS = 'tasks/'
export const BALANCE_MONTHLY = `balance/monthly?date=`
export const BALANCE_MONTHLY_USER = `balance/monthly/user?date=`
export const USER_ID = `&userId=`

/*************************************************************/
/* OFFICE MANAGEMENT API */

export const CREATE_ROOM_PATH = `https://office-management-api-4hupxege7q-uc.a.run.app/rooms`
export const GET_ROOMS_PATH = `https://office-management-api-4hupxege7q-uc.a.run.app/rooms`
export const GET_BOOKABLE_ROOMS_PATH = `https://office-management-api-4hupxege7q-uc.a.run.app/rooms/full-bookable`
export const DELETE_ROOM_PATH = `https://office-management-api-4hupxege7q-uc.a.run.app/rooms/`


/*****************************************************************/
/* NOTIFICATIONS API */
export const CREATE_NOTIFICATION_PATH = `https://notifications-api-4hupxege7q-uc.a.run.app/notifications`
export const DELETE_ALL_NOTIFICATIONS_PATH = `https://notifications-api-4hupxege7q-uc.a.run.app/notifications`

/****************************************************************************/
/* BOOKING API */

export const BASE_URL_BOOKING_API ='https://schedule-api-4hupxege7q-uc.a.run.app'
export const BOOKINGS = BASE_URL_BOOKING_API +"/bookings"
export const BOOKINGS_BY_USER = BOOKINGS +"/user"
export const GET_ALL_BOOKINGS_BY_ROOM = BASE_URL_BOOKING_API + "/bookings/rooms"
export const GET_ALL_NORMAL_ROOMS_BOOKINGS_BY_USER = BASE_URL_BOOKING_API +"/bookings/normal-room"
export const POST_FULL_ROOM_BOOKING = BASE_URL_BOOKING_API + "/bookings/full-room"
export const GET_ALL_AVAILABLE_SEATS = BASE_URL_BOOKING_API +"/seats" //desks

/****************************************************************************/
/* USER MANAGEMENT API */
export const CREATE_USER_PATH = `https://user-management-api-4hupxege7q-uc.a.run.app/users`
export const GET_ALL_USERS = `https://user-management-api-4hupxege7q-uc.a.run.app/users`

/*************************************************************/
//Access token
export const ACCESS_TOKEN = `/api/getAccessToken`

//Users API paths
export const USERS_BASE_URL = `https://user-management-api-4hupxege7q-uc.a.run.app/users`

/*************************************************************/
//Statistics API paths
export const STATISTICS_BASE_URL = `https://statistics-api-4hupxege7q-uc.a.run.app`
export const STATISTICS_COLLABORATORS_COUNT = STATISTICS_BASE_URL + `/collaborators-count`
export const STATISTICS_COLLABORATORS_IN_OFFICE = STATISTICS_BASE_URL + `/collaborators-in-office`
export const STATISTICS_INFLUX_PAST_WEEK = STATISTICS_BASE_URL + `/occupancy/week`
export const STATISTICS_INFLUX_PAST_MONTH = STATISTICS_BASE_URL + `/occupancy/month`
export const STATISTICS_ROOM_OCCUPANCY_PAST_MONTH = STATISTICS_BASE_URL + `/room-occupancy/month`


