import {Alert} from "@mui/lab";

export const getInitials = (name = '') => name
    .replace(/\s+/, ' ')
    .split(' ')
    .slice(0, 2)
    .map((v) => v && v[0].toUpperCase())
    .join('');

export function showAlert(message){ return (<Alert severity="error" >{message}</Alert>) }

export function showAlertSuccess(message){ return (<Alert severity="success" >{message}</Alert>) }


export function ShowAlertWithClose({message, failPost, setFailPost}){
    return (<Alert onClose={() => setFailPost(!failPost)} severity="error" >{message}</Alert>)
}


export function getUserRole(user){ return user["http://demozero.net/roles"][0] }

export function extractUserId(oAuthUserId){
    return oAuthUserId.split('|')[1]
}

