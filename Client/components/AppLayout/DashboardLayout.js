import React, {useEffect, useState} from 'react';
import {Sidebar} from "../User/Sidebar/Sidebar";
import {Navbar} from "../User/NavBar/Navbar";
import { styled } from '@mui/material/styles';
import { Box } from '@mui/material';
import Head from "next/head";

import {useUser, withPageAuthRequired} from '@auth0/nextjs-auth0';
const axios = require("axios").default;

const DashboardLayoutRoot = styled('div')(({ theme }) => ({
    display: 'flex',
    flex: '1 1 auto',
    maxWidth: '100%',
    paddingTop: 64,
    [theme.breakpoints.up('lg')]: {
        paddingLeft: 280
    }
}));

export const DashboardLayout = (props) => {
    const { children } = props;

    const [isSidebarOpen, setSidebarOpen] = useState(true);

    return (
        <>
            <Head>
                <title>
                    FlexWork | Office Management
                </title>
            </Head>
            <Navbar onSidebarOpen={() => setSidebarOpen(true)} />
            <DashboardLayoutRoot>
                <Box
                    sx={{
                        backgroundColor: 'divider',
                        display: 'flex',
                        flex: '1 1 auto',
                        flexDirection: 'column',
                        width: '100%'
                    }}
                >
                    {children}
                </Box>
            </DashboardLayoutRoot>

            <Sidebar
                onClose={() => setSidebarOpen(false)}
                open={isSidebarOpen}
            />
        </>
    );
};
