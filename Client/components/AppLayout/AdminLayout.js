import React, {useEffect, useState} from 'react';
import {AdminSidebar} from "../Admin/Sidebar/AdminSidebar";
import {AdminNavbar} from "../Admin/NavBar/AdminNavbar";
import { styled } from '@mui/material/styles';
import { Box } from '@mui/material';
import Head from "next/head";

const axios = require("axios").default;

const AdminLayoutRoot = styled('div')(({ theme }) => ({
    display: 'flex',
    flex: '1 1 auto',
    maxWidth: '100%',
    paddingTop: 64,
    [theme.breakpoints.up('lg')]: {
        paddingLeft: 280
    }
}));

export const AdminLayout = (props) => {
    const { children } = props;

    const [isSidebarOpen, setSidebarOpen] = useState(true);

    return (
        <>
            <Head>
                <title>
                    FlexWork | Office Management
                </title>
            </Head>
            <AdminLayoutRoot>
                <Box
                    sx={{
                    backgroundColor: '#8080a2',
                    display: 'flex',
                    flex: '1 1 auto',
                    flexDirection: 'column',
                    width: '100%'
                }}>
                    {children}
                </Box>
            </AdminLayoutRoot>

            <AdminNavbar onSidebarOpen={() => setSidebarOpen(true)} />
            <AdminSidebar
                onClose={() => setSidebarOpen(false)}
                open={isSidebarOpen}
            />


        </>
    );
};
