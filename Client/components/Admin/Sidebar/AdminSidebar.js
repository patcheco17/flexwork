import { useEffect } from 'react';
import { useRouter } from 'next/router';
import PropTypes from 'prop-types';
import { Box, Drawer, useMediaQuery } from '@mui/material';
import { NavItem } from '../NavBar/nav-item';
import AnalyticsIcon from '@mui/icons-material/Analytics';
import GroupAddIcon from '@mui/icons-material/GroupAdd';
import BusinessIcon from '@mui/icons-material/Business';
import QueryBuilderIcon from '@mui/icons-material/QueryBuilder';
import ForumIcon from '@mui/icons-material/Forum';

const items = [
    {
        href: '/admin/statistics',
        icon: (<AnalyticsIcon fontSize="small" />),
        title: 'Statistics'
    },
    {
        href: '/admin/office-management',
        icon: (<BusinessIcon fontSize="small" />),
        title: 'Office Management'
    },
    {
        href: '/admin/user-management',
        icon: (<GroupAddIcon fontSize="small" />),
        title: 'User Management'
    },
    {
        href: '/admin/hours-bank',
        icon: (<QueryBuilderIcon fontSize="small" />),
        title: 'Hours Bank'
    },
    {
        href: '/admin/communication',
        icon: (<ForumIcon fontSize="small" />),
        title: 'Communication'
    }
];

export const AdminSidebar = (props) => {
    const { open, onClose } = props;
    const router = useRouter();
    const lgUp = useMediaQuery((theme) => theme.breakpoints.up('lg'), {
        defaultMatches: true,
        noSsr: false
    });

    useEffect(
        () => {
            if (!router.isReady) {
                return;
            }

            if (open) {
                onClose?.();
            }
        },
        [router.asPath]
    );


    const content = (
        <>
            <Box sx={{backgroundColor:'neutral.500' , display: 'flex',flexDirection: 'column',height: '100%',boxShadow:'3' }}>
                <div>
                    <Box sx={{mt:8}}>

                    </Box>

                </div>


                <Box sx={{ flexGrow: 1 }}>
                    {items.map((item) => (
                        <NavItem
                            key={item.title}
                            icon={item.icon}
                            href={item.href}
                            title={item.title}
                        />
                    ))}
                </Box>


            </Box>
        </>
    );

    if (lgUp) {
        return (
            <Drawer anchor="left" open
                PaperProps={{
                    sx: {backgroundColor: '#4E4E6A', color: 'neutral.500',borderColor:'neutral.500', width: 280, mt:8}
                }}
                variant="permanent">
                {content}
            </Drawer>
        );
    }

    return (
        <Drawer
            anchor="left"
            onClose={onClose}
            open={open}
            PaperProps={{
                sx: {
                    backgroundColor: 'primary',
                    color: '#FFFFFF',
                    width: 280
                }
            }}
            sx={{ zIndex: (theme) => theme.zIndex.appBar + 100 }}
            variant="temporary"
        >
            {content}
        </Drawer>
    );
};

AdminSidebar.propTypes = {
    onClose: PropTypes.func,
    open: PropTypes.bool
};
