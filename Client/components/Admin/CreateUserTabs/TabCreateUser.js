// ** React Imports
import { useState } from 'react'
// ** MUI Imports
import Grid from '@mui/material/Grid'
import Select from '@mui/material/Select'
import { styled } from '@mui/material/styles'
import MenuItem from '@mui/material/MenuItem'
import TextField from '@mui/material/TextField'
import InputLabel from '@mui/material/InputLabel'
import CardContent from '@mui/material/CardContent'
import FormControl from '@mui/material/FormControl'
import Button from '@mui/material/Button'
//Validator to validate email and password
import validator from "validator";

import axios from "axios";
import {CREATE_USER_PATH} from "../../../config-path";
import {ACCESS_TOKEN, USERS_BASE_URL} from "../../../config-path";

const ImgStyled = styled('img')(({ theme }) => ({
    width: 120,
    height: 120,
    marginRight: theme.spacing(6.25),
    borderRadius: theme.shape.borderRadius
}))

const ButtonStyled = styled(Button)(({ theme }) => ({
    [theme.breakpoints.down('sm')]: {
        width: '100%',
        textAlign: 'center'
    }
}))

const ResetButtonStyled = styled(Button)(({ theme }) => ({
    marginLeft: theme.spacing(4.5),
    [theme.breakpoints.down('sm')]: {
        width: '100%',
        marginLeft: 0,
        textAlign: 'center',
        marginTop: theme.spacing(4)
    }
}))

const TabCreateUser = () => {
    const [imgSrc, setImgSrc] = useState('https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460__340.png')
    const [passwordErrorMessage, setPasswordErrorMessage] = useState('')
    const [emailErrorMessage, setEmailErrorMessage] = useState('')
    const [newUser,setNewUser] = useState({
        firstName: '',
        lastName: '',
        email: '',
        password: '',
        userFunction: '',
        userRole: '',
        img: imgSrc,
        address: ''
    })

    const handleChange = prop => event => {
        const value = event.target.value
        setNewUser({ ...newUser, [prop]: value })

        if (prop === 'password') validatePassword(value)
        if (prop === 'email') validateEmail(value)
    }

    async function handleCreateUser() {
        // Validate user input
        if (!isUserInputValid(newUser)) return

        axios.get(ACCESS_TOKEN)
            .then(res => {
                return res.data
            }).catch(err => {
                alert(err)
                return err
            }).then(token =>{
                axios.post(CREATE_USER_PATH, {
                        firstName: newUser.firstName,
                        lastName: newUser.lastName,
                        email: newUser.email,
                        password: newUser.password,
                        address: newUser.address,
                        position: newUser.userFunction,
                        role: newUser.userRole,
                    },
                    {
                        headers: {
                            Authorization: `Bearer ${token}`
                        }
                    })
                    .then(function (response) {
                        console.log("Response = ", response.data)
                        alert("User created successfully")
                    }).catch(function (error) {
                        if (error.response) {
                            console.log(error.response);
                            alert("Couldn't create user")
                        }
                    })
            })
    }

    const handleReset = () => {
        setNewUser({})
    }

    /*
        ---------------------------------------------------------------------
                                    Aux functions
        ---------------------------------------------------------------------
    */
    function isUserInputValid(newUser){
        let isInputValid = false
        if (anyFieldEmpty(newUser))                     //Validate if there is any field empty
            alert("All fields required")
        else if(emailErrorMessage !== '')               //Validate if email has correct format
            alert("Incorrect email")
        else if (passwordErrorMessage !== '')           //Validate if password is strong enough
            alert("Password to weak")
        else isInputValid = true

        return isInputValid
    }

    function anyFieldEmpty(newUser){
        return newUser.firstName === '' ||
            newUser.lastName === '' ||
            newUser.email === '' ||
            newUser.password === '' ||
            newUser.userFunction === '' ||
            newUser.userRole === '' ||
            newUser.img === '' ||
            newUser.address === ''
    }

    function validateEmail(email){
        if (!validator.isEmail(email))
            setEmailErrorMessage('Invalid email')
        else
            setEmailErrorMessage('')
    }

    function validatePassword(password) {
        if (!validator.isStrongPassword(password, {
            minLength: 8,
            minLowercase: 1,
            minUppercase: 1,
            minNumbers: 1,
            minSymbols: 1
        }))
            setPasswordErrorMessage('Password must contain minimum 8 characters, lowercase, uppercase, number and symbol')
        else
            setPasswordErrorMessage('')
    }

    /*
        ---------------------------------------------------------------------
    */

    return (
        <CardContent>
            <form>
                <Grid container spacing={2}>
                    <Grid item xs={12} sx={{ marginTop: 3, marginBottom: 3 }}>
                        {}
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField
                            required
                            fullWidth
                            label='First Name'
                            placeholder='Name'
                            onChange={handleChange('firstName')}/>
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField
                            required
                            fullWidth
                            label='Last Name'
                            placeholder='Last Name'
                            onChange={handleChange('lastName')}/>
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField
                            required
                            fullWidth
                            type='email'
                            label='Email'
                            placeholder='example@example.com'
                            onChange={handleChange('email')}
                            helperText={emailErrorMessage}
                            error={emailErrorMessage !== ''}
                        />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField
                            required
                            fullWidth
                            type='password'
                            label='Password'
                            placeholder='Password'
                            onChange={handleChange('password')}
                            helperText={passwordErrorMessage}
                            error={passwordErrorMessage !== ''}
                        />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <FormControl fullWidth>
                            <InputLabel>Function</InputLabel>
                            <Select
                                required
                                label='Status'
                                defaultValue='active'
                                placeholder='Select User Function'
                                onChange={handleChange('userFunction')}
                            >
                                <MenuItem value='Developer'>Developer</MenuItem>
                                <MenuItem value='Tester'>Tester</MenuItem>
                                <MenuItem value='Project Manager'>Project Manager</MenuItem>
                            </Select>
                        </FormControl>
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <FormControl fullWidth>
                            <InputLabel>Role</InputLabel>
                            <Select
                                required
                                label='Role'
                                defaultValue='admin'
                                placeholder='Select Role'
                                onChange={handleChange('userRole')}
                            >
                                <MenuItem value='Admin'>Admin</MenuItem>
                                <MenuItem value='user'>User</MenuItem>
                            </Select>
                        </FormControl>
                    </Grid>
                    <Grid item xs={12} sm={12}>
                        <TextField
                            required
                            fullWidth
                            label='Address'
                            placeholder='Address'
                            onChange={handleChange('address')}
                        />
                    </Grid>

                    <Grid item xs={12}>
                        <Button variant='contained' onClick={handleCreateUser} sx={{ marginRight: 3.5 }}>
                            Save Changes
                        </Button>
                        <Button type='reset' variant='outlined' color='secondary' onClick={handleReset}>
                            Reset
                        </Button>
                    </Grid>
                </Grid>
            </form>
        </CardContent>
    )
}

export default TabCreateUser
