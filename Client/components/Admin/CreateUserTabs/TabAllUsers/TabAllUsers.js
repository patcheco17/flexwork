// ** React Imports
import React, {useEffect, useState} from 'react'

// ** MUI Imports
import Box from '@mui/material/Box'

import { AllUsersTable } from "./AllUsersTable";
import axios from "axios";
import {ACCESS_TOKEN, USERS_BASE_URL} from "../../../../config-path";

const TabAllUsers = () => {
    const [users, setUsers] = useState([])

    useEffect(() => {
        axios.get(ACCESS_TOKEN)
            .then(res => {
                return res.data
            }).catch(err => {
                alert("Something went wrong")
                console.log(err)
                return err
            }).then(token => {
                axios.get(USERS_BASE_URL, {
                    headers: {
                        Authorization: `Bearer ${token}`
                    }})
                    .then(res => {
                        const usersReturned = res.data.properties.users;
                        setUsers( usersReturned );
                    })
            })
    }, [])

    return (
        <>
            <Box sx={{ mt: 3 }}>
                <AllUsersTable users={users} changeUsers={setUsers} />
            </Box>
        </>
    )
}

export default TabAllUsers
