import React, { useState } from 'react';
import PerfectScrollbar from 'react-perfect-scrollbar';
import { useRouter } from 'next/router'
import PropTypes from 'prop-types';
import {
    Box,
    Card,
    Table,
    TableBody,
    TableCell,
    TableHead,
    TablePagination,
    TableRow,
    Typography
} from '@mui/material';
import IconButton from "@mui/material/IconButton";
import DeleteIcon from '@mui/icons-material/Delete';
import InfoOutlinedIcon from '@mui/icons-material/InfoOutlined';
import axios from "axios";
import Avatar from "@mui/material/Avatar";
import {getInitials} from "../../../../utils/utils";
import {ACCESS_TOKEN, USERS_BASE_URL} from "../../../../config-path";

export const AllUsersTable = ({ users, changeUsers, ...rest }) => {
    const [limit, setLimit] = useState(10);
    const [page, setPage] = useState(0);
    const router = useRouter()

    const handleLimitChange = (event) => {
        setLimit(event.target.value);
    };

    const handlePageChange = (event, newPage) => {
        setPage(newPage);
    };

    function removeUser(userId){
        axios.get(ACCESS_TOKEN)
            .then(res => {
                return res.data
            }).catch(err => {
            alert(err)
            return err
        }).then(token => {
            const path = `${USERS_BASE_URL}/${userId}`
            axios.delete( path, {
                headers: {
                    Authorization: `Bearer ${token}`
                }})
                .then(res => {
                    //TODO: check this response
                    console.log(`User deleted successfully`)
                    console.log(`Response properties = ${res.data}`)

                    const filteredUsers = users.filter( user => user.userId !== userId )
                    changeUsers(filteredUsers)

                }).catch(err => {
                    alert(`Couldn't delete user - ${err}`)
            })
        })


    }

    return (
        <Card {...rest}>
            <PerfectScrollbar>
                <Box sx={{ minWidth: 1050 }}>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell>
                                    User Name
                                </TableCell>
                                <TableCell>
                                    Email
                                </TableCell>
                                <TableCell>
                                    Role
                                </TableCell>
                                <TableCell>
                                   Address
                                </TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {users.slice(0, limit).map((user) => (
                                //TODO: change this path, user != admin
                                <TableRow hover key={user.userId}>
                                    <TableCell>
                                        <Box sx={{alignItems: 'center', display: 'flex'}}>
                                            {/*
                                            <Avatar src={user.avatarUrl} sx={{ mr: 2 }}>
                                                {getInitials(user.name)}
                                            </Avatar>*/}
                                            <Typography color="textPrimary" variant="body1">
                                                {`${user.firstName} ${user.lastName}`}
                                            </Typography>
                                        </Box>
                                    </TableCell>
                                    <TableCell>
                                        {user.email}
                                    </TableCell>
                                    <TableCell>
                                        {`${user.position}`}
                                    </TableCell>
                                    <TableCell>
                                        {`${user.address}`}
                                    </TableCell>
                                    <IconButton
                                        aria-label="delete"
                                        onClick={() => removeUser(user.userId)}>
                                        <DeleteIcon />
                                    </IconButton>
                                    <IconButton
                                        aria-label="info"
                                        onClick={ () => router.push(`/admin/${user.userId}`) }>
                                        <InfoOutlinedIcon />
                                    </IconButton>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </Box>
            </PerfectScrollbar>
            <TablePagination
                component="div"
                count={users.length}
                onPageChange={handlePageChange}
                onRowsPerPageChange={handleLimitChange}
                page={page}
                rowsPerPage={limit}
                rowsPerPageOptions={[5, 10, 25]}
            />
        </Card>
    );
};

AllUsersTable.propTypes = {
    users: PropTypes.array.isRequired
};
