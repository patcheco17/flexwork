import {useEffect, useState} from 'react';
import PerfectScrollbar from 'react-perfect-scrollbar';
import PropTypes from 'prop-types';
import {
    Box,
    Card,
    Table,
    TableBody,
    TableCell,
    TableHead,
    TableRow,
    LinearProgress, Typography, Button, Collapse
} from '@mui/material';

import axios from "axios";
import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';
import {Alert} from "@mui/lab";
import {DELETE_ROOM_PATH, GET_ROOMS_PATH} from "../../../config-path";


export const AllRoomsTable = ({update} ) => {
    const [rooms, setRooms] = useState([])
    const [loading, setLoading] = useState(true)
    const [failFetch, setFailFetch] = useState(false)
    const [deleteFlag, setDeleteFlag] = useState(false)

    useEffect(() => {
        setLoading(true)
        setFailFetch(false)
        const token = 'eyJhbGciOiJSUzI1NiIsImtpZCI6IjYzMWZhZTliNTk0MGEyZDFmYmZmYjAwNDAzZDRjZjgwYTIxYmUwNGUiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJhY2NvdW50cy5nb29nbGUuY29tIiwiYXpwIjoiNjE4MTA0NzA4MDU0LTlyOXMxYzRhbGczNmVybGl1Y2hvOXQ1Mm4zMm42ZGdxLmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwiYXVkIjoiNjE4MTA0NzA4MDU0LTlyOXMxYzRhbGczNmVybGl1Y2hvOXQ1Mm4zMm42ZGdxLmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwic3ViIjoiMTA2NDAyMDkxMjU4ODU2MzI3NTM0IiwiZW1haWwiOiJmbGV4d29ya21hbmFnZW1lbnRAZ21haWwuY29tIiwiZW1haWxfdmVyaWZpZWQiOnRydWUsImF0X2hhc2giOiIyZ3RGdUhXeXRRdWxZRUJtWVdIVkp3IiwiaWF0IjoxNjU3OTc2MjExLCJleHAiOjE2NTc5Nzk4MTEsImp0aSI6ImE5NDg2MTgyNWNmMDAyMGY3YzA0MzQ0YWE0MjRiODhkMjMyOTU2ZGQifQ.TaxQQhjtRwCjh_5csDI2IvIoeYhk7pge9i4YivU5GfInyVlvfRYmBCxmMfStGfx-bJdvxZebT_OZJoScme-o4nmp6hY7r2Fm59P06NWCD93XFvLCmWJAoceM_B_D1Lvf8kfStLuuW6N1esAk877dfXMDmlwOANSZmj5m6ujfiFDDe5MiMSbsTwlDS3ibLO0z5LjQvd5Ng0lNDaYVj1DPBIQg1Am4GshF_5sL5ly-bgHpLC6Yi2_Yz0wkXANu7RqrWgUWOG3KqZzf3A3DaSzhJCZpd0CIfQFAUEvFRV1pMcfqwc5UyAPihizfh9W9CFSb3l6W7O3JB5XUKxpY_qWMuQ'
        axios.get(GET_ROOMS_PATH, {
            headers: {
                'Authorization': `Bearer ${token}`
            }
            } , {timeout: 10})
            .then(res => {
                const roomsReturned = res.data.properties.rooms;
                setRooms( roomsReturned );
            })
            .catch((error) => {
            setFailFetch(true)
        }).finally(() =>
            setLoading(false)
        );

    }, [update , deleteFlag])

    function deleteRoom(id){
        let path = getDeletePath(id)
        axios.delete(path, {
        } , {timeout: 10})
            .then(res => {
                setDeleteFlag(!deleteFlag)

            })
            .catch((error) => {
                console.log(error)
                alert('There was an error');
            })

    }

    function showAlert(message){
        return (
            <Box sx={{ width: '100%' }}>
                <Alert severity="error" onClose={() => {setFailFetch(false)}}>{message}</Alert>
            </Box>
        )
    }

    function loadRooms() {
        return (
            rooms.map((room) => (
                <TableRow hover key={room.id}>
                    <TableCell>
                        {room.roomName}
                    </TableCell>
                    <TableCell>
                        {`${room.roomCapacity}`}
                    </TableCell>
                    <TableCell>
                        {room.isFullRoom ? 'Full room' : 'Normal room'}
                    </TableCell>
                    <TableCell>
                        <Button  sx={{padding:0}}size="small" onClick={() => {deleteRoom(room.id)}}> <DeleteOutlineIcon color= 'red' fontSize="small"> </DeleteOutlineIcon> </Button>
                    </TableCell>
                </TableRow>
            ))
        );
    }



    return (
        <Card >
            <PerfectScrollbar>
                {failFetch ? showAlert('Something went wrong while fetching rooms.') :
                <Box sx={{ minWidth: 500 }}>
                    {loading ? <LinearProgress color="inherit" /> : <Table>
                        <TableHead sx={{backgroundColor:'neutral.600'}}>
                            <TableRow>
                                <TableCell>
                                    Room Name
                                </TableCell>
                                <TableCell>
                                    Capacity
                                </TableCell>
                                <TableCell>
                                    Room type
                                </TableCell>
                                <TableCell>
                                </TableCell>
                            </TableRow>
                        </TableHead>
                            <TableBody>
                                {loadRooms()}
                            </TableBody>

                    </Table> }
                </Box>}
            </PerfectScrollbar>
        </Card>
    );
};

function getDeletePath(id){
    return DELETE_ROOM_PATH + id
}


AllRoomsTable.propTypes = {
    rooms: PropTypes.array.isRequired
};

