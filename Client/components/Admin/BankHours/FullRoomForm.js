import * as React from 'react';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import {useEffect, useState} from "react";
import {showAlert, showAlertSuccess, ShowAlertWithClose} from "../../../utils/utils";
import CardContent from "@mui/material/CardContent";
import FormControl from "@mui/material/FormControl";
import Grid from "@mui/material/Grid";
import TextField from "@mui/material/TextField";
import InputLabel from "@mui/material/InputLabel";
import Select from "@mui/material/Select";
import MenuItem from "@mui/material/MenuItem";
import Card from "@mui/material/Card";
import axios from "axios";
import AllBookingsTable from "../../User/Booking/AllBookingsTable";
import CustomPagination from "../../User/CustomPagination";
import {BOOKINGS, GET_ALL_BOOKINGS_BY_ROOM, POST_FULL_ROOM_BOOKING} from "../../../config-path";


export default function FullRoomForm({roomId, userId}) {
    const [open, setOpen] = useState(false);
    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);
    const PAGE_SIZE = 3
    const [page, setPage] = useState(1)
    const [offset, setOffset] = useState(0)
    const [numOfPages, setNumOfPages] = useState(0)
    const [action, setAction] = useState(false)
    const [bookings, setBookings] = useState([])
    const [failPost, setFailPost] = useState(false)
    const [errorMessage, setErrorMessage] = useState([])

    const [newBooking, setNewBooking] = useState({
        ['userId']: userId,
        ['roomId']: roomId,
        ['day']: '',
        ['startTime']: '',
        ['finishTime']: '',
    })


    const handleChange = prop => event => {
        setNewBooking({...newBooking, [prop]: event.target.value})
    }


    const handleCreateBooking = e => {

        e.preventDefault();

        setNewBooking({...newBooking, ['roomId']: roomId})

        const headers = {
            "Content-Type": 'application/json',
        }

        function doFetch() {
            if (newBooking.day === '' || newBooking.startTime === '' || newBooking.finishTime === '') {
                alert("Please insert all the values")
                return
            }
            axios.post(POST_FULL_ROOM_BOOKING , JSON.stringify(newBooking), {headers: headers})
                .then(reponse => {
                    if (reponse.status === 200) {
                        alert("Your book were created with success")
                        setAction(!action)
                    } else {
                        setFailPost(true)
                        setErrorMessage(error.response.data.detail)
                        setNewBooking({ ['userId']: userId,
                            ['day']: '',
                            ['startTime']: '',
                            ['finishTime']: '',
                            ['roomId']: roomId,})
D
                    }
                })
                .catch(error => {
                    setFailPost(true)
                    setNewBooking({ ['userId']: userId,
                        ['day']: '',
                        ['startTime']: '',
                        ['finishTime']: '',
                        ['roomId']: roomId,})
                    if(error.code === 'ECONNABORTED' || error.message === 'Network Error'){
                        setErrorMessage("Something went wrong while creating booking")

                    }else{
                        setErrorMessage(error.response.data.detail)
                    }
                })


        }

        doFetch()
    }

    useEffect(() => {
        axios.get(GET_ALL_BOOKINGS_BY_ROOM + `/?roomId=${roomId}&offset=${offset}&limit=${PAGE_SIZE}`)
            .then(response => {
                if (response.status === 200) {
                    setBookings(response.data.properties.list)
                    setNumOfPages(response.data.properties.size)
                } else {
                    alert("Something went wrong")
                }
            })
            .catch(error => {
                setErrorMessage(error.message)
            })

    }, [action, page])

    return (
        <div>
            <Button variant='contained'onClick={handleOpen}>Book room</Button>
            <Modal
                open={open}
                onClose={handleClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"

            >

                <Grid container md={12}>
                    <Card sx={{margin: "0 auto", marginTop: "10%", width: "50%"}}
                    >
                        <Box>
                            <CardContent style={{backgroundColor: 'neutral.100'}}>
                                <Grid item xs={12} sm={12} md={12}>
                                    <Grid container xs={12} sm={12} md={12}>
                                        <Grid item xs={12} sm={12} md={6}>
                                            <FormControl>
                                                {failPost ?
                                                    <ShowAlertWithClose message={errorMessage} failPost={failPost}
                                                                        setFailPost={setFailPost}/> :
                                                    <>
                                                        <Typography
                                                            color="textSecondary"
                                                            gutterBottom
                                                            variant="overline"
                                                        >
                                                            Create a booking
                                                        </Typography>
                                                        <Grid container spacing={2} xs={12} sm={12} md={12}>
                                                            <Grid item xs={12} md={12}>
                                                                <TextField required fullWidth placeholder='Date'
                                                                           type='date'
                                                                           onChange={handleChange('day')}/>
                                                            </Grid>

                                                            <Grid item xs={12} sm={12} md={6}>
                                                                <TextField required fullWidth type='Time'
                                                                           placeholder='Start time'
                                                                           onChange={handleChange('startTime')}/>
                                                            </Grid>
                                                            <Grid item xs={12} sm={12} md={6}>
                                                                <TextField required fullWidth type='Time'
                                                                           placeholder='Finish time'
                                                                           onChange={handleChange('finishTime')}
                                                                />
                                                            </Grid>

                                                            <Grid item xs={12} sm={12} md={12}>
                                                                <Button size="large" variant='contained'
                                                                        onClick={handleCreateBooking}>
                                                                    Book
                                                                </Button>

                                                                <Button sx={{ml:5}}size="large" variant='contained'
                                                                        onClick={handleClose}>
                                                                    Close
                                                                </Button>

                                                            </Grid>

                                                        </Grid>


                                                    </>}
                                            </FormControl>


                                        </Grid>


                                        <Grid item xs={12} sm={12} md={6}>
                                            <AllBookingsTable bookings={bookings} title={"Room bookings"} action={action} setAction={setAction} deleteFunction={true}/>
                                            {numOfPages === 0 ? <></> :
                                                <CustomPagination numOfPages={numOfPages} page={page} setPage={setPage}
                                                                  setOffset={setOffset} offset={offset} limit={PAGE_SIZE}/>
                                            }
                                        </Grid>
                                    </Grid>

                                </Grid>

                            </CardContent>
                        </Box>
                    </Card>
                </Grid>

            </Modal>
        </div>
    );
}



