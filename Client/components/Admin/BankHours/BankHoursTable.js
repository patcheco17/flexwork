// ** MUI Imports
import Box from '@mui/material/Box'
import Card from '@mui/material/Card'
import Chip from '@mui/material/Chip'
import Table from '@mui/material/Table'
import TableRow from '@mui/material/TableRow'
import TableHead from '@mui/material/TableHead'
import TableBody from '@mui/material/TableBody'
import TableCell from '@mui/material/TableCell'
import Typography from '@mui/material/Typography'
import TableContainer from '@mui/material/TableContainer'
import InfoIcon from '@mui/icons-material/Info';
import {IconButton, LinearProgress} from "@mui/material";
import Avatar from "@mui/material/Avatar";
import {getInitials} from "../../../utils/utils";
import React, {useEffect, useState} from "react";
import dateFormat from "dateformat";
import {Alert} from "@mui/lab";
import axios from "axios";
import {BASE_URL, BALANCE_MONTHLY} from "../../../config-path";
import {useRouter} from "next/router";


const BankHoursTable = () => {
    const currentDay = new Date();
    const [date, setDate] = useState(dateFormat(currentDay, "yyyy-mm-dd"))
    const [data, setData] = useState([])
    const [loading, setLoading] = useState(true)
    const [failFetch, setFailFetch] = useState(false)
    const [errorMessage, setErrorMessage] = useState("")
    const router = useRouter()

    useEffect(() => {

        async function doFetch() {
            try {
                const response = await axios.get(BASE_URL + BALANCE_MONTHLY + date, {}, {timeout: 5000})
                setData(response.data.properties.users)
            } catch (e) {
                setFailFetch(true)
                if (e.response.data != null) {
                    setErrorMessage(e.response.data.detail)
                } else {
                    setErrorMessage("Server down")
                }
            } finally {
                setLoading(false)
            }
        }

        doFetch()
    }, [])


    function timeConvert(workedTime) {
        const hours = (workedTime / 60);
        const finalHours = Math.floor(hours);
        const minutes = (hours - finalHours) * 60;
        const finalMinutes = Math.round(minutes);
        return finalHours + "h" + finalMinutes + "m";
    }

    function showAlert(message) {
        return (
            <Box sx={{width: '100%'}}>
                <Alert severity="error" onClose={() => {
                    setFailFetch(false)
                }}>{message}</Alert>
            </Box>
        )
    }

    return (
        <div>
            {failFetch ? showAlert(errorMessage) :
                <Card>
                    {loading ? <LinearProgress color="inherit"/> :
                        <TableContainer>
                            <Table sx={{minWidth: 800}} aria-label='table in dashboard'>
                                <TableHead>
                                    <TableRow>

                                        <TableCell></TableCell>
                                        <TableCell>Name</TableCell>
                                        <TableCell>Project</TableCell>
                                        <TableCell>Function</TableCell>
                                        <TableCell>Hours in Bank</TableCell>
                                        <TableCell>Details</TableCell>

                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {data.map(user => (
                                        <TableRow hover key={user.userId}
                                                  sx={{'&:last-of-type td, &:last-of-type th': {border: 0}}}>
                                            <TableCell>
                                                <Avatar src={""} sx={{mr: 2}}>
                                                    {getInitials(user.firstName + " " + user.lastName)}
                                                </Avatar>
                                            </TableCell>
                                            <TableCell sx={{py: theme => `${theme.spacing(0.5)} !important`}}>
                                                <Box sx={{display: 'flex', flexDirection: 'column'}}>
                                                    <Typography sx={{
                                                        fontWeight: 500,
                                                        fontSize: '0.875rem !important'
                                                    }}>{user.firstName} {user.lastName}</Typography>
                                                    <Typography variant='caption'>{user.designation}</Typography>
                                                </Box>
                                            </TableCell>
                                            <TableCell>{user.projectName}</TableCell>
                                            <TableCell>{user.position}</TableCell>
                                            <TableCell><Chip
                                                label={timeConvert(user.timeWorked)}
                                                sx={{
                                                    height: 24,
                                                    fontSize: '0.75rem',
                                                    textTransform: 'capitalize',
                                                    '& .MuiChip-label': {fontWeight: 500}
                                                }}
                                            /></TableCell>
                                            <TableCell>
                                                <IconButton
                                                    onClick={ () => router.push(`/admin/${user.userId}`) }>
                                                    <InfoIcon/>
                                                </IconButton></TableCell>
                                        </TableRow>
                                    ))}
                                </TableBody>
                            </Table>
                        </TableContainer>}

                </Card>}
        </div>
    )
}

export default BankHoursTable
