import CardHeader from "@mui/material/CardHeader";
import CardContent from "@mui/material/CardContent";
import Box from "@mui/material/Box";
import Card from "@mui/material/Card";
import Grid from "@mui/material/Grid";
import React, {useEffect, useState} from "react";
import {Doughnut} from "react-chartjs-2";
import axios from "axios";
import {ACCESS_TOKEN, STATISTICS_ROOM_OCCUPANCY_PAST_MONTH} from "../../../config-path";

const PastMonthRoomsOccupancyCard = () => {

    const [pastMonthOccupancy, setPastMonthOccupancy] = useState( [])
    const [pastMonthTotalBookings, setPastMonthTotalBookings] = useState( null)

    useEffect(() => {
        axios.get(ACCESS_TOKEN)
            .then(res => {
                return res.data
            }).catch(err => {
                alert(err)
                return err
        }).then(token => {
            axios.get(STATISTICS_ROOM_OCCUPANCY_PAST_MONTH, {
                headers: {
                    Authorization: `Bearer ${token}`
                }})
                .then(res => {
                    //console.log(res)
                    const pastMonthOccupancyResponse = res.data.properties.rooms;
                    const pastMonthTotalBookingsResponse = res.data.properties.totalBookings;
                    setPastMonthOccupancy( pastMonthOccupancyResponse );
                    setPastMonthTotalBookings( pastMonthTotalBookingsResponse );
                })
        })
    }, [])

    const data = {
        labels: pastMonthOccupancy.map(room => room.roomName),
        datasets: [
            {
                label: 'Bookings Counter per Room',
                fill: false,
                lineTension: 0.1,
                backgroundColor:  [
                    'rgb(78,78,106)',
                    'rgb(120,104,230)',
                    'rgb(189,197,247)',
                    'rgb(128,128,162)',
                    'rgb(27,24,27)',
                    'rgb(48,45,255)',
                    'rgb(3,199,255)',
                ],
                data: pastMonthOccupancy.map(room => room.bookingsCounter)
            }
        ]
    }

    return (
        <Card>
            <CardHeader title='Number of Bookings Per Room Last Month'/>
            <CardContent sx={{ pt: theme => `${theme.spacing(0)} !important` }}>
                <Grid container >
                    <p>Total bookings: {pastMonthTotalBookings}</p>
                    <Grid item xs={12} sm={12} >
                        <Box sx={{ display: 'flex', alignItems: 'center' }}>
                            <Doughnut
                                data={data}
                                responsive
                                maintainAspectRatio/>
                        </Box>
                    </Grid>
                </Grid>
            </CardContent>
        </Card>
    )
}

export default PastMonthRoomsOccupancyCard