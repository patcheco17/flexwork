import CardHeader from "@mui/material/CardHeader";
import CardContent from "@mui/material/CardContent";
import Box from "@mui/material/Box";
import Card from "@mui/material/Card";
import Grid from "@mui/material/Grid";
import React, {useEffect, useState} from "react";
import {Line} from "react-chartjs-2";
import axios from "axios";
import {ACCESS_TOKEN, STATISTICS_INFLUX_PAST_MONTH} from "../../../config-path";

const PastMonthInflux = () => {

    const [pastMonthInflux, setPastMonthInflux] = useState( [])
    const [pastMonthInfluxAverage, setPastMonthInfluxAverage] = useState(null)

    useEffect(() => {
        axios.get(ACCESS_TOKEN)
            .then(res => {
                return res.data
            }).catch(err => {
                alert("Something went wrong")
                console.log(err)
                return err
        }).then(token => {
            axios.get(STATISTICS_INFLUX_PAST_MONTH, {
                headers: {
                    Authorization: `Bearer ${token}`
                }})
                .then(res => {
                    //console.log(res)
                    const pastWeekInfluxResponse = res.data.properties.days;
                    const pastWeekInfluxAverageResponse = res.data.properties.timeSpanAverage;
                    setPastMonthInflux( pastWeekInfluxResponse );
                    setPastMonthInfluxAverage( pastWeekInfluxAverageResponse );
                })
        })
    }, [])

    const data = {
        labels: pastMonthInflux.map(day => day.date.split("-")[2]),
        datasets: [
            {
                label: 'Influx',
                fill: false,
                lineTension: 0.1,
                backgroundColor: '#fff',
                borderColor: 'rgb(78,78,106)',
                borderCapStyle: 'butt',
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: 'miter',
                pointBorderColor: 'rgb(78,78,106)',
                pointBackgroundColor: '#fff',
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: 'rgb(78,78,106)',
                pointHoverBorderColor: 'rgba(220,220,220,1)',
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                data: pastMonthInflux.map(day => day.occupancy)
            }
        ]
    }

    const options = {
        scales: {
            x: {
                display: true,
                title: {
                    display: true,
                    text: 'Day',
                }
            },
            y: {
                display: true,
                title: {
                    display: true,
                    text: '% of Workers',
                },
                beginAtZero: true,
                steps: 10,
                stepValue: 10,
                max: 100
            }
        }
    }


    return (
        <Card>
            <CardHeader title='Influx of Workers Last Month'/>
            <CardContent sx={{ pt: theme => `${theme.spacing(0)} !important` }}>
                <Grid container >
                    <p>Average of last month = {pastMonthInfluxAverage} %</p>
                    <Grid item xs={12} sm={12} >
                        <Box sx={{ display: 'flex', alignItems: 'center' }}>
                            <Line
                                data={data}
                                options={options}
                                responsive
                                maintainAspectRatio/>
                        </Box>
                    </Grid>
                </Grid>
            </CardContent>
        </Card>
    )
}

export default PastMonthInflux