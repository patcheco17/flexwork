// ** MUI Imports
import Card from '@mui/material/Card'
import Button from '@mui/material/Button'
import Typography from '@mui/material/Typography'
import CardContent from '@mui/material/CardContent'
import { styled, useTheme } from '@mui/material/styles'

const DayCard = () => {
    const theme = useTheme()
    const imageSrc = theme.palette.mode === 'light' ? 'triangle-light.png' : 'triangle-dark.png'

    return (
        <Card sx={{ position: 'relative' }}>
            <CardContent>
                <Typography variant='h6'>20/04/2022</Typography>
                <Typography variant='h5' sx={{ my: 4, color: 'primary.main' }}>
                    Monday
                </Typography>
                <Button size='small' variant='contained'>
                    View Events
                </Button>

            </CardContent>
        </Card>
    )
}

export default DayCard
