// ** MUI Imports
import Box from '@mui/material/Box'
import Grid from '@mui/material/Grid'
import Card from '@mui/material/Card'
import Avatar from '@mui/material/Avatar'
import CardHeader from '@mui/material/CardHeader'
import Typography from '@mui/material/Typography'
import CardContent from '@mui/material/CardContent'

import MenuIcon from '@mui/icons-material/Menu';
import HomeWorkIcon from '@mui/icons-material/HomeWork';
import HailIcon from '@mui/icons-material/Hail';
import {useEffect, useState} from "react";
import axios from "axios";
import {ACCESS_TOKEN, STATISTICS_COLLABORATORS_COUNT} from "../../../config-path";

const renderStats = () => {
    const [collaboratorsCounter, setCollaboratorsCounter] = useState(null)

    useEffect(() => {
        axios.get(ACCESS_TOKEN)
            .then(res => {
                return res.data
            }).catch(err => {
                alert("Something went wrong")
                return err
        }).then(token => {
            axios.get(STATISTICS_COLLABORATORS_COUNT, {
                headers: {
                    Authorization: `Bearer ${token}`
                }})
                .then(res => {
                    const collaboratorsCounter = res.data;
                    setCollaboratorsCounter( collaboratorsCounter );
                })
        })
    }, [])

    return(
        <Grid item xs={12} sm={12} >
            <Box sx={{ display: 'flex', alignItems: 'center' }}>
                <Avatar
                    variant='rounded'
                    sx={{
                        mr: 3,
                        width: 44,
                        height: 44,
                        boxShadow: 3,
                        color: 'common.white',
                        backgroundColor: 'primary.main'
                    }}>
                    <HailIcon sx={{ fontSize: '1.75rem' }} />
                </Avatar>
                <Box sx={{ display: 'flex', flexDirection: 'column' }}>
                    <Typography variant='h5'>{collaboratorsCounter}</Typography>
                </Box>
            </Box>
        </Grid>
    )
}

const TopCard = () => {
    return (
        <Card>
            <CardHeader title='Number of collaborators in the company:'/>
            <CardContent sx={{ pt: theme => `${theme.spacing(0)} !important` }}>
                <Grid container >
                    {renderStats()}
                </Grid>
            </CardContent>
        </Card>
    )
}

export default TopCard
