// ** MUI Imports
import Card from '@mui/material/Card'
import Button from '@mui/material/Button'
import CardMedia from '@mui/material/CardMedia'
import Typography from '@mui/material/Typography'
import CardContent from '@mui/material/CardContent'


const RoomsCard = ({room}) => {
    return (
        <Card sx={{ height: '14rem' , width: '20rem', backgroundColor: 'neutral.100'}}>
            <CardMedia sx={{ height: '10rem' }} image={room.img} />
            <CardContent sx={{ padding: theme => `${theme.spacing(3, 5.25, 4)} !important` }}>
                <Typography variant='h6' sx={{ marginBottom: 2 }}>
                    {room.name}
                </Typography>
                <Typography variant='body2' sx={{ marginBottom: 2 }}>
                    {room.desc}
                </Typography>
                <Typography variant='h6' sx={{ marginBottom: 2 }}>
                    Occupation Limit
                </Typography>
                <Typography sx={{ marginBottom: 2 }}>20</Typography>

            </CardContent>
            <Button variant='contained' sx={{ py: 2.5, width: '100%', borderTopLeftRadius: 0, borderTopRightRadius: 0 }}>
                Manage Room
            </Button>
        </Card>
    )
}

export default RoomsCard
