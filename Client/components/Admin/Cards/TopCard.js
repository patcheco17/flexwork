    // ** MUI Imports
    import Box from '@mui/material/Box'
    import Grid from '@mui/material/Grid'
    import Card from '@mui/material/Card'
    import Avatar from '@mui/material/Avatar'
    import CardHeader from '@mui/material/CardHeader'
    import Typography from '@mui/material/Typography'
    import CardContent from '@mui/material/CardContent'

    import MenuIcon from '@mui/icons-material/Menu';
    import HomeWorkIcon from '@mui/icons-material/HomeWork';
    import HailIcon from '@mui/icons-material/Hail';
    const salesData = [
        {
            stats: '14',
            title: 'Nr Of People At The Office Today',
            color: 'primary',
            icon: <HailIcon sx={{ fontSize: '1.75rem' }} />
        },
        {
            stats: '120',
            title: 'Nr Of People Working Remote Today',
            color: 'success',
            icon: <HomeWorkIcon sx={{ fontSize: '1.75rem' }} />
        },
        {
            stats: '1',
            color: 'error',
            title: 'Nr Of People OFF',
            icon: <MenuIcon sx={{ fontSize: '1.75rem' }} />
        }
    ]

    const renderStats = () => {
        return salesData.map((item, index) => (
            <Grid item xs={12} sm={4} key={index}>
                <Box key={index} sx={{ display: 'flex', alignItems: 'center' }}>
                    <Avatar
                        variant='rounded'
                        sx={{
                            mr: 3,
                            width: 44,
                            height: 44,
                            boxShadow: 3,
                            color: 'common.white',
                            backgroundColor: `${item.color}.main`
                        }}>
                        {item.icon}
                    </Avatar>
                    <Box sx={{ display: 'flex', flexDirection: 'column' }}>
                        <Typography variant='caption'>{item.title}</Typography>
                        <Typography variant='h6'>{item.stats}</Typography>
                    </Box>
                </Box>
            </Grid>
        ))
    }

    const TopCard = () => {
        return (
            <Card>
                <CardHeader
                    title='Statistics Card'
                    subheader={
                        <Typography variant='body2'>
                            <Box component='span' sx={{ fontWeight: 500, color: 'text.primary' }}>
                                Today's Information
                            </Box>
                        </Typography>
                    }
                />
                <CardContent sx={{ pt: theme => `${theme.spacing(3)} !important` }}>
                    <Grid container spacing={[2, 0]}>
                        {renderStats()}
                    </Grid>
                </CardContent>
            </Card>
        )
    }

    export default TopCard
