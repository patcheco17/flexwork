import PropTypes from 'prop-types';
import styled from '@emotion/styled';
import {AppBar, Avatar, Badge, Box, Button, IconButton, Toolbar, Tooltip} from '@mui/material';
import MenuIcon from '@mui/icons-material/Menu';
import NotificationsIcon from '@mui/icons-material/Notifications';
import CircleIcon from '@mui/icons-material/Circle';
import React, {useEffect, useState} from "react";
import NextLink from "next/link";
import { useUser } from '@auth0/nextjs-auth0';
import axios from "axios";

const NavbarRoot = styled(AppBar)(({ theme }) => ({
    backgroundColor: '#4E4E6A',
    boxShadow: theme.shadows[3]
}));



export const AdminNavbar = (props) => {
    const { onSidebarOpen, ...other } = props;

    return (
        <>
            <NavbarRoot>
                <Toolbar disableGutterssx={{minHeight: 64, left: 0,px: 2 }} >
                    <Box sx={{ml:4,p: 1, display:"flex" , color:'#F4F5FA',justifyContent:"center", fontSize: '2.4rem' }}>
                        <NextLink href="/" passHref >
                            <p>FlexWork</p>
                        </NextLink>
                    </Box>
                    <IconButton onClick={onSidebarOpen}
                        sx={{display: {xs: 'inline-flex', lg: 'none'}}}>
                        <MenuIcon fontSize="small" />
                    </IconButton>

                    <Box sx={{ flexGrow: 1 }} />

                    <Button variant="outlined" color="themedButton" >
                        <a href="/api/auth/logout">Logout</a>
                    </Button>

                    <Tooltip title="Notifications">
                        <IconButton sx={{ ml: 1 }}>
                            <Badge
                                badgeContent={4}
                                color="primary"
                                variant="dot"
                            >
                                <NotificationsIcon fontSize="small" />
                            </Badge>
                        </IconButton>
                    </Tooltip>


                    <Avatar sx={{ height: 40,width: 40, ml: 1}}
                        src="https://m.economictimes.com/thumb/msid-86001366,width-1200,height-900,resizemode-4,imgsize-31272/manager-istock.jpg"
                    >
                        <CircleIcon fontSize="small" />
                    </Avatar>

                </Toolbar>
            </NavbarRoot>
        </>
    );
};

AdminNavbar.propTypes = {
    onSidebarOpen: PropTypes.func
};
