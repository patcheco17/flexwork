import PropTypes from 'prop-types';
import {AppBar, Box, Button, Toolbar} from '@mui/material';
import Typography from "@mui/material/Typography";
import NextLink from "next/link";
import React from "react";

export const  HomePageNavbar = (props) => {
    const { onSidebarOpen, ...other } = props;

    return (
        <>
            <Box >
                <AppBar  elevation={0}>
                    <Toolbar >
                        <h1>
                            <span>FlexWork</span>
                        </h1>
                        <Box sx={{  flexGrow: '1'}} />
                        <Button variant="contained"  sx={{ backgroundColor: '#F4F5FA', color:'#4E4E6A',
                            '&:hover': {color: '#F4F5FA',backgroundColor: '#707086',}}}>
                            <a href="/api/auth/login">Login</a>
                        </Button>
                    </Toolbar>
                </AppBar>
            </Box>
        </>
    );
};

HomePageNavbar.propTypes = {
    onSidebarOpen: PropTypes.func
};
