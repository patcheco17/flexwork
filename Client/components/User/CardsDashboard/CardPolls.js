// ** MUI Imports
import Box from '@mui/material/Box'
import Card from '@mui/material/Card'
import Avatar from '@mui/material/Avatar'
import Typography from '@mui/material/Typography'
import IconButton from '@mui/material/IconButton'
import CardHeader from '@mui/material/CardHeader'
import CardContent from '@mui/material/CardContent'
import LinearProgress from '@mui/material/LinearProgress'
import {Grid} from "@mui/material";
import ThumbUpIcon from '@mui/icons-material/ThumbUp';
import ThumbDownIcon from '@mui/icons-material/ThumbDown';
import AvatarGroup from "@mui/material/AvatarGroup";

const data = [
    {
        progress: 30,
        title: 'Footbal Wednesday',
        color: 'info',
        subtitle: 'Field: Sporting Stadium',
        theme:'Sports',
        icon: 'https://upload.wikimedia.org/wikipedia/commons/thumb/6/6e/Football_%28soccer_ball%29.svg/1200px-Football_%28soccer_ball%29.svg.png'
    },
    {
        progress: 90,
        color: 'success',
        title: 'Lunch at McDonalds',
        subtitle: '1PM: McDonalds',
        theme: 'Lunch',
        icon: 'https://media.istockphoto.com/photos/closeup-mcdonalds-outdoor-sign-against-blue-sky-picture-id458546943?k=20&m=458546943&s=612x612&w=0&h=G7fU8lNJh50I-Ou_ocB8XE5s_jpphKO0wNPy_5OxOkc='
    }
]

const CardPolls = () => {
    return (
        <Card>
            <CardHeader
                title='Polls'
                titleTypographyProps={{ sx: { lineHeight: '1.6 !important', letterSpacing: '0.15px !important' } }}
                action={
                    <IconButton size='small' aria-label='settings' className='card-more-options' sx={{ color: 'text.secondary' }}>
                    </IconButton>
                }
            />
            <CardContent sx={{ pt: theme => `${theme.spacing(2.25)} !important` }}>
                {data.map((item, index) => {
                    return (
                        <Box key={item.title} sx={{display: 'flex', alignItems: 'center', ...(index !== data.length - 1 ? { mb: 8.5 } : {})}}>

                            <Grid container spacing={1}>
                                <Grid item lg={3} sm={6} xl={3} xs={12}>
                                    <Avatar src={item.icon} variant='rounded' sx={{mr: 3, width: 40, height: 40}}></Avatar>
                                </Grid>
                                <Grid item lg={3} sm={6} xl={3} xs={12}>
                                    <Typography variant='body2' sx={{ mb: 0.5, fontWeight: 600, color: 'text.primary' }}>
                                        {item.title}
                                    </Typography>

                                </Grid>
                                <Grid item lg={6} sm={6} xl={6} xs={12}>
                                    <AvatarGroup max={4}>
                                        <Avatar src='http://s2.glbimg.com/jsaPuF7nO23vRxQkuJ_V3WgouKA=/e.glbimg.com/og/ed/f/original/2014/06/10/461777879.jpg' alt='Alice Cobb' />
                                        <Avatar src='https://cajamar.sp.gov.br/noticias/wp-content/uploads/sites/2/2021/08/site-vacinacao-26-anos.png' alt='Jeffery Warner' />
                                        <Avatar src='https://portalmaratimba.com.br/wp-content/uploads/2020/03/pessoa-facil-de-gostar.jpg' alt='Howard Lloyd' />
                                        <Avatar src='/images/avatars/2.png' alt='Bettie Dunn' />
                                        <Avatar src='/images/avatars/4.png' alt='Olivia Sparks' />
                                        <Avatar src='/images/avatars/5.png' alt='Jimmy Hanson' />
                                        <Avatar src='/images/avatars/6.png' alt='Hallie Richards' />
                                    </AvatarGroup>
                                </Grid>
                                <Grid item lg={12} sm={12} xl={12} xs={12}>
                                    <Typography variant='caption'>{item.subtitle}</Typography>
                                </Grid>

                                <Grid item lg={12} sm={12} xl={12} xs={12}>
                                    <LinearProgress color={item.color} value={item.progress} variant='determinate' />
                                </Grid>
                                <Grid item align="center" lg={12} sm={12} xl={12} xs={12}>
                                    <IconButton color="success">
                                        <ThumbUpIcon />
                                    </IconButton>
                                    <IconButton color="error" >
                                        <ThumbDownIcon/>
                                    </IconButton>

                                </Grid>

                            </Grid>
                        </Box>
                    )
                })}
            </CardContent>
        </Card>
    )
}

export default CardPolls
