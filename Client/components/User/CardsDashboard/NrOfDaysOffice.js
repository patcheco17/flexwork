import { Avatar, Box, Card, CardContent, Grid, Typography } from '@mui/material';
import ArrowUpwardIcon from '@mui/icons-material/ArrowUpward';
import ApartmentIcon from '@mui/icons-material/Apartment';

export const NrOfDaysOffice = (props) => (
    <Card {...props}>
        <CardContent>
            <Grid container spacing={3} sx={{ justifyContent: 'space-between' }}>
                <Grid item>
                    <Typography color="textSecondary" gutterBottom variant="overline">
                        Number Of Days At the Office This Month
                    </Typography>
                    <Typography color="textPrimary" variant="h4" sx={{display:"flex" ,justifyContent:"center"}} >
                        48
                    </Typography>
                </Grid>
                <Grid item>
                    <Avatar sx={{backgroundColor: 'info.light',height: 56, width: 56}}>
                        <ApartmentIcon />
                    </Avatar>
                </Grid>
            </Grid>
        </CardContent>
    </Card>
);
