import {Avatar, Card, CardContent, Grid, LinearProgress, Typography} from '@mui/material';
import ScheduleIcon from '@mui/icons-material/Schedule';
import React, {useEffect, useState} from "react";
import axios from "axios";
import {Schedule} from "@mui/icons-material";
import {BALANCE_MONTHLY, BALANCE_MONTHLY_USER, BASE_URL, USER_ID} from "../../../config-path";
import dateFormat from "dateformat";
import {useUser} from "@auth0/nextjs-auth0";
import {extractUserId} from "../../../utils/utils";
import Box from "@mui/material/Box";
import {Alert} from "@mui/lab";

const UserBalance = () => {
    const currentDay = new Date();
    const [date, setDate] = useState(dateFormat(currentDay, "yyyy-mm-dd"))
    const {user, error, isLoading} = useUser();

    const [loading, setLoading] = useState(true)
    const [failFetch, setFailFetch] = useState(false)
    const [errorMessage, setErrorMessage] = useState("")

    if (isLoading || !user) {
        return <LinearProgress/>
    }

    const [hoursBalance, setHoursBalance] = useState(null)

    useEffect(() => {
        axios.get(BASE_URL + BALANCE_MONTHLY_USER + date + USER_ID + extractUserId(user.sub))
            .then(response => {
                if (response.status === 200) {
                    const hoursBalance = response.data.properties.hoursWorked
                    setHoursBalance(hoursBalance)
                    setLoading(false)
                } else {
                    if (response.data != null) {
                        setErrorMessage(response.data.detail)
                    } else {
                        setErrorMessage("Server down")
                    }
                    setLoading(false)
                }
            })
            .catch(error => {
                if (error.response.data != null) {
                    setErrorMessage(error.response.data.detail)
                } else {
                    setErrorMessage("Server down")
                }
                setLoading(false)
            })
    }, [])

    function showAlert(message) {
        return (
            <Box sx={{width: '100%'}}>
                <Alert severity="error" onClose={() => {
                    setFailFetch(false)
                }}>{message}</Alert>
            </Box>
        )
    }

    return (
        <div>
            {failFetch ? showAlert(errorMessage) :
                <Card>
                    {loading ? <LinearProgress color="inherit"/> :
                        <CardContent>
                            <Grid
                                container
                                spacing={3}
                                sx={{justifyContent: 'space-between'}}
                            >
                                <Grid item>
                                    <Typography
                                        color="textSecondary"
                                        gutterBottom
                                        variant="overline"
                                    >
                                        Number of Hours Worked This Month
                                    </Typography>
                                    <Typography color="textPrimary" variant="h4"
                                                sx={{display: "flex", justifyContent: "center"}}>
                                        {hoursBalance}
                                    </Typography>
                                </Grid>
                                <Grid item>
                                    <Avatar sx={{backgroundColor: 'info.main', height: 50, width: 50}}>
                                        <ScheduleIcon/>
                                    </Avatar>
                                </Grid>
                            </Grid>

                        </CardContent>
                    }
                </Card>
            }
        </div>
    )
}

export default UserBalance