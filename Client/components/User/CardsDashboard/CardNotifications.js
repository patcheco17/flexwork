// ** MUI Imports
import Box from '@mui/material/Box'
import Card from '@mui/material/Card'
import Avatar from '@mui/material/Avatar'
import Typography from '@mui/material/Typography'
import IconButton from '@mui/material/IconButton'
import CardHeader from '@mui/material/CardHeader'
import CardContent from '@mui/material/CardContent'
import {getInitials} from "../../../utils/utils";
import Grid from "@mui/material/Grid";
import React from "react";



const CardNotifications = ({messageCache}) => {
    messageCache.reverse()

    return (
        <Grid container alignItems="center"  justifyContent="center"  spacing={2}>
        {
            messageCache.map( msg =>
                (
                    <>
                        <Grid item xs={12} md={12} alignItems="center"  justifyContent="center" >
                            <Card sx={{backgroundColor:'neutral.100', maxWidth :'100%'}}>
                                <CardContent>
                                    <Typography  variant='h6' sx={{ color:'neutral.500' }} style={{ wordWrap: "break-word" }}  color="text.primary">
                                        {msg ? msg.title : ''}
                                    </Typography>
                                    <Typography  variant='h6' style={{ wordWrap: "break-word" }}  color="text.primary">
                                        {msg ? msg.text : ''}
                                    </Typography>
                                </CardContent>
                            </Card>
                        </Grid>
                    </>
                )
            )
        }
        </Grid>
    )
}

export default CardNotifications
