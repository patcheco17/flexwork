import { Box, Button, Card, CardContent, CardHeader, Divider, useTheme } from '@mui/material';
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';
import ArrowRightIcon from '@mui/icons-material/ArrowRight';
import SimpleAccordion from "./simpleAccordion";

export const EventsCard = (props) => {
    const theme = useTheme();

    const event = {
        title:'Reunião CEO'
    }
    const event1 = {
        title:'Reunião Cliente'
    }
    const event2 = {
        title:'Standup'
    }
    const event3 = {
        title:'Jogo Futebol'
    }
    const event4 = {
        title:'Formação'
    }
    return (
        <Card {...props}>
            <CardHeader
                title="This Week Events"
            />
            <Divider />
            <CardContent sx={{backgroundColor: 'background.default'}}>
                <Box sx={{  height: 400, position: 'relative'}}>
                    <SimpleAccordion event={event}/>
                    <SimpleAccordion event={event1}/>
                    <SimpleAccordion event={event2}/>
                    <SimpleAccordion event={event3}/>
                    <SimpleAccordion event={event4}/>

                </Box>
            </CardContent>
        </Card>
    );
};
