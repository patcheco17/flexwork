// ** MUI Imports
import Box from '@mui/material/Box'
import Card from '@mui/material/Card'
import Avatar from '@mui/material/Avatar'
import Typography from '@mui/material/Typography'
import IconButton from '@mui/material/IconButton'
import CardHeader from '@mui/material/CardHeader'
import CardContent from '@mui/material/CardContent'
import {getInitials} from "../../../utils/utils"
import React, {useEffect, useState} from "react";
import axios from "axios";
import {ACCESS_TOKEN, STATISTICS_COLLABORATORS_COUNT, STATISTICS_COLLABORATORS_IN_OFFICE} from "../../../config-path";
import {LinearProgress} from "@mui/material";
import {useUser} from "@auth0/nextjs-auth0";
import {Alert} from "@mui/lab";

const PeopleAtTheOffice = () => {
    const [collaboratorsInOffice, setCollaboratorsInOffice] = useState([])
    const {user, error, isLoading} = useUser();
    const [loading, setLoading] = useState(true)
    const [failFetch, setFailFetch] = useState(false)
    const [errorMessage, setErrorMessage] = useState("")

    if (isLoading || !user) {
        return <LinearProgress/>
    }

    useEffect(() => {
        axios.get(ACCESS_TOKEN)
            .then(res => {
                return res.data
            }).catch(err => {
                setErrorMessage(err)
                setFailFetch(true)
                setLoading(false)
                return err
        }).then(token => {
            //console.log(token)
            axios.get(STATISTICS_COLLABORATORS_IN_OFFICE, {
                headers: {
                    Authorization: `Bearer ${token}`
                }})
                .then(res => {
                    const collaboratorsInOffice = res.data.properties.users;
                    setCollaboratorsInOffice( collaboratorsInOffice );
                    setLoading(false)
                })
                .catch(err => {
                    setFailFetch(true)
                    setErrorMessage(err)
                    setLoading(false)
                })
        })
    }, [])

    function showAlert(message) {
        return (
            <Box sx={{width: '100%'}}>
                <Alert severity="error" onClose={() => {
                    setFailFetch(false)
                }}>{message}</Alert>
            </Box>
        )
    }

    return (
        <div>
            {failFetch ? showAlert(errorMessage) :
                <Card>
                    {loading ? <LinearProgress color="inherit"/> :
                        <div>
                        <CardHeader
                            title='People At the Office Today'
                            titleTypographyProps={{
                                sx: {
                                    lineHeight: '1.2 !important',
                                    letterSpacing: '0.31px !important'
                                }
                            }}
                        />
                        <CardContent sx={{pt: theme => `${theme.spacing(2)} !important`}}>
                        {collaboratorsInOffice.length > 0 ?
                            collaboratorsInOffice.map((item, index) => {
                                return (
                                    <Box key={item.userId} sx={{
                                        display: 'flex',
                                        alignItems: 'center', ...(index !== collaboratorsInOffice.length - 1 ? {mb: 5.875} : {})
                                    }}>
                                        <Avatar sx={{
                                            width: 38,
                                            height: 38,
                                            marginRight: 3,
                                            fontSize: '1rem',
                                            color: 'common.white',
                                            backgroundColor: `success.main`
                                        }}>
                                            {getInitials(`${item.firstName} ${item.lastName}`)}
                                        </Avatar>

                                        <Box sx={{
                                            width: '100%',
                                            display: 'flex',
                                            flexWrap: 'wrap',
                                            alignItems: 'center',
                                            justifyContent: 'space-between'
                                        }}>
                                            <Box sx={{marginRight: 2, display: 'flex', flexDirection: 'column'}}>
                                                <Box sx={{display: 'flex'}}>
                                                    <Typography sx={{
                                                        mr: 0.5,
                                                        fontWeight: 600,
                                                        letterSpacing: '0.25px'
                                                    }}>{`${item.firstName} ${item.lastName}`}</Typography>
                                                    <Box sx={{display: 'flex', alignItems: 'center'}}>
                                                    </Box>
                                                </Box>
                                                <Typography variant='caption' sx={{lineHeight: 1.5}}>
                                                    {item.projectName}
                                                </Typography>
                                            </Box>

                                            <Box sx={{display: 'flex', textAlign: 'end', flexDirection: 'column'}}>
                                                <Typography sx={{
                                                    fontWeight: 600,
                                                    fontSize: '0.875rem',
                                                    lineHeight: 1.72,
                                                    letterSpacing: '0.22px'
                                                }}>
                                                    {item.position}
                                                </Typography>
                                            </Box>
                                        </Box>
                                    </Box>
                                )
                            }) :
                            <Box sx={{
                                width: '100%',
                                display: 'flex',
                                flexWrap: 'wrap',
                                alignItems: 'center',
                                justifyContent: 'space-between'
                            }}>
                                <Typography sx={{mr: 0.5, fontWeight: 600, letterSpacing: '0.25px'}}>Looks like the
                                    office will be empty today</Typography>
                            </Box>
                        }

                        </CardContent>
                        </div>
                    }
                </Card>
            }
        </div>
    )
}

export default PeopleAtTheOffice
