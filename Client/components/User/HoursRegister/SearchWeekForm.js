import Box from '@mui/material/Box'
import Grid from '@mui/material/Grid'
import TextField from '@mui/material/TextField'
import CardContent from '@mui/material/CardContent'
import FormControl from '@mui/material/FormControl'
import Button from '@mui/material/Button'
import MenuItem from '@mui/material/MenuItem'
import Select from '@mui/material/Select'
import InputLabel from '@mui/material/InputLabel'
import React, { useEffect, useState } from "react";
import { resolveHref } from 'next/dist/shared/lib/router/router'
import { setDate } from 'date-fns'
import dateFormat from "dateformat";

export default function SearchWeekForm ({setDate, currentDay}) {

    const handleSubmit = async (event) => {
        event.preventDefault();
        const date = event.target.date.value
        setDate(date)
      }

    return (
        <CardContent>
            <h3> Search week </h3>
            <form onSubmit={handleSubmit}>
                <Grid container spacing={2}>
                    <Grid item xs={12} sm={12}>
                        <TextField  required fullWidth id='date'  type='date' placeholder='Day' />
                    </Grid>

                    <Grid item xs={12}>
                        <Button
                            fullWidth
                            variant='contained'
                            sx={{ marginRight:5 }}
                            type="submit"
                        >
                            Search
                        </Button>

                        <Button fullWidth sx={{mr: 5, mt: 1, mb: 0, backgroundColor: 'neutral.500', color: 'neutral.100'}}
                                onClick={() => setDate(dateFormat(currentDay.setDate(currentDay.getDate() - currentDay.getDay() + 1 - 7), "yyyy-mm-dd"))}> Prev </Button>
                        <Button fullWidth sx={{mr: 5, mt: 1, mb: 0, backgroundColor: 'neutral.500', color: 'neutral.100'}}
                                onClick={() => setDate(dateFormat(currentDay.setDate(currentDay.getDate() - currentDay.getDay() + 1 + 7), "yyyy-mm-dd"))}> Next </Button>
                    </Grid>
                </Grid>
            </form>
        </CardContent>
    )
};
