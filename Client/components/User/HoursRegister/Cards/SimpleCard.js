import * as React from 'react';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';



export default function OutlinedCard({date}) {
  return (    
    <Box sx={{margin: 1}}>
      <Card variant="outlined"  >
        <React.Fragment>
            <Typography align="center"  justifyContent="center" sx={{ fontSize: 14, padding: 2}} variant="h5" component="div">
            {date}
            </Typography>
        </React.Fragment>
      </Card>
    </Box>
  );
}