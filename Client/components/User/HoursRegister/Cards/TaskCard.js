import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import React from "react";
import EditModal from '../EditTask/EditModal'
import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';


export default function TaskCard({task, deleteTask, setAction, action}) {

    return (
        <Box>
            <Card variant="outlined">
                <React.Fragment>
                    <CardContent sx={{padding: 2}}>
                        <Typography align='center' sx={{fontSize: 12}} color="text.secondary" gutterBottom>
                            [{task && task.startTime.substring(0, 5)} - {task && task.finishTime.substring(0, 5)}]
                        </Typography>
                        <Typography align='center' sx={{fontSize: 16}} variant="h5" component="div">
                            {task && task.taskName}
                        </Typography>
                        <Box sx={{display: 'flex', mt: 2}}>
                            <Button sx={{padding: 0}} size="small" onClick={() => {
                                deleteTask(task.taskId)
                            }}> <DeleteOutlineIcon fontSize="small"/> </Button>
                            <EditModal task={task} setAction={setAction} action={action}/>
                        </Box>
                    </CardContent>
                </React.Fragment>
            </Card>
        </Box>
    )
}