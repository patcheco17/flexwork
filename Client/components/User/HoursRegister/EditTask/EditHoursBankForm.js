import Box from '@mui/material/Box'
import Grid from '@mui/material/Grid'
import TextField from '@mui/material/TextField'
import CardContent from '@mui/material/CardContent'
import FormControl from '@mui/material/FormControl'
import Button from '@mui/material/Button'
import MenuItem from '@mui/material/MenuItem'
import Select from '@mui/material/Select'
import InputLabel from '@mui/material/InputLabel'
import React, { useEffect, useState } from "react";
import { resolveHref } from 'next/dist/shared/lib/router/router'
import {BASE_URL, TASKS} from "../../../../config-path";
import axios from "axios";

export default function EditHoursBankForm ({task, setAction, action, handleClose}) {
    const [request, setRequest] = useState({['userId']: task.userId, ['taskId']: task.taskId})

    const handleSubmit = e => {
        e.preventDefault();
        var headers = {
            "Content-Type": 'application/json',
          }

        async function doFetch() {
            const id = task.taskId;
            await axios.patch(BASE_URL + TASKS + `${id}`,
                JSON.stringify(request),
                {headers: headers}
            )
            .then(response => {
                if (response.status === 200) {
                    setAction(!action)
                }
            }).catch(error => {
                    if(error.code === 'ECONNABORTED' || error.message === 'Network Error'){
                        alert("Something went wrong while fetching")

                    }else{
                        alert(error.response.data.detail)
                    }
                })
        }

        doFetch()
        handleClose()
    }

      const handleChange = prop => event => {
        setRequest({ ...request, [prop]: event.target.value })
    }

    return (
        <CardContent>
            <h3> Edit task </h3>
            <form onSubmit={handleSubmit}>
                <Grid container spacing={2}>
                    
                    <Grid item xs={12} sm={12}>
                        
                    <TextField 
                        fullWidth 
                        label='Task name' 
                        defaultValue={task.taskName}
                        onChange={handleChange('taskName')}
                        />
                    </Grid>

                    <Grid item xs={12}>
                        <Button variant='contained' sx={{ marginRight:5 }}
                        type="submit"
                        >
                            Save Changes
                        </Button>
                        
                    </Grid>
                </Grid>
            </form>
        </CardContent>
    )
};
