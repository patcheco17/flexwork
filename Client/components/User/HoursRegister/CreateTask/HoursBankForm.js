import Grid from '@mui/material/Grid'
import TextField from '@mui/material/TextField'
import CardContent from '@mui/material/CardContent'
import Button from '@mui/material/Button'
import React, { useState} from "react";
import {useUser} from "@auth0/nextjs-auth0";
import {BASE_URL, TASKS} from "../../../../config-path";
import LinearProgress from "@mui/material/LinearProgress";
import axios from "axios";

export default function HoursBankForm({setAction, action, setTasks, tasks}) {
    const {user, error, isLoading} = useUser();
    if (isLoading) return  <LinearProgress />;
    if (error) return <div>{error.message}</div>;

    const userId = user.sub.split('|')[1]
    const [request, setRequest] = useState({
        ['userId']: userId
    })

    const handleSubmit = e => {
        e.preventDefault();

        const headers = {
            "Content-Type": 'application/json',
        }

        async function doFetch() {
            console.log("REEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEQ", request)
            await axios.post(BASE_URL + TASKS,
                JSON.stringify(request),
                {headers: headers})
            .then(response => {
                if (response.status === 200) {
                    setTasks(tasks.concat(response.data.properties))
                    setAction(!action)
                }
            }).catch(error => {
                if(error.code === 'ECONNABORTED' || error.message === 'Network Error'){
                    alert("Something went wrong while fetching")

                }else{
                    alert(error.response.data.detail)
                }
            })
        }

        doFetch()
    }

    const handleChange = prop => event => {
        setRequest({...request, [prop]: event.target.value})
    }

    return (
        <CardContent>
            <h3> Create task</h3>
            <form onSubmit={handleSubmit}>
                <Grid container spacing={1}>
                    <Grid item xs={12} sm={12}>
                        <TextField required fullWidth label='Task name' placeholder='Task name'
                                   onChange={handleChange('taskName')}/>
                    </Grid>
                    <Grid item xs={12} sm={12}>
                        <TextField
                            required
                            fullWidth
                            type='time'
                            placeholder='Start time'
                            onChange={handleChange('startTime')}/>
                    </Grid>
                    <Grid item xs={12} sm={12}>
                        <TextField
                            required
                            fullWidth
                            type='Time'
                            placeholder='Finish time'
                            onChange={handleChange('finishTime')}
                        />
                    </Grid>
                    <Grid item xs={12} sm={12}>
                        <TextField required fullWidth type='date' placeholder='Day' onChange={handleChange('day')}/>
                    </Grid>

                    <Grid item xs={12} sm={12}>
                        <Button fullWidth variant='contained' sx={{marginRight: 5}}
                                type="submit"
                        >
                            Save
                        </Button>
                    </Grid>
                </Grid>
            </form>
        </CardContent>
    )
};

