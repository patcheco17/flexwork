import Grid from '@mui/material/Grid'
import TextField from '@mui/material/TextField'
import CardContent from '@mui/material/CardContent'
import Button from '@mui/material/Button'
import React, { useEffect, useState } from "react";

export default function HoursBankForm () {

    const [data, setData] = useState({taskName: '', startTime: '', finishTime: '', day: ''})

    const handleSubmit = e => {
        e.preventDefault();
        console.log(data)
        var myHeaders = new Headers({
            "Content-Type": 'application/json',

        });

        fetch('http://localhost:8080/workedhours', {
            method: 'post',
            body: data,
            myHeaders
        })
    };

    const handleChange = prop => event => {
        setData({ ...data, [prop]: event.target.value })
    }

    return (
        <CardContent>
            <form onSubmit={handleSubmit}>
                <Grid container spacing={2}>
                    <Grid item xs={12} sm={12}>
                        <TextField fullWidth label='Task name' placeholder='Task name'  onChange={handleChange('taskName')}/>
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField
                            fullWidth
                            type='time'
                            placeholder='Start time'
                            onChange={handleChange('startTime')} />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField
                            fullWidth
                            type='Time'
                            placeholder='Finish time'
                            onChange={handleChange('finishTime')}
                        />
                    </Grid>
                    <Grid item xs={12} sm={12}>
                        <TextField fullWidth type='date' placeholder='Day' onChange={handleChange('day')} />
                    </Grid>

                    <Grid item xs={12}>
                        <Button variant='contained' sx={{ marginRight: 3.5 }}
                                type="submit"
                        >
                            Save Changes
                        </Button>
                        <Button type='reset' variant='outlined' color='secondary' >
                            Reset
                        </Button>
                    </Grid>
                </Grid>
            </form>
        </CardContent>
    )
};