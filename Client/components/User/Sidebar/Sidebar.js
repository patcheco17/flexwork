
import { useEffect } from 'react';
import NextLink from 'next/link';
import { useRouter } from 'next/router';
import PropTypes from 'prop-types';
import { Box, Button, Divider, Drawer, Typography, useMediaQuery } from '@mui/material';
import { NavItem } from '../NavBar/nav-item';
import HomeIcon from '@mui/icons-material/Home';
import TodayIcon from '@mui/icons-material/Today';

const items = [
    {
        href: '/user/user-dashboard',
        icon: (<HomeIcon fontSize="small" />),
        title: 'Dashboard'
    },
    {
        href: '/user/book-working-space',
        icon: (<TodayIcon fontSize="small" />),
        title: 'Book Working Space'
    },
    {
        href: '/user/book-rooms',
        icon: (<TodayIcon fontSize="small" />),
        title: 'Book Rooms'
    },
    {
        href: '/user/hours-register',
        icon: (<TodayIcon fontSize="small" />),
        title: 'Hours Register'
    },
    {
        href: '/user/profile',
        icon: (<TodayIcon fontSize="small" />),
        title: 'Profile'
    }

];

export const Sidebar = (props) => {
    const { open, onClose } = props;
    const router = useRouter();
    const lgUp = useMediaQuery((theme) => theme.breakpoints.up('lg'), {
        defaultMatches: true,
        noSsr: false
    });

    useEffect(
        () => {
            if (!router.isReady) {
                return;
            }

            if (open) {
                onClose?.();
            }
        },
        // eslint-disable-next-line react-hooks/exhaustive-deps
        [router.asPath]
    );

    const content = (
        <>
            <Box sx={{display: 'flex',flexDirection: 'column',height: '100%' }}>
                <div>
                    <Box sx={{mt:8}}>

                    </Box>
                </div>

                <Divider sx={{ borderColor: 'divider',my: 3}}/>

                <Box sx={{ flexGrow: 1 }}>
                    {items.map((item) => (
                        <NavItem
                            key={item.title}
                            icon={item.icon}
                            href={item.href}
                            title={item.title}
                        />
                    ))}
                </Box>


            </Box>
        </>
    );

    if (lgUp) {
        return (
            <Drawer
                anchor="left" open
                PaperProps={{
                    sx: {
                        backgroundColor: '#F4F5FA', color: '#FFFFFF', width: 280, mt:10 }
                }}
                variant="permanent"
            >
                {content}
            </Drawer>
        );
    }

    return (
        <Drawer
            anchor="left"
            onClose={onClose}
            open={open}
            PaperProps={{
                sx: {backgroundColor: 'primary', color: '#FFFFFF', width: 280}
            }}
            sx={{ zIndex: (theme) => theme.zIndex.appBar + 100 }}
            variant="temporary"
        >
            {content}
        </Drawer>
    );
};

Sidebar.propTypes = {
    onClose: PropTypes.func,
    open: PropTypes.bool
};