// ** MUI Imports
import Card from '@mui/material/Card'
import Typography from '@mui/material/Typography'
import CardContent from '@mui/material/CardContent'
import Grid from "@mui/material/Grid";
import TextField from "@mui/material/TextField";
import EditIcon from "@mui/icons-material/Edit";
import Button from "@mui/material/Button";

import React, {useState} from "react";
import axios from "axios";
import {useRouter} from "next/router";
import {ACCESS_TOKEN, USERS_BASE_URL} from "../../../config-path";
import {getInitials} from "../../../utils/utils";
import Avatar from "@mui/material/Avatar";

const CardWithProfilePicture = ({user}) => {
    const router = useRouter()

    const [newInfo,setNewInfo] = useState({
        position: user.position,
        projectName: user.projectName,
        address: user.address,
        description: user.description
    })

    const handleChange = prop => event => {
        const value = event.target.value
        setNewInfo({ ...newInfo, [prop]: value })
    }

    async function handleUpdateUser() {
        // Validate user input
        if (!isUserInputValid(newInfo)) return

        axios.get(ACCESS_TOKEN)
            .then(res => {
                return res.data
            }).catch(err => {
                alert("Something went wrong")
                console.log(err)
                return err
        }).then(token =>{
            const path = `${USERS_BASE_URL}/${user.userId}`
            axios.patch(path, {
                    position: newInfo.position,
                    projectName: newInfo.projectName,
                    address: newInfo.address,
                    description: newInfo.description,
                },
                {
                    headers: {
                        Authorization: `Bearer ${token}`
                    }
                })
                .then(function (response) {
                    console.log("Response = ", response.data)
                    alert("User updated successfully")
                    //router.back()
                }).catch(function (error) {
                    if (error.response) {
                        console.log(error.response);
                        alert("Couldn't update user")
                    }
            })
        })
    }

    function isUserInputValid(newInfo){
        let isInputValid = false

        //Validate if there is any field empty
        if (newInfo.position.isNullOrUndefined || newInfo.position.isEmpty ||
            newInfo.projectName.isNullOrUndefined || newInfo.projectName.isEmpty ||
            newInfo.address.isNullOrUndefined || newInfo.address.isEmpty ||
            newInfo.description.isNullOrUndefined || newInfo.description.isEmpty)
            alert("All fields required")
        else isInputValid = true

        return isInputValid
    }

    return (
        <Card >

            <Avatar
                sx={{
                    width: 100,
                    height: 100,
                    marginLeft: "4.0rem",
                    marginTop: "1.0rem",
                    border: theme => `0.25rem solid ${theme.palette.common.white}`
                }}
            >
                {getInitials(user.firstName + " " + user.lastName)}
            </Avatar>

            <CardContent>
                <Grid container spacing={2}>
                    <Grid item xs={12} md={6}>
                        <Typography variant='h6'>Name:</Typography>
                        <TextField
                            fullWidth
                            disabled={true}
                            defaultValue={user.firstName.concat(' ').concat(user.lastName)}
                        />
                    </Grid>
                    <Grid item xs={12} md={6}>
                        <Typography variant='h6'>Email:</Typography>
                        <TextField
                            fullWidth
                            disabled={true}
                            defaultValue={user.email}
                        />
                    </Grid>
                    <Grid item xs={12} md={6}>
                        <Typography variant='h6'>Position:</Typography>
                        <TextField
                            fullWidth
                            defaultValue={user.position}
                            onChange={handleChange('position')}
                        />
                    </Grid>
                    <Grid item xs={12} md={6}>
                        <Typography variant='h6'>Project:</Typography>
                        <TextField
                            fullWidth
                            defaultValue={user.projectName}
                            onChange={handleChange('projectName')}
                        />
                    </Grid>
                    <Grid item xs={12} md={6}>
                        <Typography variant='h6'>Address:</Typography>
                        <TextField
                            required
                            fullWidth
                            defaultValue={user.address}
                            onChange={handleChange('address')}
                        />
                    </Grid>
                    <Grid item xs={12} md={6}>
                        <Typography variant='h6'>Description:</Typography>
                        <TextField
                            required
                            fullWidth
                            defaultValue={user.description}
                            onChange={handleChange('description')}
                        />
                    </Grid>
                </Grid>

                <Button sx={{ mt:2 }} variant="contained" onClick={handleUpdateUser} endIcon={<EditIcon />} >
                    Update
                </Button>
            </CardContent>
        </Card>
    )
}

export default CardWithProfilePicture
