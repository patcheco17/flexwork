// ** MUI Imports
import Box from '@mui/material/Box'
import Card from '@mui/material/Card'
import Button from '@mui/material/Button'
import Avatar from '@mui/material/Avatar'
import CardMedia from '@mui/material/CardMedia'
import Typography from '@mui/material/Typography'
import CardContent from '@mui/material/CardContent'
import AvatarGroup from '@mui/material/AvatarGroup'
import FullRoomForm from "../../Admin/BankHours/FullRoomForm";

const CardWithImageOnTop = ({content}) => {

    return (
        <Card sx={{ position: 'relative' }}>
            <CardMedia sx={{ height: '8rem' }} image={content.img} />
            <CardContent>
                <Box
                    sx={{
                        display: 'flex',
                        flexWrap: 'wrap',
                        alignItems: 'center',
                        justifyContent: 'space-between',
                    }}
                >
                    <Box sx={{ mr: 0, mb: 1, display: 'flex', flexDirection: 'column' }}>
                        <Typography variant='h6'>{content.name}</Typography>
                    </Box>
                    <FullRoomForm roomId={content.roomId} userId={content.userId}/>
                </Box>
            </CardContent>
        </Card>
    )
}

export default CardWithImageOnTop
