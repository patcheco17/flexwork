import * as React from 'react';
import Typography from '@mui/material/Typography';
import Pagination from '@mui/material/Pagination';
import Stack from '@mui/material/Stack';
import {useState} from "react";

export default function CustomPagination({numOfPages, page, setPage, setOffset, offset, limit}) {

    const handleChange = (event, value) => {
        if(page === value) {
            document.getElementById("pag").disabled = true
            return
        }
        if(value === 1){
            setOffset(0)
            setPage(value);
            return
        }

        if(page > value){
            setOffset(offset = limit*(value-1))
        }else{
            setOffset(offset = limit*value - limit)
        }
        setPage(value);
    };

    return (
        <Stack spacing={2}>
            <Pagination id="pag" count={numOfPages} page={page} onChange={handleChange} />
        </Stack>
    );
}
