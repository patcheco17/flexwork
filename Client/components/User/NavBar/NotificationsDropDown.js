// ** React Imports
import React, {useState, Fragment, useEffect} from 'react'

// ** Next Import
import { useRouter } from 'next/router'

// ** MUI Imports
import Box from '@mui/material/Box'
import Menu from '@mui/material/Menu'
import Badge from '@mui/material/Badge'
import { styled } from '@mui/material/styles'
import NotificationsIcon from "@mui/icons-material/Notifications";
import { IconButton, Tooltip} from "@mui/material";
import CardNotifications from "../CardsDashboard/CardNotifications";

import { initializeApp } from "firebase/app";
import { getFirestore } from 'firebase/firestore';
import { collection, query, orderBy, onSnapshot } from "firebase/firestore";


const firebaseConfig = {
    apiKey: "AIzaSyD9b-zxpH6oBHVllObL-u32U2yn_M9NZo4",
    authDomain: "flexwork---notifications.firebaseapp.com",
    projectId: "flexwork---notifications",
    storageBucket: "flexwork---notifications.appspot.com",
    messagingSenderId: "748470965673",
    appId: "1:748470965673:web:0ee5cb1c4e21c5595860b8"
};
const app = initializeApp(firebaseConfig);
const db = getFirestore(app);

// ** Styled Components
const BadgeContentSpan = styled('span')(({ theme }) => ({
    width: 8,
    height: 8,
    borderRadius: '50%',
    backgroundColor: theme.palette.success.main,
    boxShadow: `0 0 0 2px ${theme.palette.background.paper}`
}))

const NotificationsDropDown = () => {
    const [messageCache, setMessageCache] = useState([])

    const q = query(collection(db, "messages"), orderBy("createdAt"));

    useEffect(() => {
        if(messageCache){
            const unsubscribe = onSnapshot(q, (querySnapshot) => {
                const notifications = [];
                querySnapshot.forEach((doc) => {
                    if(!notifications.includes(doc.data())){
                        notifications.push(doc.data());
                        if(!wasChecked){
                            setNewNotification(true)
                        }
                    }
                });
                setMessageCache(notifications)
            });
        }

    }, []);

    const [anchorEl, setAnchorEl] = useState(null)

    // ** Hooks
    const router = useRouter()

    const handleDropdownOpen = event => {
        setAnchorEl(event.currentTarget)
        setWasChecked(true)
        setNewNotification(false)
    }

    const handleDropdownClose = url => {
        if (url) {
            router.push(url)
        }
        setAnchorEl(null)
    }


    const [newNotification, setNewNotification] = useState(false)
    const [wasChecked, setWasChecked] = useState(false)


    return (
        <Fragment>
            <Tooltip title="Notifications">
                {newNotification ?
                    <IconButton sx={{ ml: 1}}  onClick={handleDropdownOpen}>
                        <Badge color="error" badgeContent={1}>
                            <NotificationsIcon fontSize="small" />
                        </Badge>
                    </IconButton>
                        :
                    <IconButton sx={{ ml: 1}}  onClick={handleDropdownOpen}>
                        <Badge color="divider" variant="dot">
                            <NotificationsIcon fontSize="small" />
                        </Badge>
                    </IconButton>
                }
            </Tooltip>
            <Menu anchorEl={anchorEl} open={Boolean(anchorEl)} onClose={() => handleDropdownClose()} sx={{ '& .MuiMenu-paper': { width: 400, marginTop: 4 } }} anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }} transformOrigin={{ vertical: 'top', horizontal: 'right' }}>
                <Box sx={{ pt: 2, pb: 3, px: 4 }}>
                    <Box sx={{ display: 'flex', alignItems: 'center' }}>
                        <CardNotifications messageCache={messageCache}/>
                    </Box>
                </Box>
            </Menu>
        </Fragment>
    )
}

export default NotificationsDropDown