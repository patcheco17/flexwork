import PropTypes from 'prop-types';
import styled from '@emotion/styled';
import {AppBar, Avatar, Badge, Box, Button, IconButton, Toolbar, Tooltip} from '@mui/material';
import MenuIcon from '@mui/icons-material/Menu';
import NotificationsIcon from '@mui/icons-material/Notifications';
import CircleIcon from '@mui/icons-material/Circle';

import React, {useEffect} from "react";
import NextLink from "next/link";
import NotificationsDropDown from "./NotificationsDropDown";

const NavbarRoot = styled(AppBar)(({ theme }) => ({
    backgroundColor: '#F4F5FA',
    boxShadow: theme.shadows[3]
}));



export function Navbar (props) {
    const { user, userRole, onSidebarOpen, ...other } = props;

    return (
        <>
            <NavbarRoot {...other}>
                <Toolbar disableGutterssx={{minHeight: 64, left: 0,px: 2 }} >
                    <Box sx={{ml:4,p: 1, display:"flex" , color:'neutral.500',justifyContent:"center", fontSize: '2.4rem' }}>
                        <NextLink href="/" passHref >
                            <p>FlexWork</p>
                        </NextLink>
                    </Box>
                    <IconButton onClick={onSidebarOpen} sx={{display: {xs: 'inline-flex', lg: 'none'}}}>
                        <MenuIcon fontSize="small" />
                    </IconButton>


                    <Box sx={{ flexGrow: 1 }} />


                    <Button variant="outlined" color="error">
                        <a href="/api/auth/logout">Logout</a>
                    </Button>

                    <NotificationsDropDown/>

                    <Avatar sx={{ height: 40,width: 40, ml: 1}}
                            src="https://blog.lg.com.br/wp-content/uploads/2017/05/custo-funcionario-para-empresa-e1494336133827-1.jpg">
                        <CircleIcon fontSize="small" />
                    </Avatar>



                </Toolbar>
            </NavbarRoot>
        </>
    );
};

Navbar.propTypes = {
    onSidebarOpen: PropTypes.func
};
