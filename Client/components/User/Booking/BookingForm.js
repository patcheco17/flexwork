import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Grid from "@mui/material/Grid";
import TextField from "@mui/material/TextField";
import FormControl from "@mui/material/FormControl";
import InputLabel from "@mui/material/InputLabel";
import Select from "@mui/material/Select";
import MenuItem from "@mui/material/MenuItem";
import Button from "@mui/material/Button";
import React, {useEffect, useState} from "react";
import axios from "axios";
import {LinearProgress, Typography} from '@mui/material';
import {useUser} from "@auth0/nextjs-auth0";
import {showAlert, ShowAlertWithClose, showAlertWithClose} from "../../../utils/utils";
import {BOOKINGS, GET_ALL_AVAILABLE_SEATS} from "../../../config-path";

export default function BookingForm({userId, action, setAction}) {

    const [newBooking, setNewBooking] = useState({
        ['userId']: userId,
        ['day']: '',
        ['startTime']: '',
        ['finishTime']: '',
        ['roomId']: '',
    })
    const [rooms, setRooms] = useState([]);
    const [availability, setAvailability] = useState(false);
    const [roomId, setRoomId] = useState('')
    const [failPost, setFailPost] = useState(false)
    const [errorMessage, setErrorMessage] = useState([])

    const handleChange = prop => event => {
        setNewBooking({...newBooking, [prop]: event.target.value})
        if (prop === 'roomId') {
            setRoomId(event.target.value)
        }
    }


    //Make a request to the API to create the new booking
    const handleCreateBooking = e => {
        e.preventDefault();
        const currentRoom = rooms.find(room => room.id === roomId)
        console.log(currentRoom)
        if (currentRoom.roomCapacity === 0) {
            alert("This room is not available")
            return
        }

        setNewBooking({...newBooking, ['roomId']: roomId, ['userId']: userId})
        const myHeaders = new Headers({
            "Content-Type": 'application/json',
        });

        async function doFetch() {
            const result = await fetch(BOOKINGS, {
                method: 'POST',
                body: JSON.stringify(newBooking),
                headers: myHeaders
            })

            const resp = await result.json()
            if (result.ok) {
                setRoomId('')
                alert("Your booking was created")
                setAvailability(false)
                setAction(!action)
            } else {
                showAlert(resp.detail)
            }
        }

        doFetch()
    }

    function getAvailableRooms() {
        if (newBooking.day === '' || newBooking.startTime === '' || newBooking.finishTime === '') {
            alert("Please insert all the values")
            return
        }

        const headers = {
            "Content-Type": 'application/json',
        }

        const json = JSON.stringify(newBooking)
        axios.post(GET_ALL_AVAILABLE_SEATS, json, {headers: headers})
            .then(response => {
                if (response.status === 200) {
                    setRooms(response.data.properties.rooms);
                    setAvailability(true)
                    setRoomId('')
                } else {
                    alert("Something went wrong")
                    setRooms([])
                    setFailPost(true)
                    setNewBooking({ ['userId']: userId,
                        ['day']: '',
                        ['startTime']: '',
                        ['finishTime']: '',
                        ['roomId']: '',})

                }
            })
            .catch(error => {
                setRooms([])
                setFailPost(true)
                setNewBooking({ ['userId']: userId,
                    ['day']: '',
                    ['startTime']: '',
                    ['finishTime']: '',
                    ['roomId']: '',})



                if(error.code === 'ECONNABORTED' || error.message === 'Network Error'){
                    setErrorMessage("Something went wrong while creating booking")

                }else{
                    setErrorMessage(error.response.data.detail)
                }


            })
    }

    useEffect(() => {
    }, [failPost])

    console.log(rooms)

    return (
                <Card>
                    {failPost ? <ShowAlertWithClose message={errorMessage} failPost={failPost} setFailPost={setFailPost}/> :
                    <CardContent style={{backgroundColor: 'neutral.100'}}>
                        <FormControl onSubmit={getAvailableRooms}>
                            <Typography
                                color="textSecondary"
                                gutterBottom
                                variant="overline"
                            >
                                Create an appointment
                            </Typography>
                            <Grid container spacing={2}>
                                <Grid item xs={12} sm={12}>
                                    <TextField required fullWidth placeholder='Date' type='date'
                                               onChange={handleChange('day')}/>
                                </Grid>

                                <Grid item xs={12} sm={6}>
                                    <TextField required fullWidth type='Time' placeholder='Start time'
                                               onChange={handleChange('startTime')}/>
                                </Grid>
                                <Grid item xs={12} sm={6}>
                                    <TextField required fullWidth type='Time' placeholder='Finish time'
                                               onChange={handleChange('finishTime')}
                                    />
                                </Grid>

                                <Grid item xs={12} sm={6}>
                                    <Button size="large" variant='contained' onClick={getAvailableRooms}>
                                        Check rooms
                                    </Button>
                                </Grid>

                                {!availability
                                    ? <> </>
                                    :
                                    <>
                                        <Grid item xs={12} sm={6}>
                                            <InputLabel id="demo-simple-select-label">Room</InputLabel>
                                            <Select
                                                required
                                                fullWidth
                                                labelId="demo-simple-select-label"
                                                id="demo-simple-select"
                                                value={roomId}
                                                label="Room"
                                                onChange={handleChange('roomId')}
                                            >
                                                {rooms.map(room => room.roomCapacity > 0
                                                    ? <MenuItem key={room.id}
                                                                value={room.id}>{room.roomName} - {room.roomCapacity} seats</MenuItem>
                                                    : <MenuItem disabled>{room.roomName} - Not available</MenuItem>)}
                                            </Select>
                                        </Grid>
                                    </>

                                }
                                {roomId === ''
                                    ? <> </>
                                    :
                                    <Grid item xs={12} sm={6}>
                                        <Button size="large" variant='contained' onClick={handleCreateBooking}>
                                            Book
                                        </Button>
                                    </Grid>
                                }


                            </Grid>
                        </FormControl>
                    </CardContent>
                    }
                </Card>
        )
}



