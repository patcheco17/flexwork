import Table from "@mui/material/Table";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import TableCell from "@mui/material/TableCell";
import TableBody from "@mui/material/TableBody";
import Button from "@mui/material/Button";
import DeleteOutlineIcon from "@mui/icons-material/DeleteOutline";
import React from "react";
import TableContainer from "@mui/material/TableContainer";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import {LinearProgress, Typography} from "@mui/material";
import {useUser} from "@auth0/nextjs-auth0";
import {extractUserId} from "../../../utils/utils";
import {BOOKINGS} from "../../../config-path";

export default function AllBookingsTable({bookings, title, deleteFunction, action, setAction}) {
    const {user, error, isLoading} = useUser();
    if (isLoading) return <LinearProgress/>;
    if (!user) return  <LinearProgress />;
    const userId = extractUserId(user.sub);

    function deleteSchedule(scheduleId) {
        console.log(scheduleId)
        fetch(BOOKINGS, { method: 'delete', body: scheduleId })
            .then(response => {
                if (response.ok) {
                    setAction(!action)
                } else {
                    alert("error")
                }
            })
            .catch(error => {
                console.error('There was an error!', error);
            });
    }

    return (
        <Card>
            <CardContent sx={{padding: 2}}>
                <Typography
                    color="textSecondary"
                    gutterBottom
                    variant="overline"
                >
                    {title}
                </Typography>
                <TableContainer>
                    <Table aria-label='table in dashboard'>
                        <TableHead>
                            <TableRow>
                                <TableCell>Date</TableCell>
                                <TableCell>Room</TableCell>
                                <TableCell>Schedule</TableCell>
                                <TableCell>Room type</TableCell>
                                {deleteFunction && <TableCell></TableCell>}
                            </TableRow>
                        </TableHead>
                        {bookings.map(booking => (

                            <TableBody key={booking.bookingId}>
                                <TableCell>{booking.day}</TableCell>
                                <TableCell>{booking.roomName}</TableCell>
                                <TableCell>{booking.startTime.substring(0,5)} - {booking.finishTime.substring(0,5)}</TableCell>
                                {booking.isFullRoom ? <TableCell> Full room</TableCell> : <TableCell> Working space </TableCell>}
                                {deleteFunction && userId===booking.userId && <TableCell> <Button onClick={() => deleteSchedule(booking.bookingId)}> Delete </Button> </TableCell>}
                            </TableBody>
                        ))}
                    </Table>
                </TableContainer>
            </CardContent>
        </Card>
)
}

