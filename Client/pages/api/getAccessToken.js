import { withApiAuthRequired } from '@auth0/nextjs-auth0';
import axios from "axios";

// Code that needs some "love"
export default withApiAuthRequired(async function getToken(req, res) {
    try{
        let accessToken
        await axios.post('https://flexwork.eu.auth0.com/oauth/token', {
            grant_type: "client_credentials",
            client_id: "AzjmOinBypoonJtkfHtPgVs1kLbpSS0c",
            client_secret: "HDEOR3MyCWTa47729miJIarpfJCejiIWwyYyt358yEN99T13UhXmbDj2V7tFmsRF",
            audience: "https://flexwork.eu.auth0.com/api/v2/"
        })
            .then(res => {
                console.log('response = ', res.data.access_token)
                accessToken = res.data.access_token
            })
            .catch(err => {
                console.error('error = ', err)
                res.status(401).json("Error while getting token")
            })

        res.status(200).json(accessToken)

    } catch(error) {
        console.error(error)
        res.status(error.status || 500).end(error.message)
    }
});