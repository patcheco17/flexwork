import {AdminLayout} from "../../components/AppLayout/AdminLayout";
import {Grid} from "@mui/material";
import PeopleAtTheOffice from "../../components/User/CardsDashboard/PeopleAtTheOffice";
import CollaboratorsCounterCard from "../../components/Admin/Cards/CollaboratorsCounterCard"
import PastWeekInflux from "../../components/Admin/Cards/PastWeekInflux";
import AllBookingsTable from "../../components/User/Booking/AllBookingsTable";
import React, {useEffect, useState} from "react";
import {useUser, withPageAuthRequired} from "@auth0/nextjs-auth0";
import axios from "axios";
import {BOOKINGS} from "../../config-path";
import CustomPagination from "../../components/User/CustomPagination";
import PastMonthInflux from "../../components/Admin/Cards/PastMonthInflux";
import PastMonthRoomsOccupancyCard from "../../components/Admin/Cards/PastMonthRoomsOccupancyCard";
import LinearProgress from "@mui/material/LinearProgress";
const Statistics = () => {
    const {user, error, isLoading} = useUser();


    const [page, setPage] = useState(1)
    const [offset, setOffset] = useState(0)
    const [numOfPages, setNumOfPages] = useState(0)
    const [action, setAction] = useState(false)
    const [list, setList] = useState([])
    const [getFailed, setGetFailed] = useState(false)
    const [errorMessage, setErrorMessage] = useState([])

    const PAGE_SIZE = 3

    useEffect(() => {
        axios.get(BOOKINGS + `?offset=${offset}&limit=${PAGE_SIZE}`)
            .then(response => {
                if (response.status === 200) {
                    //console.log(response.data.properties.list)
                    setList(response.data.properties.list)
                    setNumOfPages(response.data.properties.size)
                    setGetFailed(false)
                } else {
                    alert("Something went wrong")
                    setGetFailed(false)
                }
            })
            .catch(error => {
                setGetFailed(true)
                setErrorMessage(error.message)
            })
    }, [action, page])

    if (isLoading) return <LinearProgress />;
    if (!user) return <LinearProgress />;


    return (
        <>
            <Grid container spacing={6} sx={{p:5}}>
                <Grid item xs={12} md={3}>
                    <CollaboratorsCounterCard />
                </Grid>

                <Grid item xs={12} md={5}>
                    <PeopleAtTheOffice />
                </Grid>

                <Grid item xs={12} md={4}>
                    <PastMonthRoomsOccupancyCard />
                </Grid>

                <Grid item xs={12} md={6}>
                    <PastWeekInflux />
                </Grid>

                <Grid item xs={12} md={6}>
                    <PastMonthInflux />
                </Grid>

                <Grid item xs={12} md={6}>
                    <AllBookingsTable bookings={list} title={'All Users Booking'} deleteFunction={false}/>
                    {numOfPages === 1 ? <></> :
                        <CustomPagination
                            numOfPages={numOfPages}
                            page={page}
                            setPage={setPage}
                            setOffset={setOffset}
                            offset={offset}
                            limit={PAGE_SIZE}/>
                    }
                </Grid>

            </Grid>
        </>
    );
}

Statistics.getLayout = (page) => (
    <AdminLayout>
        {page}
    </AdminLayout>
);

export default Statistics;

export const getServerSideProps = withPageAuthRequired();