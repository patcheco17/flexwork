import {AdminLayout} from "../../components/AppLayout/AdminLayout";
import {Alert, Box, Collapse, Grid, InputLabel, Select} from "@mui/material";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import React, {useEffect, useState} from "react";
import {AllRoomsTable} from "../../components/Admin/OfficeManagement/AllRoomsTable";
import axios from "axios";
import { withPageAuthRequired} from '@auth0/nextjs-auth0';
import {CREATE_ROOM_PATH} from "../../config-path";
import MenuItem from "@mui/material/MenuItem";


export const OfficeManagement = () => {
    const [update, setUpdate] = useState(false)
    const [newRoom, setNewRoom] = useState({
        roomName: '',
        roomCapacity: '',
        isFullRoom: ''
    })

    const [failPost, setFailPost] = useState(false)
    const [errorMessage, setErrorMessage] = useState([])
    const [roomType, setRoomType] = useState(0)

    const handleChange = prop => event => {
        setNewRoom({ ...newRoom, [prop]: event.target.value })
    }

    const handleChangeRoomType = prop => event => {
        setNewRoom({ ...newRoom, [prop]: event.target.value })
        setRoomType(event.target.value)
    }

    const handleCreateRoom = () => {
        const token = 'eyJhbGciOiJSUzI1NiIsImtpZCI6IjYzMWZhZTliNTk0MGEyZDFmYmZmYjAwNDAzZDRjZjgwYTIxYmUwNGUiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJhY2NvdW50cy5nb29nbGUuY29tIiwiYXpwIjoiNjE4MTA0NzA4MDU0LTlyOXMxYzRhbGczNmVybGl1Y2hvOXQ1Mm4zMm42ZGdxLmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwiYXVkIjoiNjE4MTA0NzA4MDU0LTlyOXMxYzRhbGczNmVybGl1Y2hvOXQ1Mm4zMm42ZGdxLmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwic3ViIjoiMTA2NDAyMDkxMjU4ODU2MzI3NTM0IiwiZW1haWwiOiJmbGV4d29ya21hbmFnZW1lbnRAZ21haWwuY29tIiwiZW1haWxfdmVyaWZpZWQiOnRydWUsImF0X2hhc2giOiIyZ3RGdUhXeXRRdWxZRUJtWVdIVkp3IiwiaWF0IjoxNjU3OTc2MjExLCJleHAiOjE2NTc5Nzk4MTEsImp0aSI6ImE5NDg2MTgyNWNmMDAyMGY3YzA0MzQ0YWE0MjRiODhkMjMyOTU2ZGQifQ.TaxQQhjtRwCjh_5csDI2IvIoeYhk7pge9i4YivU5GfInyVlvfRYmBCxmMfStGfx-bJdvxZebT_OZJoScme-o4nmp6hY7r2Fm59P06NWCD93XFvLCmWJAoceM_B_D1Lvf8kfStLuuW6N1esAk877dfXMDmlwOANSZmj5m6ujfiFDDe5MiMSbsTwlDS3ibLO0z5LjQvd5Ng0lNDaYVj1DPBIQg1Am4GshF_5sL5ly-bgHpLC6Yi2_Yz0wkXANu7RqrWgUWOG3KqZzf3A3DaSzhJCZpd0CIfQFAUEvFRV1pMcfqwc5UyAPihizfh9W9CFSb3l6W7O3JB5XUKxpY_qWMuQ'

        setFailPost(false)
        axios.post(CREATE_ROOM_PATH, {
            roomName: newRoom.roomName,
            roomCapacity: newRoom.roomCapacity,
            isFullRoom: newRoom.isFullRoom
        },{
            headers: {
                'Authorization': `Bearer ${token}`
            }
        })
            .then(res => {
                console.log(res)
            })
            .catch((error) => {
                setFailPost(true)
                console.log(error.message)
                if(error.message === "Network Error"){
                    setErrorMessage("Something went wrong creating room")
                }else{
                    setErrorMessage(error.response.data.detail)
                }
            }).finally( () => {
                setUpdate(!update)
            }
        )

    };

    function showAlert(message){
        return (
            <Box sx={{ width: '100%' }}>
                <Alert severity="error" onClose={() => {setFailPost(false)}}>{message}</Alert>
            </Box>
        )
    }


    return (
        <>
            <Grid container spacing={6} sx={{ p:6 }}>

                <Grid item xs={12} md={6}>
                    {failPost ? showAlert(errorMessage) :
                        <Card sx={{position: 'relative'}}>
                            <CardContent>
                                <Grid item xs={12} sm={12}>
                                    <h3> Create Room</h3>
                                </Grid>

                                <form>
                                    <Grid container spacing={2}>
                                        <Grid item xs={6} sm={6}>
                                            <TextField fullWidth label='Room name' onChange={handleChange('roomName')}
                                                       placeholder='Room name'/>
                                        </Grid>

                                        <Grid item xs={6} sm={6}>
                                            <TextField fullWidth label='Room Capacity'
                                                       onChange={handleChange('roomCapacity')}
                                                       placeholder='Room Capacity'/>
                                        </Grid>

                                        <Grid item xs={6} sm={8}>
                                            <InputLabel id="demo-simple-select-label">Room type</InputLabel>
                                            <Select
                                                labelId="demo-simple-select-label"
                                                id="demo-simple-select"
                                                value={roomType}
                                                label="Room type"
                                                onChange={handleChangeRoomType('isFullRoom')}
                                            >
                                                <MenuItem value={0}>Normal room</MenuItem>
                                                <MenuItem value={1}>Full room</MenuItem>
                                            </Select>
                                        </Grid>

                                        <Grid  item xs={6} sm={6}>
                                            <Button justify="flex-end" variant='contained' onClick={handleCreateRoom}
                                                    sx={{marginRight: 5}}>
                                                Create Room
                                            </Button>
                                        </Grid>


                                    </Grid>
                                </form>
                            </CardContent>

                        </Card>
                    }
                </Grid>


                <Grid item xs={12} md={6}>

                    <Card sx={{ position: 'relative' }}>
                        <CardContent>
                            <AllRoomsTable update={update} />
                        </CardContent>

                    </Card>
                </Grid>
            </Grid>
        </>
    )
}

OfficeManagement.getLayout = (page) => (
    <AdminLayout>
        {page}
    </AdminLayout>
);
export default OfficeManagement

export const getServerSideProps = withPageAuthRequired();