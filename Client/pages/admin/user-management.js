import Box from '@mui/material/Box'
import Card from '@mui/material/Card'
import TabList from '@mui/lab/TabList'
import TabPanel from '@mui/lab/TabPanel'
import TabContext from '@mui/lab/TabContext'
import { styled } from '@mui/material/styles'
import MuiTab from '@mui/material/Tab'
import {Grid} from "@mui/material";
import GroupAddIcon from '@mui/icons-material/GroupAdd';
import ListIcon from '@mui/icons-material/List';

import {AdminLayout} from "../../components/AppLayout/AdminLayout";
import TabCreateUser from "../../components/Admin/CreateUserTabs/TabCreateUser";
import TabAllUsers from "../../components/Admin/CreateUserTabs/TabAllUsers/TabAllUsers";

import React, {useState} from "react";

import {useUser, withPageAuthRequired} from "@auth0/nextjs-auth0";
import LinearProgress from "@mui/material/LinearProgress";

const Tab = styled(MuiTab)(({ theme }) => ({
    [theme.breakpoints.down('md')]: {
        minWidth: 100
    },
    [theme.breakpoints.down('sm')]: {
        minWidth: 67
    }
}))

const TabName = styled('span')(({ theme }) => ({
    lineHeight: 1.71,
    fontSize: '0.875rem',
    marginLeft: theme.spacing(2.4),
    [theme.breakpoints.down('md')]: {
        display: 'none'
    }
}))

const UserManagement = () => {
    const [value, setValue] = useState('allUsers')

    const {user, error, isLoading } = useUser();

    if (isLoading) return  <LinearProgress />;
    if (error) return <div>{error.message}</div>;

    const handleChange = (event, newValue) => {
        setValue(newValue)
    }

    return (
        <>
            <Grid container  justify = "center" spacing={6} sx={{p:5}}>
                <Grid item xs={12} md={12}>
                    <Card >
                        <TabContext value={value}>
                            <TabList onChange={handleChange} aria-label='allUsers tabs'
                                     sx={{borderBottom: theme => `1px solid ${theme.palette.divider}`}}>
                                <Tab value='allUsers'
                                     label={
                                        <Box sx={{display: 'flex', alignItems: 'center'}}>
                                            <ListIcon/>
                                            <TabName>All Users</TabName>
                                        </Box>
                                    }
                                />
                                <Tab value='createUser'
                                    label={
                                        <Box sx={{display: 'flex', alignItems: 'center'}}>
                                            <GroupAddIcon/>
                                            <TabName>Create User</TabName>
                                        </Box>
                                    }
                                />
                            </TabList>

                            <TabPanel sx={{p: 0}} value='allUsers'>
                                <TabAllUsers/>
                            </TabPanel>
                            <TabPanel sx={{p: 0}} value='createUser'>
                                <TabCreateUser/>
                            </TabPanel>

                        </TabContext>
                    </Card>
                </Grid>
            </Grid>

        </>
    );
}

UserManagement.getLayout = (page) => (
    <AdminLayout>
        {page}
    </AdminLayout>
);

export default UserManagement;

export const getServerSideProps = withPageAuthRequired();