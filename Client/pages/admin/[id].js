import CardWithProfilePicture from "../../components/User/Cards/CardWithProfilePicture";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import {useRouter} from "next/router";
import React, {useEffect, useState} from "react";
import axios from "axios";
import {AdminLayout} from "../../components/AppLayout/AdminLayout";
import Button from "@mui/material/Button";
import {ArrowBack} from "@mui/icons-material";
import {ACCESS_TOKEN, USERS_BASE_URL} from "../../config-path";
import {withPageAuthRequired} from "@auth0/nextjs-auth0";

export const Id = () => {
    const router = useRouter()

    const [isLoading, setLoading] = useState(true);
    const [user, setUser] = useState(null)

    useEffect(() => {
        const userId = router.query.id

        //Request to get access token
        axios.get(ACCESS_TOKEN)
            .then(res => {
                return res.data
            }).catch(err => {
                alert("Something went wrong")
                return err
            }).then(token => {
                // Request to get user profile
                const URL = `${USERS_BASE_URL}/${userId}`
                    axios.get( URL, {
                        headers: {
                            Authorization: `Bearer ${token}`
                        }})
                        .then(res => {
                            setUser(res.data.properties)
                            setLoading(false)
                        }).catch(err =>{
                            console.log("Error", err)
                        })
                })
    }, [])

    if (isLoading) {
        return <div> Loading... </div>;
    }

    return (
        <>
            <Grid>
                <Typography sx={{ ml: 4 , mt:4 }} variant='h3'>User's Information</Typography>
                <Button sx={{ ml: 4 , mt:2 }} variant="contained" onClick={() => router.back()} startIcon={<ArrowBack/>} >
                    Back
                </Button>
            </Grid>

            <Grid sx={{ m:4 }} >
                <Grid item xs={10} sm={8} md={8}>
                    <CardWithProfilePicture user={user}/>
                </Grid>

            </Grid>
        </>
    )
}

Id.getLayout = (page) => (
    <AdminLayout>
        {page}
    </AdminLayout>
);

export default Id

export const getServerSideProps = withPageAuthRequired();