import {AdminLayout} from "../../components/AppLayout/AdminLayout";
import BankHoursTable from "../../components/Admin/BankHours/BankHoursTable";
import {Box, Container, Grid} from "@mui/material";
import React from "react";
import {useUser, withPageAuthRequired} from "@auth0/nextjs-auth0";

export const HoursBank = () => {
    const {user, error, isLoading } = useUser();

    return (
        <>
            <Box component="main" sx={{flexGrow: 1, m:4 }}>
                <Container maxWidth={false}>
                    <Grid container spacing={3}>
                        <Grid item lg={12} sm={12} xl={12} xs={12}>
                            <BankHoursTable/>
                        </Grid>
                    </Grid>
                </Container>
            </Box>
        </>
    )
}

HoursBank.getLayout = (page) => (
    <AdminLayout>
        {page}
    </AdminLayout>
);

export default HoursBank

export const getServerSideProps = withPageAuthRequired();