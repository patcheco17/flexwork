import {AdminLayout} from "../../components/AppLayout/AdminLayout";
import React, {useEffect, useState} from 'react'
import {Alert, Box, Grid} from "@mui/material";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import CardContent from "@mui/material/CardContent";
import Card from "@mui/material/Card";
import axios from "axios";
import {CREATE_NOTIFICATION_PATH, CREATE_ROOM_PATH, DELETE_ALL_NOTIFICATIONS_PATH} from "../../config-path";
import DeleteOutlineIcon from "@mui/icons-material/DeleteOutline";
import {useUser, withPageAuthRequired} from "@auth0/nextjs-auth0";

export const Communication = () => {

    const [message, setMessage] = useState({})
    const [update, setUpdate] = useState(false)
    const [failPost, setFailPost] = useState(false)
    const [errorMessage, setErrorMessage] = useState([])
    const [successAlert, setsuccessAlert] = useState(false)

    const handleChange = prop => event => {
        setMessage({ ...message, [prop]: event.target.value })
    }

    const handleSubmit = e => {
        e.preventDefault()

        axios.post(CREATE_NOTIFICATION_PATH, {
            title: message.title,
            text: message.text
        })
            .then(res => {
                setsuccessAlert(true)
            })
            .catch((error) => {
                setFailPost(true)
                if(error.message === "Network Error"){
                    setErrorMessage("Something went wrong creating Notification")
                }else{
                    setErrorMessage(error.response.data.detail)
                }
            }).finally( () => {
                setUpdate(!update)
            }
        )
    }

    useEffect(() => {
        const timer = setTimeout(() => {
            setsuccessAlert(false)
        }, 2000);
        return () => clearTimeout(timer);
    }, [successAlert]);

    function showAlert(message){
        return (
            <Box sx={{ width: '100%' }}>
                <Alert severity="error" onClose={() => {setFailPost(false)}}>{message}</Alert>
            </Box>
        )
    }
    function deleteNotifications(){
        axios.delete(DELETE_ALL_NOTIFICATIONS_PATH, {
        } , {timeout: 10})
            .then(res => {
                alert("All Notifications were deleted")
            })
            .catch((error) => {
                console.log(error)
                alert('There was an error deleting notifications');
            })

    }


    return(
        <Box display="flex"
             justifyContent="center"
             alignItems="center">
            <Grid container spacing={12} justifyContent="center" sx={{ p:6, width:'70%'}}>
                <Grid item xs={12} md={12} lg={12} alignItems="center"  justifyContent="center" >
                    <Card sx={{backgroundColor:'neutral.100', maxWidth :'100%'}}>
                        {failPost ? showAlert(errorMessage) :
                            <CardContent>

                                <h3> Create Notification</h3>
                                <form onSubmit={handleSubmit}>
                                    <Grid container spacing={2}>
                                        <Grid item xs={12} sm={12}>
                                            <TextField required fullWidth placeholder='Write Title'
                                                       onChange={handleChange('title')}/>
                                        </Grid>
                                        <Grid item xs={12} sm={12}>
                                            <TextField required fullWidth placeholder='Write Message'
                                                       onChange={handleChange('text')}/>
                                        </Grid>
                                        <Grid item xs={12}>
                                            <Button variant='contained' sx={{marginRight: 5}}
                                                    type="submit">
                                                Save Changes
                                            </Button>
                                        </Grid>
                                        <Grid item xs={12}>
                                            {successAlert &&<Alert severity="success">Notification sent with Success!</Alert>}
                                        </Grid>
                                    </Grid>
                                </form>
                            </CardContent>
                        }
                    </Card>
                </Grid>
            </Grid>
            <Grid container spacing={12} justifyContent="center" sx={{ p:6, width:'70%'}}>
                <Grid item xs={12} md={12} lg={12} alignItems="center"  justifyContent="center" >
                    <Card sx={{backgroundColor:'neutral.100', maxWidth :'100%'}}>
                        <CardContent>
                            <h3> Delete All Notifications</h3>
                            <Button  sx={{padding:0}}size="small" onClick={() => {deleteNotifications()}}> <DeleteOutlineIcon color= 'red' fontSize="small"> </DeleteOutlineIcon> </Button>
                        </CardContent>

                    </Card>
                </Grid>
            </Grid>
        </Box>


    )
}

Communication.getLayout = (page) => (
    <AdminLayout>
        {page}
    </AdminLayout>
);

export default Communication

export const getServerSideProps = withPageAuthRequired();