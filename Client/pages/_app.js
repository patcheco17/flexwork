import React from 'react';
import { CacheProvider } from '@emotion/react';
import { ThemeProvider, CssBaseline } from '@mui/material';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import createEmotionCache from '../utility/createEmotionCache';
import '../styles/globals.css';
import lightTheme from "../styles/theme/lightTheme";
import Head from 'next/head';
import "@fullcalendar/common/main.css";
import "@fullcalendar/daygrid/main.css";
import { UserProvider } from '@auth0/nextjs-auth0';

const clientSideEmotionCache = createEmotionCache();

const MyApp = (props) => {
  const { Component, emotionCache = clientSideEmotionCache, pageProps } = props;

  const getLayout = Component.getLayout ?? ((page) => page);

  return (
      <UserProvider>
          <CacheProvider value={emotionCache}>
            <Head>
              <title>
                FlexWork
              </title>
              <meta
                  name="viewport"
                  content="initial-scale=1, width=device-width"
              />
            </Head>
            <LocalizationProvider dateAdapter={AdapterDateFns}>
              <ThemeProvider theme={lightTheme}>
                <CssBaseline />
                {getLayout(<Component {...pageProps} />)}
              </ThemeProvider>
            </LocalizationProvider>
          </CacheProvider>
      </UserProvider>
  );
};

export default MyApp;