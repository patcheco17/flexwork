
import {useState} from "react";
import {HomePageNavbar} from "../components/HomePage/HomePageNavbar/HomePageNavbar";
import {Box, Typography} from "@mui/material";
import Image from 'next/image'
import {createTheme} from "@mui/material/styles";

export const HomePage = () => {
    const [isSidebarOpen, setSidebarOpen] = useState(true);
    const theme = createTheme();

    theme.typography.h2 = {
        color: "#4E4E6A",
        fontSize: '1.2rem',
        '@media (min-width:600px)': {
            fontSize: '1.5rem',
        },
        [theme.breakpoints.up('md')]: {
            fontSize: '2.4rem',
        },
    };

    theme.typography.h6 = {
        color: "#4E4E6A",
        textShadow: "0px 5px 5px  #DDDDDD",
        fontSize: '0.8rem',
        '@media (min-width:600px)': {
            fontSize: '1rem',
        },
        [theme.breakpoints.up('md')]: {
            fontSize: '1.4rem',
        },
    };

    return (
        <>
            <HomePageNavbar onSidebarOpen={() => setSidebarOpen(true)} />
            <Box sx={{m:20, display: "flex", alignItems: "center", justifyContent: "center" }}>
                <Box sx={{mr:10}}>
                    <Typography theme={theme} variant="h2"> Welcome to FlexWork! </Typography>
                    <Typography theme={theme} variant="h6"> Do not waste your time and work with us! </Typography>
                </Box>
                <Image
                    src='/officem.svg'
                    alt="Picture of office"
                    width={600}
                    height={600}
                />
            </Box>

        </>
    );
}

export default HomePage


