import {getSession} from "@auth0/nextjs-auth0";
import {getUserRole} from "../utils/utils";

function evaluateRoles( ) {}

export async function getServerSideProps(context) {
    const session = getSession(context.req, context.res)
    //console.log(session)

    if (getUserRole(session.user) === 'admin'){
        return {
            redirect: {
                permanent: false,
                destination: `/admin/statistics`
            },
        };
    } else {
        return {
            redirect: {
                permanent: false,
                destination: `/user/user-dashboard`
            },
        };
    }
}

export default evaluateRoles