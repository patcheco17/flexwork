import {Box, Container, Grid} from '@mui/material';
import {DashboardLayout} from "../../components/AppLayout/DashboardLayout";
import PeopleAtTheOffice from "../../components/User/CardsDashboard/PeopleAtTheOffice";
import {useUser, withPageAuthRequired} from "@auth0/nextjs-auth0";
import {extractUserId, getUserRole} from "../../utils/utils";
import AllBookingsTable from "../../components/User/Booking/AllBookingsTable";
import React, {useEffect, useState} from "react";
import axios from "axios";
import CustomPagination from "../../components/User/CustomPagination";
import Card from "@mui/material/Card";
import {BOOKINGS_BY_USER} from "../../config-path";
import LinearProgress from "@mui/material/LinearProgress";

const Dashboard = () => {
    const {user, error, isLoading} = useUser();
    if (isLoading) return <LinearProgress />;
    if (!user) return <LinearProgress />;

    const [page, setPage] = useState(1)
    const [offset, setOffset] = useState(0)
    const [numOfPages, setNumOfPages] = useState(0)
    const [action, setAction] = useState(false)
    const [list, setList] = useState([])
    const [getFailed, setGetFailed] = useState(false)
    const [errorMessage, setErrorMessage] = useState([])

    const userId = extractUserId(user.sub);
    const PAGE_SIZE = 5
    useEffect(() => {
        axios.get(BOOKINGS_BY_USER + `?userId=${userId}&offset=${offset}&limit=${PAGE_SIZE}`)
            .then(response => {
                if (response.status === 200) {
                    console.log(response.data.properties.list)
                    setList(response.data.properties.list)
                    setNumOfPages(response.data.properties.size)
                    setGetFailed(false)
                } else {
                    alert("Something went wrong")
                    setGetFailed(false)
                }
            })
            .catch(error => {
                setGetFailed(true)
                setErrorMessage(error.message)
            })
    }, [action, page])

    return (
        <>
            <Box component="main" sx={{flexGrow: 1, py: 8}}>
                <Container maxWidth={false}>
                    <Grid container spacing={3}>
                        {}
                        <Grid item xs={12} sm={12} lg={12} xl={6}>
                            <PeopleAtTheOffice />
                        </Grid>

                        <Grid item xs={12} sm={12} lg={12} xl={6}>
                            <Card>
                                <AllBookingsTable bookings={list} title={user.nickname + "'s bookings"}
                                                  deleteFunction={false}/>
                                {numOfPages === 1 ? <></> :
                                    <CustomPagination numOfPages={numOfPages} page={page} setPage={setPage}
                                                      setOffset={setOffset} offset={offset} limit={PAGE_SIZE}/>}
                            </Card>
                        </Grid>

                        {}
                    </Grid>
                </Container>
            </Box>
        </>
    );
}

Dashboard.getLayout = (page) => (
    <DashboardLayout>
        {page}
    </DashboardLayout>
);

export default Dashboard;

export const getServerSideProps = withPageAuthRequired();