import {DashboardLayout} from "../../components/AppLayout/DashboardLayout";
import Grid from "@mui/material/Grid";
import Card from "@mui/material/Card";
import SimpleCard from '../../components/User/HoursRegister/Cards/SimpleCard'
import HoursBankForm from '../../components/User/HoursRegister/CreateTask/HoursBankForm'
import React, {useEffect, useState} from "react";
import dateFormat from "dateformat";
import SearchWeekForm from '../../components/User/HoursRegister/SearchWeekForm'
import { Box, LinearProgress, Container} from "@mui/material";
import axios from 'axios';
import {useUser, withPageAuthRequired} from "@auth0/nextjs-auth0";
import {extractUserId, getUserRole, showAlert, ShowAlertWithClose} from "../../utils/utils";
import TaskCard from "../../components/User/HoursRegister/Cards/TaskCard";
import {BASE_URL, TASKS, WEEKLY_TASKS_1, USER_ID} from "../../config-path";


var week = []

function getWeek(date) {
    week = []
    const curr = new Date(date)
    var first
    for (let i = 1; i <= 7; i++) {
        if (curr.getDay() == 0) {
            first = curr.getDate() - curr.getDay() + i - 7
        } else {
            first = curr.getDate() - curr.getDay() + i
        }
        const dayOfWeek = dateFormat(curr.setDate(first), "ddd, dd mmm, yyyy")
        week.push(dayOfWeek)
    }
}


function HoursRegister() {
    const {user, error, isLoading} = useUser();
    if (!user) {
        return <LinearProgress/>
    }
    const currentDay = new Date(dateFormat(week[0], "yyyy-mm-dd"));
    const [date, setDate] = useState(dateFormat(currentDay, "yyyy-mm-dd"))
    const [tasks, setTasks] = useState([])
    const [action, setAction] = useState(false)
    const [failFetch, setFailFetch] = useState(false)
    const [errorMessage, setErrorMessage] = useState("")
    const [loading, setLoading] = useState(true)


    useEffect(() => {
        function doFetch() {
            setLoading(true)
            getWeek(date)
            const userId = extractUserId(user.sub)
            const WEEKLY_PATH = replaceUrlValues(date, userId)
            axios.get(BASE_URL + WEEKLY_PATH, {}, {timeout: 5000})
                .then(response => {
                    setTasks(response.data.properties.tasks)
                    setFailFetch(false)
                })
                .catch(error => {
                    setErrorMessage("Couldn't establish connection to the server")
                    setFailFetch(true)
                }).finally(() =>
                setLoading(false)
            );
        }

        doFetch()
    }, [action, date])


    function replaceUrlValues(date, userId){
        return WEEKLY_TASKS_1 + date + USER_ID + userId
    }

    function deleteTask(taskId) {
        fetch(BASE_URL + TASKS + `${taskId}`, {method: 'delete'})
            .then(response => {
                if (response.ok) {
                    const filteredTasks = tasks.filter(t => t.taskId !== taskId)
                    setTasks(filteredTasks)
                    setAction(!action)
                } else {
                    alert(task.detail)
                }
            })
            .catch(error => {
                setErrorMessage(error.message)
                setFailFetch(true)
            });
    }

    return (
        <div>
            <Box component="main" sx={{flexGrow: 1, m: 4}}>
                <Container maxWidth={false}>
                    {loading ? <LinearProgress color="inherit"/> :
                        failFetch ? <ShowAlertWithClose message={errorMessage} failPost={action}
                                                         setFailPost={setAction} />:
                        <Card sx={{backgroundColor: 'neutral.100'}}>

                                <Grid p={3} sx={{py: 0}} container spacing={1}>
                                    <Grid container item xs={12} md={2}>
                                        <Card sx={{width: '100%', mb: 2}}>
                                            <HoursBankForm setAction={setAction} action={action} setTasks={setTasks}
                                                           tasks={tasks}> </HoursBankForm>
                                        </Card>
                                        <Card sx={{width: '100%'}}>
                                            <SearchWeekForm currentDay={currentDay} setDate={setDate}/>
                                        </Card>
                                    </Grid>

                                    <Grid container item xs={12} md={10}>
                                        {week.map(day => (
                                            <Grid item xs={12} md={1.7} key={day}>

                                                <SimpleCard variant="outlined" date={day} key={day}> </SimpleCard>
                                                <Grid item xs={12}
                                                      display="flex"
                                                      flexDirection="column"
                                                    // justifyContent="flex-end" # DO NOT USE THIS WITH 'scroll'
                                                      height="700px" // fixed the height
                                                      style={{
                                                          overflow: "hidden",
                                                          overflowY: "scroll" // added scroll
                                                      }}>
                                                    {tasks.map(task => {
                                                            if (dateFormat(day, "yyyy-mm-dd") === task.day) {
                                                                return <TaskCard variant="outlined"
                                                                                 deleteTask={deleteTask}
                                                                                 setAction={setAction} action={action}
                                                                                 task={task}
                                                                                 key={task.taskId}> </TaskCard>
                                                            }
                                                        }
                                                    )}
                                                </Grid>
                                            </Grid>
                                        ))}
                                    </Grid>
                                </Grid>
                            }
                        </Card>
                    }
                </Container>
            </Box>
        </div>
    )
}

HoursRegister.getLayout = (page) => (
    <DashboardLayout>
        {page}
    </DashboardLayout>
);

export default HoursRegister

export const getServerSideProps = withPageAuthRequired();
