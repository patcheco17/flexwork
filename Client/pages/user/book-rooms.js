import {DashboardLayout} from "../../components/AppLayout/DashboardLayout";
import CardWithImageOnTop from "../../components/User/Cards/CardWithImageOnTop";
import Typography from "@mui/material/Typography";
import Grid from "@mui/material/Grid";
import {Box, LinearProgress} from "@mui/material";
import {withPageAuthRequired} from "@auth0/nextjs-auth0";
import {useUser} from '@auth0/nextjs-auth0';
import React, {useEffect, useRef, useState} from "react";
import axios from "axios";
import {GET_BOOKABLE_ROOMS_PATH} from "../../config-path";
import {extractUserId, getUserRole} from "../../utils/utils";

export const BookRooms = () => {
    const [rooms, setRooms] = useState([])
    const [loading, setLoading] = useState(true)
    const [failFetch, setFailFetch] = useState(false)
    const {user, error, isLoading} = useUser();
    const userId = useRef(-1);
    const [page, setPage] = useState(1)
    const [action, setAction] = useState(false)

    useEffect(() => {
        if (!user) {
            return <LinearProgress/>
        }

        userId.current = extractUserId(user.sub)

        setLoading(true)
        setFailFetch(false)
        axios.get(GET_BOOKABLE_ROOMS_PATH, {}, {timeout: 10})
            .then(res => {
                const roomsReturned = res.data.properties.rooms;
                setRooms(roomsReturned);
            })
            .catch((error) => {
                setFailFetch(true)
            }).finally(() =>
            setLoading(false)
        );

    }, [user, action, page])


    return (
        <>
            <Box sx={{m: 4}}>

                <Grid container md={12} item spacing={2}>
                    <Grid sx={{margin: 0}} md={12} container spacing={1}>
                        <Grid item xs={12} sx={{paddingBottom: 0}}>
                            <Typography variant='h5'>Book Room</Typography>
                        </Grid>
                        {rooms.map(room => (
                            <Grid spacing={1} key={room.id} item xs={12} sm={6} md={3}>
                                <CardWithImageOnTop s content={
                                    {
                                        name: room.roomName,
                                        img: 'https://www.wework.com/ideas/wp-content/uploads/sites/4/2021/08/20201008-199WaterSt-2_v1-scaled.jpg',
                                        roomId: room.id,
                                        userId: userId.current.valueOf()
                                    }}/>
                            </Grid>
                        ))}


                    </Grid>


                </Grid>
            </Box>
        </>
    )
}

BookRooms.getLayout = (page) => (
    <DashboardLayout>
        {page}
    </DashboardLayout>
);

function getAllBookings() {

}

export default BookRooms

export const getServerSideProps = withPageAuthRequired();