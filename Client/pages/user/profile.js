import {DashboardLayout} from "../../components/AppLayout/DashboardLayout";
import React, {useEffect, useState} from "react";
import {useUser, withPageAuthRequired} from "@auth0/nextjs-auth0";
import axios from "axios";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import {ArrowBack} from "@mui/icons-material";
import CardWithProfilePicture from "../../components/User/Cards/CardWithProfilePicture";
import {useRouter} from "next/router";
import {ACCESS_TOKEN, USERS_BASE_URL} from "../../config-path";
import {extractUserId} from "../../utils/utils";
import LinearProgress from "@mui/material/LinearProgress";

const Profile = () => {
    const router = useRouter()
    const { user, error, isLoading } = useUser();

    if (isLoading) return <LinearProgress />;
    if (error) return <div>{error.message}</div>;

    const [isLoadingResponse, setIsLoadingResponse] = useState(true);
    const [userInfo, setUserInfo] = useState(null)

    useEffect(() => {
        //Request to get access token
        axios.get(ACCESS_TOKEN)
            .then(res => {
                return res.data
            }).catch(err => {
            alert(err)
            return err
        }).then(token => {
            // Request to get user profile
            const URL = `${USERS_BASE_URL}/${extractUserId(user.sub)}`
            axios.get( URL, {
                headers: {
                    Authorization: `Bearer ${token}`
                }})
                .then(res => {
                    console.log("Response = ", res)
                    setUserInfo(res.data.properties)
                    setIsLoadingResponse(false)
                    console.log("User = ", user)
                }).catch(err =>{
                console.log("Error", err)
            })
        })
    }, [])

    if (isLoadingResponse) {
        return <LinearProgress />;
    }

    return(
        <>
            <Grid>
                <Typography sx={{ ml: 4 , mt:4 }} variant='h3'>User Profile</Typography>
                <Button sx={{ ml: 4 , mt:2 }} variant="contained" onClick={() => router.back()} startIcon={<ArrowBack/>} >
                    Back
                </Button>

            </Grid>

            <Grid sx={{ m:4 }} >
                <Grid item xs={10} sm={8} md={8}>
                    <CardWithProfilePicture user={userInfo}/>
                </Grid>

            </Grid>
        </>
    )
}

Profile.getLayout = (page) => (
    <DashboardLayout>
        {page}
    </DashboardLayout>
);

export default Profile;

export const getServerSideProps = withPageAuthRequired();