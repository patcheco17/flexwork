import {DashboardLayout} from "../../components/AppLayout/DashboardLayout";
import {useUser, withPageAuthRequired} from "@auth0/nextjs-auth0";
import React, {useState, useEffect} from "react";
import Grid from "@mui/material/Grid";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import axios from 'axios';
import {Box} from "@mui/material";
import CustomPagination from "../../components/User/CustomPagination"
import BookingForm from "../../components/User/Booking/BookingForm";
import {extractUserId, getUserRole, showAlert, ShowAlertWithClose} from "../../utils/utils";
import AllBookingsTable from "../../components/User/Booking/AllBookingsTable"
import {GET_ALL_NORMAL_ROOMS_BOOKINGS_BY_USER} from "../../config-path";
import LinearProgress from "@mui/material/LinearProgress";

const BookWorkingSpace = () => {
    const {user, error, isLoading} = useUser();
    if (isLoading) return  <LinearProgress />;
    if (!user) return  <LinearProgress />;
    const userId = extractUserId(user.sub);
    const PAGE_SIZE = 10
    const [page, setPage] = useState(1)
    const [offset, setOffset] = useState(0)
    const [numOfPages, setNumOfPages] = useState(0)
    const [action, setAction] = useState(false)
    const [list, setList] = useState([])
    const [getFailed, setGetFailed] = useState(false)
    const [errorMessage, setErrorMessage] = useState([])


    useEffect(() => {
        axios.get(GET_ALL_NORMAL_ROOMS_BOOKINGS_BY_USER + `?userId=${userId}&offset=${offset}&limit=${PAGE_SIZE}`, {timeout: 2000} )
            .then(response => {
                if (response.status === 200) {
                    console.log(response.data.properties.list)
                    setList(response.data.properties.list)
                    setNumOfPages(response.data.properties.size)
                    setGetFailed(false)
                }
            })
            .catch(error => {

                setGetFailed(true)
                if(error.code === 'ECONNABORTED' || error.message === 'Network Error'){
                    setErrorMessage("Something went wrong while fetching")

                }else{
                    setErrorMessage(error.response.data.detail)
                }

                console.log(error)


            })
    }, [action, page])


    return (
        <>
            <Card sx={{m: 6}}>
                <CardContent>
                    <Grid justifyContent="center" container spacing={2}>
                        <Grid item xs={12} sm={12} md={6}>
                            <Card>
                                {getFailed ? <ShowAlertWithClose message={errorMessage} failPost={getFailed}
                                                                 setFailPost={setGetFailed}/> :
                                    <Box>
                                        <AllBookingsTable bookings={list}
                                                          title={user.nickname + "'s bookings"}
                                                          deleteFunction={true}
                                                          setAction={setAction}
                                                          action={action}

                                        />

                                        {numOfPages === 1 ? <></> :
                                            <CustomPagination numOfPages={numOfPages} page={page} setPage={setPage}
                                                              setOffset={setOffset} offset={offset} limit={PAGE_SIZE}/>
                                        }
                                    </Box>
                                }
                            </Card>
                        </Grid>

                        <Grid item xs={12} sm={12} md={6}>
                            <BookingForm userId={userId} action={action} setAction={setAction} />
                        </Grid>

                    </Grid>
                </CardContent>
            </Card>
        </>
    )
}

BookWorkingSpace.getLayout = (page) => (
    <DashboardLayout>
        {page}
    </DashboardLayout>
);

export default BookWorkingSpace;

export const getServerSideProps = withPageAuthRequired();