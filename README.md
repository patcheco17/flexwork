Para aceder ao repositório onde se encontra o código desenvolvido, basta aceder
ao seguinte link do Bitbucket.

https://bitbucket.org/patcheco17/flexwork/src/master/

Após o acesso ao repositório, é necessária a realização de clone para a máquina
local.

<p> git clone https://patcheco17@bitbucket.org/patcheco17/flexwork.git </p>

Depois de realizado o clone do código para a máquina local, deverá utilizar-se
uma janela do terminal para aceder à pasta ”Client” presente na diretoria ”flexwork”
e, dentro desta pasta, realizar os comandos 

<p> npm install  </p>
<p> npm run build </p>

para a instalação das bibliotecas necessárias ao projeto e realização do processo de build do mesmo,
respetivamente.


Ainda na aplicação cliente, de modo a aceder às funcionalidades do Auth0, é ne-
cessário adicionar o ficheiro ”.env.local” (ou adicionar às variáveis de ambiente) as
seguintes configurações:

<p> AUTH0_SECRET=’77d6da8dbe1160b069aab82dd0882e0d’ </p>
<p> AUTH0_BASE_URL=’http://localhost:8888’ </p>
<p> AUTH0_ISSUER_BASE_URL=’https://flexwork.eu.auth0.com’ </p>
<p> AUTH0_CLIENT_ID=’JXYW6TNQsDyOkTRykZZE5AiI1AENRkqj’ </p>
<p> AUTH0_CLIENT_SECRET=
’hLALdB7tExqH5xD6qLDglzjfcPbt3CO3eaM6tEEAVxqHYYjVt1yXe6GZG7MYKWXI’ </p>


Concluídos estes passos, é necessário um servidor de MySQL para acesso às bases
dados utilizadas. Sugere-se o download no website oficial.
Sugere-se que, na configuração das bases de dados, sejam utilizadas as credenciais
username = root e password = password para uma configuração mais simplificada do
projeto. Caso não sejam seguidas estas instruções, será necessário alterar, conforme as
credenciais desejadas, o username e a password no ficheiro ”application.yml” localizado
na diretoria ”server/<NomeDaAPI>/src/main/resources/” de cada projeto.

Depois da configuração, dever-se-á realizar os comandos da criação do modelo de
dados, presente nos ficheiros ”script.sql” de cada projeto.

Após estes passos, basta fazer build e correr cada serviço num terminal.
De modo a testar a aplicação, o login deverá ser feito como administrador com as
credenciais: email = admin@gmail.com e a password = 123ProjetoFinal. Para testar as
funcionalidades do colaborador, deverá ser criado um novo a partir da página ”User
Management” do administrador, realizar o logout e voltar a fazer login com as creden-
ciais criadas para esse novo trabalhador.