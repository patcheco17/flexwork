package com.example.notificationsapi

import com.example.notificationsapi.dto.AllNotificationsOutputModel
import com.example.notificationsapi.dto.NotificationOutputModel
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class NotificationServices @Autowired constructor(notificationsRepo: NotificationRepository){
    val notifRepo = notificationsRepo


    fun saveNotification(notif: Notification): NotificationOutputModel? {
        return notifRepo.save(notif)
    }

    fun deleteAllNotifications(): Boolean {
        return notifRepo.deleteAll()
    }

    fun getAllNotifications(): AllNotificationsOutputModel? {
        return notifRepo.getAllNotifications()
    }


}