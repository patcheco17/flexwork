package com.example.notificationsapi

import com.example.notificationsapi.Exceptions.DataBaseException
import ProblemJson
import com.example.common.*
import com.example.notificationsapi.dto.AllNotificationsOutputModel
import com.example.notificationsapi.dto.NotificationInputModel
import com.example.notificationsapi.dto.NotificationOutputModel
import com.example.notificationsapi.dto.toSirenObject
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.servlet.http.HttpServletRequest

@RestController
@CrossOrigin(origins = arrayOf("*"))
class NotificationController @Autowired constructor(private val notificationService: NotificationServices){

    @CrossOrigin(origins = arrayOf("*"))
    @GetMapping(NOTIFICATIONS)
    @ResponseBody
    fun getAllNotifications(): SirenEntity<AllNotificationsOutputModel>? {
        return notificationService.getAllNotifications()?.toSirenObject(
            links = listOf(selfLink("/notifications/")),
            actions = null
        )
    }

    @CrossOrigin(origins = arrayOf("*"))
    @PostMapping(NOTIFICATIONS)
    @ResponseBody
    fun saveNotification(req: HttpServletRequest, @RequestBody newNotification: NotificationInputModel): SirenEntity<NotificationOutputModel>? {

        val notif: Notification = newNotification.toNotification()
        val outputModel: NotificationOutputModel = notificationService.saveNotification(notif) ?: throw Exception()

        return outputModel.toSirenObject(
            links = listOf(selfLink("/notification/")),
            actions = null
        )
    }

    @CrossOrigin(origins = arrayOf("*"))
    @DeleteMapping(NOTIFICATIONS)
    @ResponseBody
    fun deleteAllNotifications(): Boolean {
        return notificationService.deleteAllNotifications()
    }

    @ExceptionHandler(value = [DataBaseException::class])
    fun handle500Exception(e : Exception) = ResponseEntity
        .badRequest()
        .contentType(MediaType.APPLICATION_PROBLEM_JSON)
        .body(
            when(e::class){
                    DataBaseException::class -> ProblemJson(
                        type = "/rooms",
                        title = "Database Error",
                        detail = "Something went wrong when fetching data from database",
                        status = 500
                    )
                else -> null
            }

        )
    @ExceptionHandler(value = [Exception::class])
    fun handleException(e : Exception) = ResponseEntity
        .badRequest()
        .contentType(MediaType.APPLICATION_PROBLEM_JSON)
        .body(
            when(e::class){
                Exception::class -> ProblemJson(
                    type = "/rooms",
                    title = "Internal Server Error",
                    detail = "Something went wrong",
                    status = 500
                )
                else -> null
            }

        )

}