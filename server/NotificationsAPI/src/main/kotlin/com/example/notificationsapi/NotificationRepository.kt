package com.example.notificationsapi


import com.example.notificationsapi.Exceptions.DataBaseException
import com.example.notificationsapi.dto.AllNotificationsOutputModel
import com.example.notificationsapi.dto.NotificationOutputModel
import com.google.api.core.ApiFuture
import com.google.cloud.firestore.Firestore
import com.google.cloud.firestore.QueryDocumentSnapshot
import com.google.cloud.firestore.QuerySnapshot
import com.google.firebase.cloud.FirestoreClient
import org.springframework.stereotype.Repository


@Repository
class NotificationRepository() {

    fun save(notif: Notification): NotificationOutputModel? {
        val dbFirestore: Firestore = FirestoreClient.getFirestore()
        dbFirestore.collection("messages").document(notif.title.toString()).set(notif)
        return NotificationOutputModel(notif.title.toString() , notif.text.toString())
    }

    fun deleteAll(): Boolean{
        val dbFirestore: Firestore = FirestoreClient.getFirestore()
        val collection = dbFirestore.collection("messages")

        try {
            val future: ApiFuture<QuerySnapshot> = collection.get()
            val documents: List<QueryDocumentSnapshot> = future.get().getDocuments()
            if(documents.isEmpty()){
                throw DataBaseException("There's no notification for this user")
            }
            for (document in documents) {
                document.getReference().delete()
            }
        } catch (e: Exception) {
            throw DataBaseException("Something went wrong in the database")
        }

        return true
    }

    fun getAllNotifications(): AllNotificationsOutputModel? {
        val notifications: MutableList<NotificationOutputModel> = ArrayList()

        val dbFirestore: Firestore = FirestoreClient.getFirestore()
        val collection = dbFirestore.collection("messages")

        try {
            val future: ApiFuture<QuerySnapshot> = collection.get()
            val documents: List<QueryDocumentSnapshot> = future.get().getDocuments()
            if(documents.isEmpty()){
                throw DataBaseException("There's no notification to display")
            }
            for (document in documents) {
                notifications.add(
                     NotificationOutputModel(
                        document.getString("title").toString(),
                        document.getString("text").toString(),
                        ),
                    )
            }
        } catch (e: Exception) {
            throw DataBaseException("Something went wrong in the database")
        }

        return AllNotificationsOutputModel(notifications)
    }


}