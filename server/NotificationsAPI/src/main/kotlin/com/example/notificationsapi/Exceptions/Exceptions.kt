package com.example.notificationsapi.Exceptions

class DataBaseException(message: String) : Exception(message)

class InvalidArgumentException(message: String) : Exception(message)
