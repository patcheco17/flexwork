package com.example.notificationsapi.dto

import com.example.common.SirenAction
import com.example.common.SirenEntity
import com.example.common.SirenLink
import com.example.notificationsapi.Exceptions.InvalidArgumentException
import com.example.notificationsapi.Notification
import com.fasterxml.jackson.annotation.JsonCreator

class NotificationInputModel @JsonCreator constructor(
    private val title: String,
    private val text: String

)  {

    fun toNotification(): Notification {
        if(text.isEmpty() || title.isEmpty()){
            throw InvalidArgumentException("Missing notification attributes")
        }

        return Notification(
            title,
            text,
            System.currentTimeMillis()
        )

    }
}

class NotificationOutputModel(
    val title: String,
    val text: String
){
    constructor( title: String, not: Notification ) :
            this(
                title!!,
                not.text!!
            )
}


/**
 * Extension function that produces the Siren version of the resource's external representation
 *
 * @param [links]  the list of links to be included.
 * @param [actions]  the list of actions to be included.
 * @return The corresponding siren representation
 */
fun NotificationOutputModel.toSirenObject(links: List<SirenLink>, actions: List<SirenAction>? = null) = SirenEntity(
    properties = this,
    clazz = listOf("Notification"),
    links = links,
    actions = actions
)


class AllNotificationsOutputModel(val notifications: MutableList<NotificationOutputModel>?)

/**
 * Extension function that produces the Siren version of the resource's external representation
 *
 * @param [links]  the list of links to be included.
 * @param [actions]  the list of actions to be included.
 * @return The corresponding siren representation
 */
fun AllNotificationsOutputModel.toSirenObject(links: List<SirenLink>, actions: List<SirenAction>? = null) = SirenEntity(
    properties = this,
    clazz = listOf("Notification"),
    links = links,
    actions = actions
)