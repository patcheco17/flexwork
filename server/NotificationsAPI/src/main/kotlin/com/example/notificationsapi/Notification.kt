package com.example.notificationsapi

class Notification(
    val title:  String?,
    val text:  String?,
    val createdAt: Long
)