package com.example.notificationsapi

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class NotificationsApiApplication

fun main(args: Array<String>) {
    runApplication<NotificationsApiApplication>(*args)
}

