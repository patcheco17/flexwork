package com.example.ScheduleAPI.sqlcommands

/**
 * Schedule sql queries
 */
const val CREATE_USER =
    "SELECT INTO user (userId, firstName, lastName, email, address, description, projectName, position)" +
    "VALUES (?, ?, ?, ?, ?, ?, ?, ?);"
