package com.example.ScheduleAPI

import com.example.ScheduleAPI.exceptions.InvalidValueInsertedException
import com.fasterxml.jackson.annotation.JsonCreator
import java.time.LocalDate
import java.time.LocalTime
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeParseException
import java.time.format.FormatStyle
import java.util.*

class ScheduleInputModel @JsonCreator constructor(
    private val userId: String,
    private val startTime: String,
    private val finishTime: String,
    private val day: String,
    private val roomId: Int,
) {

    fun toSchedule(): Schedule {
        val date: LocalDate;
        try {
            date = LocalDate.parse(day, DateTimeFormatter.ISO_DATE)
        } catch (e: DateTimeParseException) {
            throw InvalidValueInsertedException("Invalid date format")
        }
        val now = LocalDate.now()

        if (date.isBefore(now)) {
            throw InvalidValueInsertedException("Invalid date. Inserted date is before today's date [$date]")
        }

        val localStartTime =
            LocalTime.parse(startTime, DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT).withLocale(Locale.FRANCE))
        val localFinishTime =
            LocalTime.parse(finishTime, DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT).withLocale(Locale.FRANCE))

        if (localFinishTime.isBefore(localStartTime)) throw InvalidValueInsertedException("Invalid time inserted. Finish time is before start time!")

        return Schedule(
            userId,
            localStartTime,
            localFinishTime,
            date,
            roomId,
        )
    }
}

class ScheduleOutputModel(
    val bookingId: Int,
    val userId: String?,
    val startTime: LocalTime?,
    val finishTime: LocalTime?,
    val day: LocalDate?,
    val roomId: Int?,
    var roomName: String? = "",
    var isFullRoom: Boolean? = false
) {}

fun ScheduleOutputModel.toSirenObject(links: List<SirenLink>, actions: List<SirenAction>? = null) = SirenEntity(
    properties = this,
    clazz = listOf("Schedule"),
    links = links,
    actions = actions
)

class ScheduleOutputModelList(var list: MutableList<ScheduleOutputModel>?, val size: Int) {}

fun ScheduleOutputModelList.toSirenObject(links: List<SirenLink>, actions: List<SirenAction>? = null) = SirenEntity(
    properties = this,
    clazz = listOf("Schedule"),
    links = links,
    actions = actions
)

class DesksOutputModelList(val roomName: String, val deskList: MutableList<ScheduleOutputModel>?) {}

class RoomInputModel(val roomId: Int, val roomName: String, val roomCapacity: Int) {}

class RoomInputModelList(val roomsList: Array<RoomInputModel>)

class Room(val roomId: Int, val currentCapacity: Int)

class RoomOutputModel(val id: Int, val roomName: String, var roomCapacity: Int, var isFullRoom: Boolean)

class AllRoomsOutputModel(val rooms: MutableList<RoomOutputModel>?)

fun AllRoomsOutputModel.toSirenObject(links: List<SirenLink>, actions: List<SirenAction>? = null) = SirenEntity(
    properties = this,
    clazz = listOf("Room"),
    links = links,
    actions = actions
)

class CheckRoom(
    val startTime: LocalTime,
    val finishTime: LocalTime,
    val day: LocalDate
)

class CheckRoomInputModel @JsonCreator constructor(
    private val startTime: String,
    private val finishTime: String,
    private val day: String,
) {
    fun toCheckRoom(): CheckRoom {
        val date: LocalDate;
        try {
            date = LocalDate.parse(day, DateTimeFormatter.ISO_DATE)
        } catch (e: DateTimeParseException) {
            throw InvalidValueInsertedException("Invalid date format")
        }
        val now = LocalDate.now()

        if (date.isBefore(now)) {
            throw InvalidValueInsertedException("Invalid date. Inserted date is before today's date [$date]")
        }

        val localStartTime = LocalTime.parse(
            startTime, DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT).withLocale(
                Locale.FRANCE
            )
        )
        val localFinishTime = LocalTime.parse(
            finishTime, DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT).withLocale(
                Locale.FRANCE
            )
        )

        if (localFinishTime.isBefore(localStartTime)) throw InvalidValueInsertedException("Invalid time inserted. Finish time is before start time!")

        return CheckRoom(localStartTime, localFinishTime, date)
    }
}