package com.example.ScheduleAPI.common

const val BASE_URL_CLIENT = "https://frontend-flexwork-4hupxege7q-uc.a.run.app"
const val BOOKINGS_BY_USER = "/bookings/user"
const val BOOKINGS = "/bookings"
const val GET_ALL_BOOKINGS_BY_ROOM = "/bookings/rooms"
const val GET_ALL_NORMAL_ROOMS_BOOKINGS_BY_USER = "/bookings/normal-room"
const val POST_FULL_ROOM_BOOKING = "/bookings/full-room"
const val GET_ALL_AVAILABLE_SEATS = "/seats"
const val OFFICE_MANAGEMENT_URL = "https://office-management-api-4hupxege7q-uc.a.run.app/"

const val GET_ALL_CURRENT_DAY = "/schedule/day"
const val GET_ALL_PAST_WEEK = "/schedule/past-week"
const val GET_ALL_PAST_MONTH = "/schedule/past-month"
