package com.example.ScheduleAPI.exceptions

class BookingNotFoundException(message: String) : Exception(message)

class InternalServiceException(message: String) : Exception(message)

class DataBaseException(message: String) : Exception(message)

class NoDataBaseSelectedException(message: String) : Exception(message)

class InvalidValueInsertedException(message: String) : Exception(message)

class BookingScheduleNotAllowed(message: String) : Exception(message)

class OfficeManagementConnectException(message: String) : Exception(message)

