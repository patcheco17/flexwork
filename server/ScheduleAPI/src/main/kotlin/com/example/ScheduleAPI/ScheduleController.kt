package com.example.ScheduleAPI

import com.example.ScheduleAPI.common.*
import com.example.ScheduleAPI.exceptions.*
import org.jdbi.v3.core.ConnectionException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpMethod
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.net.URI
import javax.servlet.http.HttpServletRequest


@RestController
@CrossOrigin(origins = arrayOf("*"))
class ScheduleController {
    private lateinit var scheduleServices: ScheduleServices

    @RequestMapping
    @Autowired
    fun ScheduleController(_scheduleServices: ScheduleServices) {
        scheduleServices = _scheduleServices
    }

    @CrossOrigin(origins = arrayOf("*"))
    @PostMapping(BOOKINGS)
    @ResponseBody
    fun createSchedule(
        req: HttpServletRequest,
        @RequestBody newBooking: ScheduleInputModel
    ): SirenEntity<ScheduleOutputModel> {
        val booking = newBooking.toSchedule()
        val outputModel = scheduleServices.post(booking)
        return outputModel!!.toSirenObject(
            links = listOf(selfLink("/offset/")),
            actions = listOf(
                SirenAction("delete-booking", URI(BOOKINGS), "Delete booking", method = HttpMethod.DELETE, type = MediaType.APPLICATION_JSON),
                SirenAction("post-booking", URI(BOOKINGS), "post booking",  method = HttpMethod.POST, type = MediaType.APPLICATION_JSON),
                SirenAction("create-full-room-booking", URI(POST_FULL_ROOM_BOOKING), "Create full room booking",  method = HttpMethod.POST, type = MediaType.APPLICATION_JSON),
                SirenAction("get-all-normal-bookings-by-user", URI(GET_ALL_NORMAL_ROOMS_BOOKINGS_BY_USER), "Get all normal bookings by user",  method = HttpMethod.GET, type = MediaType.APPLICATION_JSON),
                SirenAction("get-all-bookings-by-user", URI(BOOKINGS_BY_USER), "Get all normal bookings by user",  method = HttpMethod.GET, type = MediaType.APPLICATION_JSON),
                SirenAction("get-all-bookings", URI(BOOKING), "Get all bookings",  method = HttpMethod.GET, type = MediaType.APPLICATION_JSON),
                SirenAction("get-all-bookings-by-room", URI(GET_ALL_BOOKINGS_BY_ROOM), "Get all bookings by room",  method = HttpMethod.GET, type = MediaType.APPLICATION_JSON),
            )
        )
    }

    @CrossOrigin(origins = arrayOf("*"))
    @DeleteMapping(BOOKINGS)
    @ResponseBody
    fun deleteSchedule(
        req: HttpServletRequest,
        @RequestBody id: String
    ): Int {
        return scheduleServices.delete(id.toInt())
    }

    @CrossOrigin(origins = arrayOf("*"))
    @GetMapping(GET_ALL_BOOKINGS_BY_ROOM)
    @ResponseBody
    fun getAllBookingsByRoom(
        req: HttpServletRequest,
        @RequestParam("offset") offset: Int,
        @RequestParam("limit") limit: Int,
        @RequestParam("roomId") roomId: Int
    ): SirenEntity<ScheduleOutputModelList>? {
        return scheduleServices.getAllBookingsByRoom(offset, limit, roomId)
            ?.toSirenObject(
                links = listOf(selfLink("/offset/")),
                actions = listOf(
                    SirenAction("delete-booking", URI(BOOKINGS), "Delete booking", method = HttpMethod.DELETE, type = MediaType.APPLICATION_JSON),
                    SirenAction("post-booking", URI(BOOKINGS), "post booking",  method = HttpMethod.POST, type = MediaType.APPLICATION_JSON),
                    SirenAction("create-full-room-booking", URI(POST_FULL_ROOM_BOOKING), "Create full room booking",  method = HttpMethod.POST, type = MediaType.APPLICATION_JSON),
                    SirenAction("get-all-normal-bookings-by-user", URI(GET_ALL_NORMAL_ROOMS_BOOKINGS_BY_USER), "Get all normal bookings by user",  method = HttpMethod.GET, type = MediaType.APPLICATION_JSON),
                    SirenAction("get-all-bookings-by-user", URI(BOOKINGS_BY_USER), "Get all normal bookings by user",  method = HttpMethod.GET, type = MediaType.APPLICATION_JSON),
                    SirenAction("get-all-bookings", URI(BOOKING), "Get all bookings",  method = HttpMethod.GET, type = MediaType.APPLICATION_JSON),
                    )
            )
    }

    @CrossOrigin(origins = arrayOf("*"))
    @GetMapping(GET_ALL_NORMAL_ROOMS_BOOKINGS_BY_USER)
    fun getAllNormalRoomsBookingsByUser(
        @RequestParam("offset") offset: Int,
        @RequestParam("limit") limit: Int,
        @RequestParam("userId") userId: String
    ): SirenEntity<ScheduleOutputModelList> {
        return scheduleServices.getAllNormalRoomsBookingsByUser(offset, limit, userId)
            .toSirenObject(
                links = listOf(selfLink("/offset/")),
                actions = listOf(
                    SirenAction("delete-booking", URI(BOOKINGS), "Delete booking", method = HttpMethod.DELETE, type = MediaType.APPLICATION_JSON),
                    SirenAction("post-booking", URI(BOOKINGS), "post booking",  method = HttpMethod.POST, type = MediaType.APPLICATION_JSON),
                    SirenAction("create-full-room-booking", URI(POST_FULL_ROOM_BOOKING), "Create full room booking",  method = HttpMethod.POST, type = MediaType.APPLICATION_JSON),
                    SirenAction("get-all-bookings-by-user", URI(BOOKINGS_BY_USER), "Get all normal bookings by user",  method = HttpMethod.GET, type = MediaType.APPLICATION_JSON),
                    SirenAction("get-all-bookings", URI(BOOKING), "Get all bookings",  method = HttpMethod.GET, type = MediaType.APPLICATION_JSON),
                    SirenAction("get-all-bookings-by-room", URI(GET_ALL_BOOKINGS_BY_ROOM), "Get all bookings by room",  method = HttpMethod.GET, type = MediaType.APPLICATION_JSON),
                )
            )
    }

    @CrossOrigin(origins = arrayOf("*"))
    @GetMapping(BOOKINGS_BY_USER)
    fun getAllBookingsByUser(
        @RequestParam("offset") offset: Int,
        @RequestParam("limit") limit: Int,
        @RequestParam("userId") userId: String
    ): SirenEntity<ScheduleOutputModelList> {
        return scheduleServices.getAllBookingsByUser(offset, limit, userId)
            .toSirenObject(
                links = listOf(selfLink("/offset/")),
                actions = listOf(
                    SirenAction("delete-booking", URI(BOOKINGS), "Delete booking", method = HttpMethod.DELETE, type = MediaType.APPLICATION_JSON),
                    SirenAction("post-booking", URI(BOOKINGS), "post booking",  method = HttpMethod.POST, type = MediaType.APPLICATION_JSON),
                    SirenAction("create-full-room-booking", URI(POST_FULL_ROOM_BOOKING), "Create full room booking",  method = HttpMethod.POST, type = MediaType.APPLICATION_JSON),
                    SirenAction("get-all-normal-bookings-by-user", URI(GET_ALL_NORMAL_ROOMS_BOOKINGS_BY_USER), "Get all normal bookings by user",  method = HttpMethod.GET, type = MediaType.APPLICATION_JSON),
                    SirenAction("get-all-bookings", URI(BOOKING), "Get all bookings",  method = HttpMethod.GET, type = MediaType.APPLICATION_JSON),
                    SirenAction("get-all-bookings-by-room", URI(GET_ALL_BOOKINGS_BY_ROOM), "Get all bookings by room",  method = HttpMethod.GET, type = MediaType.APPLICATION_JSON),
                )
            )
    }

    @CrossOrigin(origins = arrayOf("*"))
    @PostMapping(GET_ALL_AVAILABLE_SEATS)
    @ResponseBody
    fun getAvailableDesks(@RequestBody inputModel: CheckRoomInputModel): SirenEntity<AllRoomsOutputModel>? {
        val checkRoom = inputModel.toCheckRoom()
        return scheduleServices.getAvailableDesks(checkRoom.day, checkRoom.startTime, checkRoom.finishTime)
            ?.toSirenObject(
                links = listOf(selfLink("/rooms/")),
                actions = null
            )
    }

    @CrossOrigin(origins = arrayOf("*"))
    @PostMapping(POST_FULL_ROOM_BOOKING)
    @ResponseBody
    fun createFullRoomBooking(
        req: HttpServletRequest,
        @RequestBody newBooking: ScheduleInputModel
    ): SirenEntity<ScheduleOutputModel> {
        val booking = newBooking.toSchedule()
        val outputModel = scheduleServices.createFullRoomBooking(booking)
        return outputModel!!.toSirenObject(
            links = listOf(selfLink("/offset/")),
            actions = listOf(
                SirenAction("delete-booking", URI(BOOKINGS), "Delete booking", method = HttpMethod.DELETE, type = MediaType.APPLICATION_JSON),
                SirenAction("post-booking", URI(BOOKINGS), "post booking",  method = HttpMethod.POST, type = MediaType.APPLICATION_JSON),
                SirenAction("get-all-normal-bookings-by-user", URI(GET_ALL_NORMAL_ROOMS_BOOKINGS_BY_USER), "Get all normal bookings by user",  method = HttpMethod.GET, type = MediaType.APPLICATION_JSON),
                SirenAction("get-all-bookings-by-user", URI(BOOKINGS_BY_USER), "Get all normal bookings by user",  method = HttpMethod.GET, type = MediaType.APPLICATION_JSON),
                SirenAction("get-all-bookings", URI(BOOKING), "Get all bookings",  method = HttpMethod.GET, type = MediaType.APPLICATION_JSON),
                SirenAction("get-all-bookings-by-room", URI(GET_ALL_BOOKINGS_BY_ROOM), "Get all bookings by room",  method = HttpMethod.GET, type = MediaType.APPLICATION_JSON),
            )
        )
    }

    @CrossOrigin(origins = arrayOf("*"))
    @GetMapping(BOOKINGS)
    @ResponseBody
    fun getAllBookings(
        req: HttpServletRequest,
        @RequestParam("offset") offset: Int,
        @RequestParam("limit") limit: Int
    ): SirenEntity<ScheduleOutputModelList>? {
        return scheduleServices.getAllBookings(offset, limit)
            ?.toSirenObject(
                links = listOf(selfLink("/offset/")),
                actions = listOf(
                    SirenAction("delete-booking", URI(BOOKINGS), "Delete booking", method = HttpMethod.DELETE, type = MediaType.APPLICATION_JSON),
                    SirenAction("post-booking", URI(BOOKINGS), "post booking",  method = HttpMethod.POST, type = MediaType.APPLICATION_JSON),
                    SirenAction("create-full-room-booking", URI(POST_FULL_ROOM_BOOKING), "Create full room booking",  method = HttpMethod.POST, type = MediaType.APPLICATION_JSON),
                    SirenAction("get-all-normal-bookings-by-user", URI(GET_ALL_NORMAL_ROOMS_BOOKINGS_BY_USER), "Get all normal bookings by user",  method = HttpMethod.GET, type = MediaType.APPLICATION_JSON),
                    SirenAction("get-all-bookings-by-user", URI(BOOKINGS_BY_USER), "Get all normal bookings by user",  method = HttpMethod.GET, type = MediaType.APPLICATION_JSON),
                    SirenAction("get-all-bookings-by-room", URI(GET_ALL_BOOKINGS_BY_ROOM), "Get all bookings by room",  method = HttpMethod.GET, type = MediaType.APPLICATION_JSON),
                )
            )
    }

    /**
     * Endpoint that returns all schedules made by the users for current day
     */
    @CrossOrigin(origins = arrayOf("*"))
    @GetMapping(GET_ALL_CURRENT_DAY)
    @ResponseBody
    fun getSchedulesCurrentDay( req: HttpServletRequest ): SirenEntity<ScheduleOutputModelList>? {
        return scheduleServices.getAllCurrentDay()
            ?.toSirenObject(
                links = listOf(selfLink("/schedule/day/")),
                actions = listOf(
                    SirenAction("get-bookings-for-past-week", URI(GET_ALL_PAST_WEEK), "Get bookings for past week", method = HttpMethod.GET, type = MediaType.APPLICATION_JSON),
                    SirenAction("get-bookings-for-past-month", URI(GET_ALL_PAST_MONTH), "Get bookings for past month, ",  method = HttpMethod.GET, type = MediaType.APPLICATION_JSON),
                    )
            )
    }

    /**
     * Endpoint that returns all schedules made by the all users for past week
     */
    @CrossOrigin(origins = arrayOf("*"))
    @GetMapping(GET_ALL_PAST_WEEK)
    @ResponseBody
    fun getSchedulesForPastWeek( req: HttpServletRequest ): SirenEntity<ScheduleOutputModelList>? {
        return scheduleServices.getAllPastWeek()
            ?.toSirenObject(
                links = listOf(selfLink("/schedule/past-week/")),
                actions = listOf(
                    SirenAction("get-bookings-for-past-month", URI(GET_ALL_PAST_MONTH), "Get bookings for past month, ",  method = HttpMethod.GET, type = MediaType.APPLICATION_JSON),
                    SirenAction("get-bookings-current-day", URI(GET_ALL_PAST_WEEK), "Get bookings for current day", method = HttpMethod.GET, type = MediaType.APPLICATION_JSON),
                )
            )
    }

    /**
     * Endpoint that returns all schedules made by the all users for past month
     */
    @CrossOrigin(origins = arrayOf("*"))
    @GetMapping(GET_ALL_PAST_MONTH)
    @ResponseBody
    fun getSchedulesForPastMonth( req: HttpServletRequest ): SirenEntity<ScheduleOutputModelList>? {
        return scheduleServices.getAllPastMonth()
            ?.toSirenObject(
                links = listOf(selfLink("/schedule/past-month/")),
                actions = listOf(
                    SirenAction("get-bookings-for-past-week", URI(GET_ALL_PAST_WEEK), "Get bookings for past week", method = HttpMethod.GET, type = MediaType.APPLICATION_JSON),
                    SirenAction("get-bookings-current-day", URI(GET_ALL_PAST_WEEK), "Get bookings for current day", method = HttpMethod.GET, type = MediaType.APPLICATION_JSON),
                )
            )
    }

    @ExceptionHandler(value = [InvalidValueInsertedException::class])
    fun handleInvalidValueInsertedException(e: InvalidValueInsertedException) = ResponseEntity
        .badRequest()
        .contentType(MediaType.APPLICATION_PROBLEM_JSON)
        .body(
            ProblemJson(
                type = "/schedule",
                title = "BAD REQUEST",
                detail = e.message.toString(),
                status = 400
            )
        )

    @ExceptionHandler(value = [BookingScheduleNotAllowed::class])
    fun handleBookingScheduleNotAllowed(e: BookingScheduleNotAllowed) = ResponseEntity
        .badRequest()
        .contentType(MediaType.APPLICATION_PROBLEM_JSON)
        .body(
            ProblemJson(
                type = "/schedule",
                title = "BAD REQUEST",
                detail = e.message.toString(),
                status = 400
            )
        )

    @ExceptionHandler(value = [BookingNotFoundException::class])
    fun handleBookingNotFoundException(e: BookingNotFoundException) = ResponseEntity
        .badRequest()
        .contentType(MediaType.APPLICATION_PROBLEM_JSON)
        .body(
            ProblemJson(
                type = "/schedule",
                title = "NOT FOUND",
                detail = e.message.toString(),
                status = 404
            )
        )

    @ExceptionHandler(
        value = [
            DataBaseException::class,
            NoDataBaseSelectedException::class,
            InternalServiceException::class
        ]
    )
    fun handle500Exceptions(e: Exception) = ResponseEntity
        .internalServerError()
        .contentType(MediaType.APPLICATION_PROBLEM_JSON)
        .body(
            when (e::class) {

                DataBaseException::class -> ProblemJson(
                    type = "/bookings",
                    title = "Database Error",
                    detail = "Something went wrong when fetching data from database",
                    status = 500
                )

                NoDataBaseSelectedException::class -> ProblemJson(
                    type = "/bookings",
                    title = "Database Error",
                    detail = "Could not establish connection to the database",
                    status = 500
                )

                InternalServiceException::class -> ProblemJson(
                    type = "/bookings",
                    title = "Internal Error",
                    detail = e.message.toString(),
                    status = 500
                )

                ConnectionException::class -> ProblemJson(
                    type = "/bookings",
                    title = "Internal Error",
                    detail = "Couldn't create ",
                    status = 500
                )

                else -> null
            }

        )

}