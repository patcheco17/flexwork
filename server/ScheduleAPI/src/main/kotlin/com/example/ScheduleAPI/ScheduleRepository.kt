package com.example.ScheduleAPI

import com.example.ScheduleAPI.exceptions.BookingNotFoundException
import com.example.ScheduleAPI.exceptions.DataBaseException
import com.example.ScheduleAPI.exceptions.NoDataBaseSelectedException
import org.jdbi.v3.core.Handle
import org.jdbi.v3.core.Jdbi
import org.jdbi.v3.core.statement.UnableToExecuteStatementException
import org.springframework.stereotype.Repository
import java.time.LocalDate
import java.time.LocalTime

@Repository
class ScheduleRepository(var jdbi: Jdbi?) {

    fun add(booking: Schedule): ScheduleOutputModel? {

        val bookingOutputModel: ScheduleOutputModel?
        try {
            bookingOutputModel = jdbi?.withHandle<ScheduleOutputModel, RuntimeException> { handle: Handle ->
                handle.createUpdate(
                    "insert into booking (userId, startTime, finishTime, day, roomId) VALUES(?, ?, ? ,? ,?) "
                )
                    .bind(0, booking.userId)
                    .bind(1, booking.startTime)
                    .bind(2, booking.finishTime)
                    .bind(3, booking.day)
                    .bind(4, booking.roomId)
                    .execute()

                handle.createQuery("SELECT * FROM booking where bookingId= (SELECT LAST_INSERT_ID())")
                    .mapTo(ScheduleOutputModel::class.java).one()
            }
        } catch (e: UnableToExecuteStatementException) {
            if (e.cause?.message.toString() == "No database selected")
                throw NoDataBaseSelectedException("No database selected")
            else
                throw DataBaseException("Database error")
        } catch (e: Exception) {
            throw DataBaseException("Database error")
        }

        return bookingOutputModel
    }

    fun checkTimeSlot(booking: Schedule): Int {
        try {
            val numOfBookings = jdbi?.withHandle<Int?, RuntimeException> { handle: Handle ->
                handle.createQuery("select count(bookingId) from booking where day=? AND startTime <= ? AND finishTime >= ? AND roomId=?")
                    .bind(0, booking.day)
                    .bind(1, booking.finishTime)
                    .bind(2, booking.startTime)
                    .bind(3, booking.roomId)
                    .mapTo(Int::class.java).one()
            } ?: -1

            return numOfBookings

        } catch (e: UnableToExecuteStatementException) {
            if (e.cause?.message.toString() == "No database selected")
                throw NoDataBaseSelectedException("No database selected")
            else
                throw DataBaseException("Database error")
        } catch (e: Exception) {
            throw DataBaseException("Database error")
        }
    }

    fun delete(id: Int): Int {
        try {
            val operationId = (jdbi?.withHandle<Int, RuntimeException> { handle: Handle ->
                handle.createUpdate("DELETE FROM booking WHERE bookingId = ?;")
                    .bind(0, id)
                    .execute()
            }) ?: -1

            if (operationId == 0) throw BookingNotFoundException("Booking with id : ${id} not found")

            return id
        } catch (e: UnableToExecuteStatementException) {
            if (e.cause?.message.toString() == "No database selected")
                throw NoDataBaseSelectedException("No database selected")
            else
                throw DataBaseException("Database error")
        } catch (e: Exception) {
            throw DataBaseException("Database error")
        }
    }

    fun getAllBookingsByUser(offset: Int, limit: Int, userId: String): ScheduleOutputModelList {
        try {
            val now = LocalDate.now()
            val list: MutableList<ScheduleOutputModel>? =
                jdbi?.withHandle<MutableList<ScheduleOutputModel>?, RuntimeException> { handle: Handle ->
                    handle.createQuery("select * from booking where userId=? AND day>=? order by day, startTime asc LIMIT ? OFFSET ? ")
                        .bind(0, userId)
                        .bind(1, now)
                        .bind(2, limit)
                        .bind(3, offset)
                        .mapTo(ScheduleOutputModel::class.java).list()
                }

            var numOfPages: Int = ((jdbi?.withHandle<Int, RuntimeException> { handle: Handle ->
                handle.createQuery("select count(*) as numOfElements from booking where userId=? AND day>=?")
                    .bind(0, userId)
                    .bind(1, now)
                    .mapTo(Int::class.java).one()
            }) ?: 0)

            if (numOfPages % limit > 0) {
                numOfPages = numOfPages / limit + 1
            } else {
                numOfPages = numOfPages / limit
            }

            return ScheduleOutputModelList(list, numOfPages)

        } catch (e: UnableToExecuteStatementException) {
            if (e.cause?.message.toString() == "No database selected")
                throw NoDataBaseSelectedException("No database selected")
            else
                throw DataBaseException("Database error")
        } catch (e: Exception) {
            throw DataBaseException("Database error")
        }
    }


    fun getAllBookings(offset: Int, limit: Int): ScheduleOutputModelList {
        try {
            val now = LocalDate.now()
            val list: MutableList<ScheduleOutputModel>? =
                jdbi?.withHandle<MutableList<ScheduleOutputModel>?, RuntimeException> { handle: Handle ->
                    handle.createQuery("select * from booking WHERE day>=? order by day, startTime asc LIMIT ? OFFSET ? ")
                        .bind(0, now)
                        .bind(1, limit)
                        .bind(2, offset)
                        .mapTo(ScheduleOutputModel::class.java).list()
                }

            var numOfPages: Int = ((jdbi?.withHandle<Int, RuntimeException> { handle: Handle ->
                handle.createQuery("select count(*) as numOfElements from booking where day>=?")
                    .bind(0, now)
                    .mapTo(Int::class.java).one()
            }) ?: 0)

            if (numOfPages % limit > 0) {
                numOfPages = numOfPages / limit + 1
            } else {
                numOfPages = numOfPages / limit
            }
            return ScheduleOutputModelList(list, numOfPages)
        } catch (e: UnableToExecuteStatementException) {
            if (e.cause?.message.toString() == "No database selected")
                throw NoDataBaseSelectedException("No database selected")
            else
                throw DataBaseException("Database error")
        } catch (e: Exception) {
            throw DataBaseException("Database error")
        }

    }

    fun getAllBookingsByRoom(offset: Int, limit: Int, roomId: Int): ScheduleOutputModelList {
        try {
            val now = LocalDate.now()
            val list: MutableList<ScheduleOutputModel>? =
                jdbi?.withHandle<MutableList<ScheduleOutputModel>?, RuntimeException> { handle: Handle ->
                    handle.createQuery("select * from booking where roomId=? AND day>=? order by day, startTime asc LIMIT ? OFFSET ? ")
                        .bind(0, roomId)
                        .bind(1, now)
                        .bind(2, limit)
                        .bind(3, offset)
                        .mapTo(ScheduleOutputModel::class.java).list()
                }

            var numOfPages: Int = ((jdbi?.withHandle<Int, RuntimeException> { handle: Handle ->
                handle.createQuery("select count(*) as numOfElements from booking where roomId=? AND day>=?")
                    .bind(0, roomId)
                    .bind(1, now)
                    .mapTo(Int::class.java).one()
            }) ?: 0)

            if (numOfPages % limit > 0) {
                numOfPages = numOfPages / limit + 1
            } else {
                numOfPages = numOfPages / limit
            }

            return ScheduleOutputModelList(list, numOfPages)

        } catch (e: UnableToExecuteStatementException) {
            if (e.cause?.message.toString() == "No database selected")
                throw NoDataBaseSelectedException("No database selected")
            else
                throw DataBaseException("Database error")
        } catch (e: Exception) {
            throw DataBaseException("Database error")
        }
    }


    fun getAvailableDesks(day: LocalDate, startTime: LocalTime, finishTime: LocalTime): MutableList<Room>? {
        val roomsList: MutableList<Room>?

        try {
            roomsList = jdbi?.withHandle<MutableList<Room>, RuntimeException> { handle: Handle ->
                handle.createQuery("select roomId, count(bookingId) as currentCapacity from booking where day=? AND startTime <= ? AND finishTime >= ? group by roomId  ")
                    .bind(0, day)
                    .bind(1, finishTime)
                    .bind(2, startTime)
                    .mapTo(Room::class.java).list()
            } ?: mutableListOf()

            return roomsList

        } catch (e: UnableToExecuteStatementException) {
            if (e.cause?.message.toString() == "No database selected")
                throw NoDataBaseSelectedException("No database selected")
            else
                throw DataBaseException("Database error")
        } catch (e: Exception) {
            throw DataBaseException("Database error")
        }
    }

    /**
     * Gets all the bookings made by all users for current day
     */
    fun getAllCurrDay(): ScheduleOutputModelList? {
        val currentDay = LocalDate.now()
        //val currentDay = "2022-07-11"
        val scheduleListCurrDay: MutableList<ScheduleOutputModel>? =
            jdbi?.withHandle<MutableList<ScheduleOutputModel>?, RuntimeException>{ handle: Handle ->
                //TODO: change Query
                handle.createQuery("SELECT * FROM booking where day=?")
                    .bind(0, currentDay)
                    .mapTo(ScheduleOutputModel::class.java)
                    .list()
            }

        scheduleListCurrDay?.map { schedule -> {
            schedule.startTime.toString()
            schedule.finishTime.toString()
            schedule.day.toString()
        } }

        //TODO: change this 2, it's related to the paging
        return ScheduleOutputModelList(scheduleListCurrDay, 2)
    }

    /**
     * Gets all the bookings made by all users for past week
     */
    fun getAllBetweenDates(start: LocalDate, finish: LocalDate): ScheduleOutputModelList?{
        val scheduleListPastWeek: MutableList<ScheduleOutputModel>? =
            jdbi?.withHandle<MutableList<ScheduleOutputModel>?, RuntimeException>{ handle: Handle ->
                handle.createQuery("SELECT * FROM booking where day >= ? AND day <= ?")
                    .bind(0, start)
                    .bind(1, finish)
                    .mapTo(ScheduleOutputModel::class.java)
                    .list()
            }

        scheduleListPastWeek?.map { schedule -> {
            schedule.startTime.toString()
            schedule.finishTime.toString()
            schedule.day.toString()
        } }

        //TODO: change this 1000, it's related to the paging
        return ScheduleOutputModelList(scheduleListPastWeek, 1000)
    }

}
