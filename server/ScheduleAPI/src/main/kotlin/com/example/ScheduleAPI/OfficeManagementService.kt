package com.example.ScheduleAPI

import retrofit2.Call
import retrofit2.http.GET

interface OfficeManagementService {

    @GET("rooms")
    fun getAllRooms(): Call<SirenEntity<AllRoomsOutputModel>>

    @GET("rooms/full-bookable")
    fun getFullBookableRooms(): Call<SirenEntity<AllRoomsOutputModel>>

    @GET("rooms/non-full-bookable")
    fun getNonFullBookableRooms(): Call<SirenEntity<AllRoomsOutputModel>>

}