CREATE DATABASE BOOKING;
USE booking;

create table booking (
    bookingId serial primary key,
    userId varchar(50),
    startTime Time,
    finishTime Time,
    day Date,
    room varchar(12)
);


insert into booking (userId, startTime, finishTime, day, roomId, desk)
VALUES(2, '09:10', '18:19', '2022-05-30', 'Room 1', 'Desk 1');

SELECT * FROM booking where bookingId= (SELECT LAST_INSERT_ID())
drop table booking

SELECT * FROM booking
//vs
SELECT COUNT(*) from booking ROWS ONLY
SELECT * FROM booking where userId = 2  LIMIT 3 OFFSET 0

SELECT * FROM booking where day >= '2022-07-09' order by day asc

select count(userId)  from booking as total

select roomId, count(bookingId) as currentCapacity from booking group by roomId
select room, count(bookingId) as currentCapacity from booking where day='2022-07-08' AND startTime <= '20:30' AND finishTime >= '11:30' group by room
