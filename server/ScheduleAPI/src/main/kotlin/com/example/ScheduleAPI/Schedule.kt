package com.example.ScheduleAPI

import java.time.LocalDate
import java.time.LocalTime

class Schedule (
    val userId: String,
    val startTime: LocalTime,
    val finishTime: LocalTime,
    val day: LocalDate,
    val roomId: Int,
)
