package com.example.ScheduleAPI

import com.example.ScheduleAPI.common.OFFICE_MANAGEMENT_URL
import com.example.ScheduleAPI.exceptions.BookingScheduleNotAllowed
import com.example.ScheduleAPI.exceptions.InternalServiceException
import com.google.gson.GsonBuilder
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.net.ConnectException
import java.time.DayOfWeek
import java.time.LocalDate
import java.time.LocalTime
import java.time.ZoneId
import java.time.temporal.TemporalAdjusters
import java.util.*
import kotlin.collections.HashMap


@Service
class ScheduleServices {

    private lateinit var bookingRepo: ScheduleRepository
    private lateinit var service: OfficeManagementService

    @Autowired
    fun officeManagementServices(_bookingRepo: ScheduleRepository) {
        this.bookingRepo = _bookingRepo

        val gson = GsonBuilder()
            .setLenient()
            .create()

        val retrofit = Retrofit.Builder()
            .baseUrl(OFFICE_MANAGEMENT_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()

        service = retrofit.create(OfficeManagementService::class.java)
    }

    fun post(booking: Schedule): ScheduleOutputModel? {
        val currentRoomsState = getAvailableDesks(booking.day, booking.startTime, booking.finishTime);

        if (currentRoomsState != null) {
            if (currentRoomsState.rooms?.size != 0) {
                val wantedRoom = currentRoomsState.rooms?.find { room -> room.id == booking.roomId }
                if (wantedRoom != null) {
                    if (wantedRoom.roomCapacity == 0) {
                        throw BookingScheduleNotAllowed("This booking is not available. Try another")
                    }
                }
            }
        }

        return bookingRepo.add(booking)
    }

    fun createFullRoomBooking(booking: Schedule): ScheduleOutputModel? {
        val numOfBookings = bookingRepo.checkTimeSlot(booking)
        if (numOfBookings > 0) {
            throw BookingScheduleNotAllowed("Couldn't book this time slot. Check existing bookings.")
        }
        return bookingRepo.add(booking)
    }

    fun delete(id: Int): Int {
        return bookingRepo.delete(id)
    }

    fun getAllNormalRoomsBookingsByUser(offset: Int, limit: Int, userId: String): ScheduleOutputModelList {
        val roomsList = getAllNormalRooms()
        val bookingOutputModelList = bookingRepo.getAllBookingsByUser(offset, limit, userId)

        val bookingList = bookingOutputModelList.list

        //filter the wanted rooms
        bookingOutputModelList.list = bookingList?.filter { booking -> booking.roomId == roomsList?.find { room -> room.id == booking.roomId }?.id }?.toMutableList()


        //assign the room's name
        bookingList?.map { booking ->
            booking.roomName = roomsList?.find { room -> room.id == booking.roomId }?.roomName
            booking.isFullRoom = roomsList?.find { room -> room.id == booking.roomId }?.isFullRoom
        }
        return bookingOutputModelList
    }

    fun getAllBookingsByUser(offset: Int, limit: Int, userId: String): ScheduleOutputModelList {
        val roomsList = getAllRooms()
        val bookingOutputModelList = bookingRepo.getAllBookingsByUser(offset, limit, userId)

        val bookingList = bookingOutputModelList.list

        //filter the wanted rooms
        bookingOutputModelList.list = bookingList?.filter { booking -> booking.roomId == roomsList?.find { room -> room.id == booking.roomId }?.id }?.toMutableList()


        //assign the room's name
        bookingList?.map { booking ->
            booking.roomName = roomsList?.find { room -> room.id == booking.roomId }?.roomName
            booking.isFullRoom = roomsList?.find { room -> room.id == booking.roomId }?.isFullRoom
        }
        return bookingOutputModelList
    }

    fun getAllBookings(offset: Int, limit: Int): ScheduleOutputModelList {
        val roomsList = getAllRooms()

        val bookingsList = bookingRepo.getAllBookings(offset, limit)

        bookingsList.list?.map { booking ->
            val room = roomsList?.find { room -> room.id == booking.roomId }
            booking.roomName = room?.roomName
            booking.isFullRoom = room?.isFullRoom

        }

        return bookingsList
    }

    fun getAllBookingsByRoom(offset: Int, limit: Int, roomId: Int): ScheduleOutputModelList {
        //TODO RoomById
        val roomsList = getAllRooms()
        val bookingsList = bookingRepo.getAllBookingsByRoom(offset, limit, roomId)

        bookingsList.list?.map { booking ->
            val room = roomsList?.find { room -> room.id == booking.roomId }
            booking.roomName = room?.roomName
            booking.isFullRoom = room?.isFullRoom
        }

        return bookingsList
    }

    fun getAvailableDesks(day: LocalDate, startTime: LocalTime, finishTime: LocalTime): AllRoomsOutputModel? {

        val roomsList = getAllNormalRooms()
        val availableSeats = bookingRepo.getAvailableDesks(day, startTime, finishTime)

        roomsList?.forEach { room ->
            run {
                val currentCapacity =
                    (availableSeats?.find { currentRoom -> currentRoom.roomId == room.id }?.currentCapacity ?: 0)
                room.roomCapacity = room.roomCapacity - currentCapacity
            }
        }

        return AllRoomsOutputModel(roomsList)

    }

    fun getAllRooms(): MutableList<RoomOutputModel>? {
        try {
            val call = service.getAllRooms()
            val response = call.execute()

            if (!response.isSuccessful) {
                return null
            }

            return response.body().properties?.rooms

        } catch (e: ConnectException) {
            throw InternalServiceException("Couldn't fetch bookings")
        }
    }

    fun getAllRoomsPost(): MutableList<RoomOutputModel>? {
        try {
            val call = service.getNonFullBookableRooms()
            val response = call.execute()

            if (!response.isSuccessful) {
                return null
            }

            return response.body().properties?.rooms
        } catch (e: ConnectException) {
            throw InternalServiceException("Something went wrong. Couldn't create booking.")
        }
    }

    fun getFullBookableRooms(): MutableList<RoomOutputModel>? {
        try {
            val call = service.getFullBookableRooms()
            val response = call.execute()

            if (!response.isSuccessful) {
                return null
            }

            return response.body().properties?.rooms

        } catch (e: ConnectException) {
            throw InternalServiceException("Couldn't fetch bookings")
        }
    }

    fun getAllNormalRooms(): MutableList<RoomOutputModel>? {
        try {
            val call = service.getNonFullBookableRooms()
            val response = call.execute()

            if (!response.isSuccessful) {
                return null
            }

            return response.body().properties?.rooms?.filter { roomOutputModel -> !roomOutputModel.isFullRoom }
                ?.toMutableList()
        } catch (e: ConnectException) {
            throw InternalServiceException("Something went wrong.")
        }
    }


    fun getAllCurrentDay(): ScheduleOutputModelList? {
        return bookingRepo.getAllCurrDay()
    }

    /**
     * Get all schedules for past week
     */
   fun getAllPastWeek(): ScheduleOutputModelList? {
       val lastWeek: LocalDate = Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().minusWeeks(1)
       val monday = lastWeek.with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY))
       val sunday = lastWeek.with(TemporalAdjusters.nextOrSame(DayOfWeek.SUNDAY))

       return bookingRepo.getAllBetweenDates(monday, sunday)
   }

    /**
     * Get all schedules for past month
     */
    fun getAllPastMonth(): ScheduleOutputModelList? {
        val lastMonth: LocalDate = Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().minusMonths(1)
        val firstDay = lastMonth.with(TemporalAdjusters.firstDayOfMonth())
        val lastDay = lastMonth.with(TemporalAdjusters.lastDayOfMonth())

        return bookingRepo.getAllBetweenDates(firstDay, lastDay)
    }
}