package com.example.common

const val ROOMS = "/rooms"
const val ROOM_ID = "/rooms/{id}"
const val ROOMS_BOOKABLE = "/rooms/full-bookable"
const val ROOMS_NON_FULL_BOOKABLE = "/rooms/non-full-bookable"

const val BASE_URL = "https://frontend-flexwork-4hupxege7q-uc.a.run.app"