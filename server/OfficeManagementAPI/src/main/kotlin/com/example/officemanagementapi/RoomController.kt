package com.example.officemanagementapi

import DataBaseException
import InvalidArgumentException
import NoDataBaseSelectedException
import ProblemJson
import RoomNameAlreadyExistsException
import com.example.common.*
import com.example.officemanagementapi.dto.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpMethod
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.http.converter.HttpMessageNotReadableException
import org.springframework.web.bind.annotation.*
import java.net.URI
import javax.servlet.http.HttpServletRequest

@RestController
@CrossOrigin(origins = arrayOf("*"))
class RoomController @Autowired constructor(private val roomServices: RoomServices){

    @CrossOrigin(origins = arrayOf("*"))
    @GetMapping(ROOMS)
    @ResponseBody
    fun getAllRooms(): SirenEntity<AllRoomsOutputModel>? {
        return roomServices.getAllRooms()?.toSirenObject(
            links = listOf(selfLink("/rooms/")),
            actions = listOf(
                SirenAction("post-room", URI(ROOMS), "Post rooms", method = HttpMethod.POST, type = MediaType.APPLICATION_JSON),
                SirenAction("get-room-by-id", URI(ROOMS), "Get room by id", method = HttpMethod.GET, type = MediaType.APPLICATION_JSON),
                SirenAction("delete-room-by-id", URI(ROOMS), "Delete room by id", method = HttpMethod.GET, type = MediaType.APPLICATION_JSON),
                SirenAction("get-full-bookable-rooms", URI(ROOMS_BOOKABLE), "Get full bookable rooms", method = HttpMethod.GET, type = MediaType.APPLICATION_JSON),
                SirenAction("get-non-full-bookable-rooms", URI(ROOMS_NON_FULL_BOOKABLE), "Get non full bookable rooms",  method = HttpMethod.PATCH, type = MediaType.APPLICATION_JSON),
            )
        )
    }

    @CrossOrigin(origins = arrayOf("*"))
    @GetMapping(ROOMS_BOOKABLE)
    @ResponseBody
    fun getFullBookableRooms(): SirenEntity<AllRoomsOutputModel>? {
        return roomServices.getFullBookableRooms()?.toSirenObject(
            links = listOf(selfLink("/rooms/")),
            actions = listOf(
                SirenAction("get-all-rooms", URI(ROOMS), "Get full bookable rooms", method = HttpMethod.GET, type = MediaType.APPLICATION_JSON),
                SirenAction("post-room", URI(ROOMS), "Post rooms", method = HttpMethod.POST, type = MediaType.APPLICATION_JSON),
                SirenAction("get-room-by-id", URI(ROOMS), "Get room by id", method = HttpMethod.GET, type = MediaType.APPLICATION_JSON),
                SirenAction("delete-room-by-id", URI(ROOMS), "Delete room by id", method = HttpMethod.GET, type = MediaType.APPLICATION_JSON),
                SirenAction("get-non-full-bookable-rooms", URI(ROOMS_NON_FULL_BOOKABLE), "Get non full bookable rooms",  method = HttpMethod.PATCH, type = MediaType.APPLICATION_JSON),
            )
        )
    }

    @CrossOrigin(origins = arrayOf("*"))
    @ResponseBody
    @GetMapping(ROOMS_NON_FULL_BOOKABLE)
    fun getNonFullBookableRooms(): SirenEntity<AllRoomsOutputModel>? {
        return roomServices.getNonFullBookableRooms()?.toSirenObject(
            links = listOf(selfLink("/rooms/")),
            actions = listOf(
                SirenAction("get-all-rooms", URI(ROOMS), "Get full bookable rooms", method = HttpMethod.GET, type = MediaType.APPLICATION_JSON),
                SirenAction("post-room", URI(ROOMS), "Post rooms", method = HttpMethod.POST, type = MediaType.APPLICATION_JSON),
                SirenAction("get-room-by-id", URI(ROOMS), "Get room by id", method = HttpMethod.GET, type = MediaType.APPLICATION_JSON),
                SirenAction("delete-room-by-id", URI(ROOMS), "Delete room by id", method = HttpMethod.GET, type = MediaType.APPLICATION_JSON),
                SirenAction("get-full-bookable-rooms", URI(ROOMS_BOOKABLE), "Get full bookable rooms", method = HttpMethod.GET, type = MediaType.APPLICATION_JSON),
            )
        )
    }

    @CrossOrigin(origins = arrayOf("*"))
    @ResponseBody
    @PostMapping(ROOMS)
    fun createRoom(req: HttpServletRequest, @RequestBody newRoom: RoomInputModel): SirenEntity<RoomOutputModel>? {

        val room: Room = newRoom.toRoom()
        val outputModel: RoomOutputModel = roomServices.createRoom(room) ?: throw Exception()

        return outputModel.toSirenObject(
            links = listOf(selfLink("/room/")),
            actions = listOf(
                SirenAction("get-all-rooms", URI(ROOMS), "Get full bookable rooms", method = HttpMethod.GET, type = MediaType.APPLICATION_JSON),
                SirenAction("get-room-by-id", URI(ROOMS), "Get room by id", method = HttpMethod.GET, type = MediaType.APPLICATION_JSON),
                SirenAction("delete-room-by-id", URI(ROOMS), "Delete room by id", method = HttpMethod.GET, type = MediaType.APPLICATION_JSON),
                SirenAction("get-full-bookable-rooms", URI(ROOMS_BOOKABLE), "Get full bookable rooms", method = HttpMethod.GET, type = MediaType.APPLICATION_JSON),
                SirenAction("get-non-full-bookable-rooms", URI(ROOMS_NON_FULL_BOOKABLE), "Get non full bookable rooms",  method = HttpMethod.PATCH, type = MediaType.APPLICATION_JSON),
            )
        )
    }

    @CrossOrigin(origins = arrayOf("*"))
    @ResponseBody
    @GetMapping(ROOM_ID)
    fun getRoomById(@PathVariable("id") roomId: Int) : SirenEntity<RoomOutputModel>? {
        return roomServices.getRoomById(roomId)?.toSirenObject(
            links = listOf(selfLink("/room/")),
            actions = listOf(
                SirenAction("get-all-rooms", URI(ROOMS), "Get full bookable rooms", method = HttpMethod.GET, type = MediaType.APPLICATION_JSON),
                SirenAction("post-room", URI(ROOMS), "Post rooms", method = HttpMethod.POST, type = MediaType.APPLICATION_JSON),
                SirenAction("get-room-by-id", URI(ROOMS), "Get room by id", method = HttpMethod.GET, type = MediaType.APPLICATION_JSON),
                SirenAction("delete-room-by-id", URI(ROOMS), "Delete room by id", method = HttpMethod.GET, type = MediaType.APPLICATION_JSON),
                SirenAction("get-full-bookable-rooms", URI(ROOMS_BOOKABLE), "Get full bookable rooms", method = HttpMethod.GET, type = MediaType.APPLICATION_JSON),
                SirenAction("get-non-full-bookable-rooms", URI(ROOMS_NON_FULL_BOOKABLE), "Get non full bookable rooms",  method = HttpMethod.PATCH, type = MediaType.APPLICATION_JSON),
            )
        )
    }

    @CrossOrigin(origins = arrayOf("*"))
    @ResponseBody
    @DeleteMapping(ROOM_ID)
    fun deleteRoom(@PathVariable("id") roomId: Int): Int? {
        return roomServices.deleteRoom(roomId)
    }

    @ExceptionHandler(value = [RoomNameAlreadyExistsException::class , InvalidArgumentException::class, HttpMessageNotReadableException::class])
    fun handle400Exception(e : Exception) = ResponseEntity
        .badRequest()
        .contentType(MediaType.APPLICATION_PROBLEM_JSON)
        .body(
            when(e::class){
                RoomNameAlreadyExistsException::class -> ProblemJson(
                    type = "/rooms",
                    title = "Bad Request",
                    detail = "${e.message}",
                    status = 400
                )
                InvalidArgumentException::class -> ProblemJson(
                    type = "/rooms",
                    title = "Bad Request",
                    detail = "${e.message}",
                    status = 400
                )
                HttpMessageNotReadableException::class -> ProblemJson(
                    type = "/rooms",
                    title = "Bad Request",
                    detail = "Invalid value inserted",
                    status = 400
                )
                else -> null
            }

        )

    @ExceptionHandler(value = [DataBaseException::class , NoDataBaseSelectedException::class])
    fun handle500Exception(e : Exception) = ResponseEntity
        .badRequest()
        .contentType(MediaType.APPLICATION_PROBLEM_JSON)
        .body(
            when(e::class){
                DataBaseException::class -> ProblemJson(
                    type = "/rooms",
                    title = "Database Error",
                    detail = "Something went wrong when fetching data from database",
                    status = 500
                )
                NoDataBaseSelectedException::class -> ProblemJson(
                    type = "/rooms",
                    title = "Database Error",
                    detail = "Could not establish connection to the database",
                    status = 500
                )
                else -> null
            }

        )

    @ExceptionHandler(value = [Exception::class])
    fun handleException(e : Exception) = ResponseEntity
        .badRequest()
        .contentType(MediaType.APPLICATION_PROBLEM_JSON)
        .body(
            when(e::class){
                Exception::class -> ProblemJson(
                    type = "/rooms",
                    title = "Internal Server Error",
                    detail = "Something went wrong",
                    status = 500
                )
                else -> null
            }

        )


}