package com.example.officemanagementapi

import DataBaseException
import NoDataBaseSelectedException
import RoomNameAlreadyExistsException
import com.example.officemanagementapi.dto.AllRoomsOutputModel
import com.example.officemanagementapi.dto.RoomOutputModel
import com.example.sqlcommands.*
import org.jdbi.v3.core.Handle
import org.jdbi.v3.core.Jdbi
import org.jdbi.v3.core.statement.UnableToExecuteStatementException
import org.springframework.stereotype.Repository

@Repository
class RoomRepository(var jdbi: Jdbi?) {
    fun getAll() : AllRoomsOutputModel? {
        val rooms: MutableList<RoomOutputModel>? =
            jdbi?.withHandle<MutableList<RoomOutputModel>?, RuntimeException> { handle: Handle ->
                handle.createQuery(GET_ALL_ROOMS)
                    .mapTo(RoomOutputModel::class.java)
                    .list()
            }

        return AllRoomsOutputModel(rooms)
    }

    fun getAllFullBookableRooms() : AllRoomsOutputModel? {
        val rooms: MutableList<RoomOutputModel>? =
            jdbi?.withHandle<MutableList<RoomOutputModel>?, RuntimeException> { handle: Handle ->
                handle.createQuery(GET_FULL_BOOKABLE_ROOMS)
                    .mapTo(RoomOutputModel::class.java)
                    .list()
            }
        return AllRoomsOutputModel(rooms)
    }

    fun getNonFullBookableRooms() : AllRoomsOutputModel? {
        val rooms: MutableList<RoomOutputModel>? =
            jdbi?.withHandle<MutableList<RoomOutputModel>?, RuntimeException> { handle: Handle ->
                handle.createQuery(GET_NON_FULL_BOOKABLE_ROOMS)
                    .mapTo(RoomOutputModel::class.java)
                    .list()
            }
        return AllRoomsOutputModel(rooms)
    }


    fun add(room: Room): RoomOutputModel? {

        var id: Any? = -1
        try {
            jdbi!!.useHandle<RuntimeException> { handle: Handle ->
                id = handle.createUpdate(CREATE_ROOM)
                    .bind(0, room.roomName)
                    .bind(1, room.roomCapacity)
                    .bind(2, room.isFullRoom)
                    .executeAndReturnGeneratedKeys("id")
                    .mapTo(Integer.TYPE)
                    .one()
            }
        }catch (e: UnableToExecuteStatementException){
            if(e.cause?.message.toString().equals("No database selected")){
                throw NoDataBaseSelectedException("No database selected")
            }
            if(e.cause?.message.toString().contains("Duplicate"))
                throw RoomNameAlreadyExistsException("Room Name Already Exists")
            throw DataBaseException("Database error")
        }catch(e: Exception){
            throw DataBaseException("Database error")
        }

        return RoomOutputModel(id = id.toString().toInt(), room = room)

    }

    fun getById(roomId: Int): RoomOutputModel? {
        val room: RoomOutputModel? =
            jdbi?.withHandle<RoomOutputModel?, RuntimeException> { handle: Handle ->
                handle.createQuery(GET_ROOM)
                    .bind(0, roomId)
                    .mapTo(RoomOutputModel::class.java)
                    .one()
            }

        return room
    }

    fun delete(roomId: Int): Int? {
        jdbi?.useHandle<RuntimeException> { handle: Handle ->
            handle.execute(DELETE_ROOM, roomId)
        }
        return roomId
    }
}