package com.example.officemanagementapi.dto

import InvalidArgumentException
import com.example.common.SirenAction
import com.example.common.SirenEntity
import com.example.common.SirenLink
import com.example.officemanagementapi.Room
import com.fasterxml.jackson.annotation.JsonCreator

class RoomInputModel @JsonCreator constructor(
    private val roomName: String,
    private val roomCapacity: Int?,
    private val  isFullRoom: Boolean
)  {

    fun toRoom(): Room {
        if(roomName.isEmpty()){
            throw InvalidArgumentException("Room Name can't be empty")
        }

        if(roomCapacity == null){
            throw InvalidArgumentException("Room capacity can't be empty")
        }

        if(roomCapacity < 0){
            throw InvalidArgumentException("Room capacity can't be a negative number")
        }

        return Room(
            null,
            roomName,
            roomCapacity,
            isFullRoom
        )
    }
}

class RoomPatchInputModel @JsonCreator constructor(
    private var id: Int,
    private val roomName: String,
    private val roomCapacity: Int?,
    private val isFullRoom: Boolean
) {
    fun toRoom(): Room {

        return Room( id, roomName, roomCapacity, isFullRoom)
    }

    fun setId(id: Int) {
        this.id = id
    }
}

class RoomOutputModel(
    val id: Int,
    val roomName: String,
    val roomCapacity: Int,
    val isFullRoom: Boolean
){
    constructor( id: Int, room: Room ) :
            this(
                id,
                room.roomName!!,
                room.roomCapacity!!,
                room.isFullRoom!!
                )
}

/**
 * Extension function that produces the Siren version of the resource's external representation
 *
 * @param [links]  the list of links to be included.
 * @param [actions]  the list of actions to be included.
 * @return The corresponding siren representation
 */
fun RoomOutputModel.toSirenObject(links: List<SirenLink>, actions: List<SirenAction>? = null) = SirenEntity(
    properties = this,
    clazz = listOf("Room"),
    links = links,
    actions = actions
)


class AllRoomsOutputModel(val rooms: MutableList<RoomOutputModel>?)

/**
 * Extension function that produces the Siren version of the resource's external representation
 *
 * @param [links]  the list of links to be included.
 * @param [actions]  the list of actions to be included.
 * @return The corresponding siren representation
 */
fun AllRoomsOutputModel.toSirenObject(links: List<SirenLink>, actions: List<SirenAction>? = null) = SirenEntity(
    properties = this,
    clazz = listOf("Room"),
    links = links,
    actions = actions
)