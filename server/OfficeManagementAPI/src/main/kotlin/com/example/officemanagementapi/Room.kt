package com.example.officemanagementapi

class Room(
    val id: Int?,
    val roomName: String?,
    val roomCapacity: Int?,
    val isFullRoom: Boolean?
)