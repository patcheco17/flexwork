package com.example.officemanagementapi

import com.example.officemanagementapi.dto.AllRoomsOutputModel
import com.example.officemanagementapi.dto.RoomOutputModel
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class RoomServices @Autowired constructor(roomsRepository: RoomRepository){
    val roomsRepo = roomsRepository

    fun getAllRooms(): AllRoomsOutputModel? {
        return roomsRepo.getAll()
    }

    fun getFullBookableRooms(): AllRoomsOutputModel? {
        return roomsRepo.getAllFullBookableRooms()
    }

    fun getNonFullBookableRooms(): AllRoomsOutputModel? {
        return roomsRepo.getNonFullBookableRooms()
    }




    fun createRoom(room: Room): RoomOutputModel? {
        return roomsRepo.add(room)
    }

    fun getRoomById(roomId: Int): RoomOutputModel? {
        return roomsRepo.getById(roomId)
    }

    fun deleteRoom(roomId: Int): Int? {
        return roomsRepo.delete(roomId)
    }
}