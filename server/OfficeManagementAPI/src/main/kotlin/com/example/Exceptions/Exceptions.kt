

class InvalidArgumentException(message: String) : Exception(message)

class RoomNameAlreadyExistsException(message: String) : Exception(message)

class DataBaseException(message: String) : Exception(message)

class NoDataBaseSelectedException(message: String) : Exception(message)