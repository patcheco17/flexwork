
CREATE DATABASE office_management_db;

USE office_management_db;

DROP TABLE rooms_management;

CREATE TABLE rooms_management (
    id int not null AUTO_INCREMENT,
    roomName VARCHAR(100) NOT NULL UNIQUE,
    roomCapacity INT NULL,
    isFullBookable bit,
    constraint roomName unique (roomName),
    PRIMARY KEY (id)
);

select * from rooms_management
