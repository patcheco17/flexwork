package com.example.sqlcommands

const val CREATE_ROOM =
    "INSERT INTO rooms_management (roomName, roomCapacity, isFullRoom)" +
    "VALUES (?, ?, ?);"

const val GET_ALL_ROOMS = "SELECT * FROM rooms_management"

const val GET_FULL_BOOKABLE_ROOMS = "SELECT * FROM rooms_management where isFullRoom=true"

const val GET_NON_FULL_BOOKABLE_ROOMS = "SELECT * FROM rooms_management where isFullRoom=false"

const val GET_ROOM = "SELECT * FROM rooms_management WHERE id = ?"

const val DELETE_ROOM = "DELETE FROM rooms_management WHERE id = ?"

const val UPDATE_ROOM = "UPDATE rooms_management " +
        "SET roomName = ?, capacity = ?" +
        "WHERE id = ?"