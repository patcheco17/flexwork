import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("org.springframework.boot") version "2.7.0"
    id("io.spring.dependency-management") version "1.0.11.RELEASE"
    kotlin("jvm") version "1.6.21"
    kotlin("plugin.spring") version "1.6.21"
}

group = "com.example"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_11

repositories {
    mavenCentral()
}

dependencies {
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation("mysql:mysql-connector-java:8.0.29")
    testImplementation("org.springframework.boot:spring-boot-starter-test")
    implementation("org.jdbi:jdbi3-kotlin:3.18.1")
    implementation("org.jdbi:jdbi3-spring4:3.19.0")
    implementation("org.jdbi:jdbi3-core:3.28.0")
    implementation("org.jdbi:jdbi3-sqlobject:3.28.0")
    implementation("org.springframework.boot:spring-boot-starter-data-jdbc:2.6.7")
    implementation("org.jdbi:jdbi3-kotlin-sqlobject:3.28.0")
    testImplementation("org.springframework.boot:spring-boot-starter-jdbc:2.6.7")
    implementation(kotlin("script-runtime"))

}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "11"
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
}
