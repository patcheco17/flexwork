package com.example.exceptions

class InvalidArgumentException(message: String) : Exception(message)

class DataBaseException(message: String) : Exception(message)

class NoDataBaseSelectedException(message: String) : Exception(message)

class UserAlreadyExistsException(message: String) : Exception(message)

class UserNotFoundException(message: String) : Exception(message)

class UnauthorizedException(message: String) : Exception(message)

class UserNotLoginException(message: String) : Exception(message)