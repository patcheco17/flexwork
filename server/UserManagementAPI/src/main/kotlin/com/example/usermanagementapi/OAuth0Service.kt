package com.example.usermanagementapi

import com.example.common.AUTH0_USER
import com.example.common.AUTH0_USERS
import com.example.common.AUTH0_USER_ROLES
import org.springframework.stereotype.Component
import org.springframework.stereotype.Service
import retrofit2.Call
import retrofit2.http.*
import java.io.Serializable

interface OAuth0Service {
    @POST(AUTH0_USERS)
    fun createUser(
        @Header("Authorization") accessToken: String,
        @Header("Content-Type") contentType: String,
        @Body user: HashMap<String, String>
    ) : Call<Auth0User>

    @POST(AUTH0_USER_ROLES)
    fun assignRoleToUser(
        @Header("Authorization") accessToken: String,
        @Header("Content-Type") contentType: String,
        @Path("id") userId: String,
        @Body role: HashMap<String, Array<String>>
    ) : Call<Auth0User>

    @DELETE(AUTH0_USER)
    fun deleteUser(
        @Header("Authorization") accessToken: String,
        @Header("Content-Type") contentType: String,
        @Path("id") userId: String
    ) : Call<User>
}