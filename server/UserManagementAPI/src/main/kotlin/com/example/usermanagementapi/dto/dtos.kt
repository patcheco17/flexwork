package com.example.usermanagementapi.dto

import com.example.common.SirenAction
import com.example.common.SirenEntity
import com.example.common.SirenLink
import com.example.exceptions.InvalidArgumentException
import com.example.usermanagementapi.User
import com.fasterxml.jackson.annotation.JsonCreator
import org.springframework.web.client.HttpClientErrorException
import java.beans.ConstructorProperties
import java.util.*

class UserInputModel @JsonCreator constructor(
    private val firstName: String,
    private val lastName: String,
    private val email: String,
    private val password: String,
    private val address: String,
    private val description: String?,
    private val projectName: String?,
    private val position: String,
    private val role: String
)  {

    fun toUser(): User {
        if (firstName.isEmpty() || firstName.isBlank() ||
            lastName.isEmpty() || lastName.isBlank() ||
            email.isEmpty() || email.isBlank() ||
            password.isEmpty() || password.isBlank() ||
            role.isEmpty() || role.isBlank() ||
            position.isEmpty() || position.isBlank() ||
            address.isEmpty() || address.isBlank() ||
            role.isEmpty() || role.isBlank())
                throw InvalidArgumentException("All arguments must have valid input")

        if (role != "admin" && role != "user")
            throw InvalidArgumentException("Role must be admin or user")

        return User(
            null,
            firstName,
            lastName,
            email,
            password,
            address,
            description,
            projectName,
            position,
            role.lowercase(Locale.getDefault())
        )
    }
}

class UserPatchInputModel @JsonCreator constructor(
    private var id: String?,
    private val address: String,
    private val description: String,
    private val projectName: String,
    private val position: String,
) {
    fun toUser(): User {
        return User( id,
            null,
            null,
            null,
            null,
            address,
            description,
            projectName,
            position,
            null)
    }

    fun setId(id: String) {
        this.id = id
    }
}

class UserOutputModel(
    val userId: String,
    val firstName: String,
    val lastName: String,
    val email: String,
    val address: String,
    val description: String?,
    val projectName: String?,
    val position: String
){
    constructor( id: String, user: User ) :
            this(
                id,
                user.firstName!!,
                user.lastName!!,
                user.email!!,
                user.address!!,
                user.description,
                user.projectName,
                user.position!!)
}

/**
 * Extension function that produces the Siren version of the resource's external representation
 *
 * @param [links]  the list of links to be included.
 * @param [actions]  the list of actions to be included.
 * @return The corresponding siren representation
 */
fun UserOutputModel.toSirenObject(links: List<SirenLink>, actions: List<SirenAction>? = null) = SirenEntity(
    properties = this,
    clazz = listOf("User"),
    links = links,
    actions = actions
)


class AllUsersOutputModel(val users: MutableList<UserOutputModel>?)

/**
 * Extension function that produces the Siren version of the resource's external representation
 *
 * @param [links]  the list of links to be included.
 * @param [actions]  the list of actions to be included.
 * @return The corresponding siren representation
 */
fun AllUsersOutputModel.toSirenObject(links: List<SirenLink>, actions: List<SirenAction>? = null) = SirenEntity(
    properties = this,
    clazz = listOf("User"),
    links = links,
    actions = actions
)