package com.example.usermanagementapi

import com.example.common.AUTH0
import com.example.exceptions.*
import com.example.usermanagementapi.dto.AllUsersOutputModel
import com.example.usermanagementapi.dto.UserOutputModel
import com.google.gson.GsonBuilder
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.lang.Exception

@Service
class UserServices {

    private lateinit var service: OAuth0Service
    private lateinit var userRepository: UserRepository

    @Autowired
    fun UserServices(userRepository: UserRepository){
        val gson = GsonBuilder()
            .setLenient()
            .create()

        val retrofit = Retrofit.Builder()
            .baseUrl(AUTH0)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()

        service = retrofit.create(OAuth0Service::class.java)
        this.userRepository = userRepository
    }

    fun getAllUsers(): AllUsersOutputModel? {
        return userRepository.getAll()
    }

    fun getUserById(userId: String): UserOutputModel? {
        return userRepository.getById(userId)
    }

    fun editUser(editedUser: User): UserOutputModel? {
        return userRepository.update(editedUser)
    }

    fun getSeveralUsersById(usersIds: Array<String>): AllUsersOutputModel?{
        val allUsers = userRepository.getAll()
        //Filter all users to get only the ones with id's contained in the array received
        val usersFiltered = allUsers?.users?.filter { user -> usersIds.contains(user.userId) } as MutableList<UserOutputModel>
        return AllUsersOutputModel(usersFiltered)
    }

    fun createUser(user: User, token: String): UserOutputModel? {
        // Create user in auth0 API
        val userAuth = hashMapOf(
            "connection" to "Username-Password-Authentication",
            "email" to user.email!!,
            "given_name" to user.firstName!!,
            "family_name" to user.lastName!!,
            "name" to "${user.firstName} ${user.lastName}",
            "password" to user.password!!,
        )

        val createUserRetrofitCall: Call<Auth0User> = service.createUser(token, "application/json", userAuth)
        val createUserResponse: Response<Auth0User> = createUserRetrofitCall.execute()

        if (!createUserResponse.isSuccessful) {
            when {
                createUserResponse.code() == 400 -> throw InvalidArgumentException("Invalid arguments")
                createUserResponse.code() == 401 -> throw UserNotLoginException("User isn't logged in")
                createUserResponse.code() == 403 -> throw UnauthorizedException("User doesn't have permissions")
                createUserResponse.code() == 409 -> throw UserAlreadyExistsException("Email already in use")
                else -> throw Exception()
            }
        }

        val userId = createUserResponse.body()!!.user_id

        // Add role do user in auth0 API
        val roleId: String =
            if(user.role == "admin") "rol_1JJuFl9nbdimmcKz"
            else "rol_35KX0rGgQPYoKRJ4"

        val role = hashMapOf( "roles" to arrayOf(roleId) )
        val assignRoleRetrofitCall: Call<Auth0User> = service.assignRoleToUser(token, "application/json", userId, role)
        val assignRoleResponse: Response<Auth0User> = assignRoleRetrofitCall.execute()

        if (!assignRoleResponse.isSuccessful) {
            when {
                createUserResponse.code() == 400 -> throw InvalidArgumentException("Invalid arguments")
                createUserResponse.code() == 401 -> throw UserNotLoginException("User isn't logged in")
                createUserResponse.code() == 403 -> throw UnauthorizedException("User doesn't have permissions")
                else -> throw Exception()
            }

        }

        //Add user to flexwork DB
        user.id = userId.split('|')[1]    //To remove the "auth0|" section of the id
        return userRepository.add(user)
    }

    fun deleteUser(userId: String, token: String): String? {
        //Delete user in Auth0 API
        val auth0UserId = "auth0|$userId"
        val retrofitCall: Call<User> = service.deleteUser(token, "application/json", auth0UserId)
        val response: Response<User> = retrofitCall.execute()

        if (!response.isSuccessful) {
            when {
                response.code() == 400 -> throw InvalidArgumentException("Invalid request")
                response.code() == 401 -> throw UserNotLoginException("User isn't logged in")
                response.code() == 403 -> throw UnauthorizedException("User doesn't have permissions")
                else -> throw Exception()
            }
        }

        //Delete user in local DB
        return userRepository.delete(userId)
    }
}