package com.example.usermanagementapi

//TODO: check if it makes sense to say that all the fields could be null. Initially we said that just the description
//and the projectName could be null but, when we update user's info, we only get from outside the API, the updatable
//fields: address, description, projectName and position so, if we can say that all of them could be null, then it's
//easier to implement the dtos.
//This way, we need to give null to all the fields that don't have value
//And then we need to change the user output model to?

class User(
    var id: String?,
    val firstName: String?,
    val lastName: String?,
    val email: String?,
    val password: String?,
    val address: String?,
    val description: String?,
    val projectName: String?,
    val position: String?,
    val role: String?,
)

class Auth0User(
    val email: String,
    val given_name: String,
    val family_name: String,
    val user_id: String,
)