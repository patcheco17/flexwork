package com.example.usermanagementapi

import ProblemJson
import com.example.common.*
import com.example.exceptions.*
import com.example.usermanagementapi.dto.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.http.converter.HttpMessageNotReadableException
import org.springframework.web.bind.annotation.*
import javax.servlet.http.HttpServletRequest

@RestController
@CrossOrigin(origins = arrayOf("*"))
class UserController @Autowired constructor(private val userServices: UserServices){
    @CrossOrigin(origins = arrayOf("*"))
    @GetMapping(USERS)
    @ResponseBody
    fun getAllUsers(): SirenEntity<AllUsersOutputModel>? {
        return userServices.getAllUsers()?.toSirenObject(
            links = listOf(selfLink("/users/")),
            actions = null
        )
    }

    @CrossOrigin(origins = arrayOf("*"))
    @PostMapping(USERS)
    @ResponseBody
    fun createUser(
        req: HttpServletRequest,
        @RequestBody newUser: UserInputModel,
        @RequestHeader("authorization") token: String
        ): SirenEntity<UserOutputModel>?
    {
        //TODO: change this authorization header token, I think this will be made in an interceptor
        // with Spring Security or something like that

        val user: User = newUser.toUser()
        val outputModel: UserOutputModel = userServices.createUser(user, token) ?: throw Exception()
        return outputModel.toSirenObject(
            links = listOf(selfLink("/user/")),
            actions = null
        )
    }

    @CrossOrigin(origins = arrayOf("*"))
    @GetMapping(USER_ID)
    @ResponseBody
    fun getUserById(@PathVariable("id") userId: String) : SirenEntity<UserOutputModel>? {
        return userServices.getUserById(userId)?.toSirenObject(
            links = listOf(selfLink("/user/")),
            actions = null
        )
    }

    /**
     * Endpoint that receives and array of users' ids and returns the correspondent users
     */
    @CrossOrigin(origins = arrayOf("*"))
    @PostMapping(USERS_IDS)
    @ResponseBody
    fun getSeveralUsersById( @RequestBody usersIds: Array<String> ): SirenEntity<AllUsersOutputModel>? {
        if (usersIds.isNullOrEmpty()) throw InvalidArgumentException("Id's array is empty")

        //TODO: receive object with id's array and convert it to array (create InputModel and add .toArray() method to it)

        return userServices.getSeveralUsersById(usersIds)?.toSirenObject(
            links = listOf(selfLink("/user/")),
            actions = null
        )
    }

    /**
     * Deletes the user and returns his id
     */
    @CrossOrigin(origins = arrayOf("*"))
    @DeleteMapping(USER_ID)
    @ResponseBody
    fun deleteUser(
        @PathVariable("id") userId: String,
        @RequestHeader("authorization") token: String): String?
    {
        //TODO: change this authorization header token, I think this will be made in an interceptor
        // with Spring Security or something like that

        return userServices.deleteUser(userId, token)
    }

    @CrossOrigin(origins = arrayOf("*"))
    @PatchMapping(USER_ID)
    @ResponseBody
    fun editUser(
        @PathVariable("id") userId: String,
        @RequestBody editedUser: UserPatchInputModel): SirenEntity<UserOutputModel>?
    {
        editedUser.setId(userId)
        val user: User = editedUser.toUser()
        return userServices.editUser(user)?.toSirenObject(
            links = listOf(selfLink("/user/")),
            actions = null
        )
    }

    @ExceptionHandler(value = [
        UserAlreadyExistsException::class,
        InvalidArgumentException::class,
        HttpMessageNotReadableException::class])
    fun handle400Exception(e : Exception) = ResponseEntity
        .badRequest()
        .contentType(MediaType.APPLICATION_PROBLEM_JSON)
        .body(
            when(e::class){
                UserAlreadyExistsException::class -> ProblemJson(
                    type = "/users",
                    title = "Bad Request",
                    detail = "${e.message}",
                    status = 400
                )
                InvalidArgumentException::class -> ProblemJson(
                    type = "/users",
                    title = "Bad Request",
                    detail = "${e.message}",
                    status = 400
                )
                HttpMessageNotReadableException::class -> ProblemJson(
                    type = "/users",
                    title = "Bad Request",
                    detail = "Invalid value inserted",
                    status = 400
                )
                else -> null
            }

        )

    @ExceptionHandler(value = [UserNotLoginException::class])
    fun handleNotLoginException(e: Exception) = ResponseEntity.status(401).body(
        ProblemJson(
            type = "/users",
            title = "Unauthorized",
            detail = "User isn't logged in",
            status = 401
        )
    )

    @ExceptionHandler(value = [UnauthorizedException::class])
    fun handleUnauthorizedException(e: Exception) = ResponseEntity.status(403).body(
        ProblemJson(
            type = "/users",
            title = "Unauthorized",
            detail = "User doesn't have permissions",
            status = 403
        )
    )

    @ExceptionHandler(value = [UserNotFoundException::class])
    fun handleNotFoundException(e: Exception) = ResponseEntity.status(404).body(
        ProblemJson(
            type = "/users",
            title = "NOT FOUND",
            detail = "User not found",
            status = 404
        )
    )

    @ExceptionHandler(value = [
        DataBaseException::class ,
        NoDataBaseSelectedException::class])
    fun handle500Exception(e : Exception) = ResponseEntity
        .internalServerError()
        .contentType(MediaType.APPLICATION_PROBLEM_JSON)
        .body(
            when(e::class){
                DataBaseException::class -> ProblemJson(
                    type = "/users",
                    title = "Database Error",
                    detail = "Something went wrong when fetching data from database",
                    status = 500
                )
                NoDataBaseSelectedException::class -> ProblemJson(
                    type = "/users",
                    title = "Database Error",
                    detail = "Could not establish connection to the database",
                    status = 500
                )
                else -> null
            }
        )

    @ExceptionHandler(value = [Exception::class])
    fun handleException(e : Exception) = ResponseEntity
        .badRequest()
        .contentType(MediaType.APPLICATION_PROBLEM_JSON)
        .body(
            when(e::class){
                DataBaseException::class -> ProblemJson(
                    type = "/users",
                    title = "Internal Server Error",
                    detail = "Something went wrong",
                    status = 500
                )
                else -> null
            }
        )

}