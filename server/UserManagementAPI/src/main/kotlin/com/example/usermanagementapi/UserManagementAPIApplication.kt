package com.example.usermanagementapi

import org.jdbi.v3.core.Jdbi
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import javax.sql.DataSource
import org.jdbi.v3.sqlobject.SqlObjectPlugin
import org.jdbi.v3.core.kotlin.KotlinPlugin

@Configuration
class JdbiConfiguration {
    @Bean
    fun jdbi(datasource: DataSource): Jdbi {
        return Jdbi.create(datasource)
            .installPlugin(SqlObjectPlugin())
            .installPlugin(KotlinPlugin())
    }
}

@SpringBootApplication
class UserManagementAPIApplication

fun main(args: Array<String>) {
    runApplication<UserManagementAPIApplication>(*args)
}