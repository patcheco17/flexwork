package com.example.usermanagementapi

import com.example.exceptions.DataBaseException
import com.example.exceptions.UserAlreadyExistsException
import com.example.exceptions.NoDataBaseSelectedException
import com.example.exceptions.UserNotFoundException
import com.example.sqlcommands.*
import com.example.usermanagementapi.dto.AllUsersOutputModel
import com.example.usermanagementapi.dto.UserOutputModel
import org.jdbi.v3.core.Handle
import org.jdbi.v3.core.Jdbi
import org.jdbi.v3.core.statement.UnableToExecuteStatementException
import org.springframework.stereotype.Repository
import java.lang.IllegalStateException

@Repository
class UserRepository(var jdbi: Jdbi?) {
    /***
     * Get all users from DB
     * Returns an AllUsersOutputModel that is a list of UserOutputModel
     */
    fun getAll() : AllUsersOutputModel? {
        val users: MutableList<UserOutputModel>?
        try {
            users = jdbi?.withHandle<MutableList<UserOutputModel>?, RuntimeException> { handle: Handle ->
                handle.createQuery(GET_ALL_USERS)
                    .mapTo(UserOutputModel::class.java)
                    .list()
            }
        } catch (e: UnableToExecuteStatementException){
            if(e.cause?.message.toString() == "No database selected")
                throw NoDataBaseSelectedException("No database selected")
            else
                throw DataBaseException("Database error")
        } catch (e: Exception){
            throw DataBaseException("Database error")
        }

        return AllUsersOutputModel(users)
    }

    /***
     * Add user to DB and get the generated id to store in the user object so we can use it to navigate to other pages, for ex.
     */
    fun add(user: User): UserOutputModel? {
        try {
            jdbi!!.useHandle<RuntimeException>  { handle: Handle ->
                handle.createUpdate(CREATE_USER)
                    .bind(0, user.id)
                    .bind(1, user.firstName)
                    .bind(2, user.lastName)
                    .bind(3, user.email)
                    .bind(4, user.address)
                    .bind(5, user.description)
                    .bind(6, user.projectName)
                    .bind(7, user.position)
                    .one()
            }
        } catch (e: UnableToExecuteStatementException){
            when {
                e.cause?.message.toString() == "No database selected" -> throw NoDataBaseSelectedException("No database selected")
                e.cause?.message.toString().contains("Duplicate") -> throw UserAlreadyExistsException("Email already exists")
                else -> throw DataBaseException("Database error")
            }
        } catch (e: Exception){
            throw DataBaseException("Database error")
        }

        return UserOutputModel(id = user.id!!, user = user)
    }

    /***
     * Get user by id
     */
    fun getById(userId: String): UserOutputModel? {
        val user: UserOutputModel?
        try {
            user = jdbi?.withHandle<UserOutputModel?, RuntimeException> { handle: Handle ->
                handle.createQuery(GET_USER)
                    .bind(0, userId)
                    .mapTo(UserOutputModel::class.java)
                    .one()
            }
        } catch (e: UnableToExecuteStatementException){
            if(e.cause?.message.toString() == "No database selected")
                throw NoDataBaseSelectedException("No database selected")
            else
                throw DataBaseException("Database error")
        } catch (e: IllegalStateException) {
            throw UserNotFoundException("User with id = $userId not found")
        } catch (e: Exception){
            throw DataBaseException("Database error")
        }

        return user
    }

    /**
     *  Delete user by id
     */
    fun delete(userId: String): String? {
        val result: Int?
        try{
            result = jdbi?.withHandle<Int, RuntimeException> { handle: Handle ->
                handle.createUpdate(DELETE_USER)
                    .bind(0, userId)
                    .execute()
            }
        } catch (e: UnableToExecuteStatementException){
            if(e.cause?.message.toString() == "No database selected")
                throw NoDataBaseSelectedException("No database selected")
            else
                throw DataBaseException("Database error")
        } catch (e: Exception) {
            throw DataBaseException("Database error")
        }

        if(result == 0 || result == null) throw UserNotFoundException("User with id = $userId not found");

        return userId
    }

    /**
     * Partially update user info (address, description, projectName and position in the company)
     */
    fun update(editedUser: User): UserOutputModel? {
        val updatedUser: UserOutputModel?
        try{
            updatedUser = jdbi?.withHandle<UserOutputModel, RuntimeException> { handle: Handle ->
                handle.createUpdate(UPDATE_USER)
                    .bind(0, editedUser.address)
                    .bind(1, editedUser.description)
                    .bind(2, editedUser.projectName)
                    .bind(3, editedUser.position)
                    .bind(4, editedUser.id)
                    .execute()

                handle.createQuery(GET_USER)
                    .bind(0, editedUser.id)
                    .mapTo(UserOutputModel::class.java)
                    .one()
            }
        } catch (e: UnableToExecuteStatementException){
            when {
                e.cause?.message.toString() == "No database selected" -> throw NoDataBaseSelectedException("No database selected")
                e.cause?.message.toString().contains("found none") -> throw NoDataBaseSelectedException("No database selected")
                else -> throw DataBaseException("Database error")
            }
        } catch (e: IllegalStateException) {
            throw UserNotFoundException("User with id = ${editedUser.id} not found")
        } catch (e: Exception) {
            throw DataBaseException("Database error")
        }

        return updatedUser
    }
}