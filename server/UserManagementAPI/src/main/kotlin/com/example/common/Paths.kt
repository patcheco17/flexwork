package com.example.common

const val BASE_URL = "https://frontend-flexwork-4hupxege7q-uc.a.run.app"
const val USERS = "/users"
const val USERS_IDS = "/users/ids"
const val USER_ID = "/users/{id}"

// Auth0 API path
const val AUTH0 = "https://flexwork.eu.auth0.com"
const val AUTH0_USERS = "/api/v2/users"
const val AUTH0_USER = "/api/v2/users/{id}"
const val AUTH0_USER_ROLES = "/api/v2/users/{id}/roles"