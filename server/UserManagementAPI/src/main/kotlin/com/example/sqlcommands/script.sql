CREATE DATABASE user;

USE user;

DROP TABLE user;

CREATE TABLE user (
      userId VARCHAR(50) NOT NULL,
      firstName VARCHAR(50) NOT NULL,
      lastName VARCHAR(50) NOT NULL,
      email VARCHAR(50) NOT NULL UNIQUE,
      address VARCHAR(100),
      description VARCHAR(250),
      projectName VARCHAR(50),
      position VARCHAR(50),
      PRIMARY KEY (userId)
);