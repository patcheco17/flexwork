package com.example.sqlcommands

/**
 * User region
 */
const val CREATE_USER =
    "INSERT INTO user (userId, firstName, lastName, email, address, description, projectName, position)" +
    "VALUES (?, ?, ?, ?, ?, ?, ?, ?);"

const val GET_ALL_USERS = "SELECT * FROM user"

const val GET_USER = "SELECT * FROM user WHERE userId = ?"

const val DELETE_USER = "DELETE FROM user WHERE userId = ?"

const val UPDATE_USER =
    "UPDATE user " +
    "SET address = ?, description = ?, projectName = ?, position = ? " +
    "WHERE userId = ?"