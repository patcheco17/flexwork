package com.example.common

const val BASE_URL = "https://statistics-api-4hupxege7q-uc.a.run.app"
const val COLLABORATORS_COUNT = "/collaborators-count"
const val COLLABORATORS_IN_OFFICE = "/collaborators-in-office"
const val INFLUX_PAST_WEEK = "/occupancy/week"
const val INFLUX_PAST_MONTH = "/occupancy/month"
const val ROOMS_OCCUPANCY_PAST_MONTH = "/room-occupancy/month"

const val USERS_MANAGEMENT_API_URL= "https://user-management-api-4hupxege7q-uc.a.run.app/"

const val SCHEDULE_MANAGEMENT_API_URL= "https://schedule-api-4hupxege7q-uc.a.run.app/"

const val OFFICE_MANAGEMENT_API_URL= "https://office-management-api-4hupxege7q-uc.a.run.app/"