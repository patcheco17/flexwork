package com.example.statisticsapi

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class StatisticsAPIApplication

fun main(args: Array<String>) {
    runApplication<StatisticsAPIApplication>(*args)
}
