package com.example.statisticsapi

import com.example.common.OFFICE_MANAGEMENT_API_URL
import com.example.common.SCHEDULE_MANAGEMENT_API_URL
import com.example.common.SirenEntity
import com.example.common.USERS_MANAGEMENT_API_URL
import com.example.exceptions.InvalidArgumentException
import com.example.statisticsapi.dto.*
import com.google.gson.GsonBuilder
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.lang.Exception
import java.time.DayOfWeek
import java.time.LocalDate
import java.time.ZoneId
import java.time.temporal.TemporalAdjusters
import java.util.*
import kotlin.collections.LinkedHashMap

@Service
class StatisticsServices {
    private lateinit var userService: UserService
    private lateinit var scheduleService: ScheduleService
    private lateinit var officeService: OfficeService

    @Autowired
    fun Services(){
        val gson = GsonBuilder()
            .setLenient()
            .create()

        // User management service to access User Management service
        var retrofit = Retrofit.Builder()
            .baseUrl(USERS_MANAGEMENT_API_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
        userService = retrofit.create(UserService::class.java)

        // Schedule service to access Schedules service
        retrofit = Retrofit.Builder()
            .baseUrl(SCHEDULE_MANAGEMENT_API_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
        scheduleService = retrofit.create(ScheduleService::class.java)

        // Office service to access Office Management service
        retrofit = Retrofit.Builder()
            .baseUrl(OFFICE_MANAGEMENT_API_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
        officeService = retrofit.create(OfficeService::class.java)

    }

    /**
     * Access User Management API to get all company's collaborators and returns the number of workers
     */
    fun getCollaboratorsCount(): Int? {
        val getAllCollaboratorsRetrofitCall: Call<SirenEntity<UsersListInputModel>> = userService.collaboratorsList()
        val getAllCollaboratorsResponse: Response<SirenEntity<UsersListInputModel>> = getAllCollaboratorsRetrofitCall.execute()

        if (!getAllCollaboratorsResponse.isSuccessful) {
            when {
                //TODO: check which errors could be sent
                getAllCollaboratorsResponse.code() == 400 -> throw InvalidArgumentException("Invalid arguments")
                else -> throw Exception()
            }
        }

        val usersList = getAllCollaboratorsResponse.body()?.properties?.users

        return usersList?.size
    }

    /**
     * Return's all the collaborators that will be in office for the current day
     */
    fun getCollaboratorsInOfficeCurrentDay(token: String): UsersInOfficeListOutputModel? {
        // Get all schedules that users made to go to the office for current day
        val getScheduleListRetrofitCall: Call<SirenEntity<ScheduleListInputModel>> = scheduleService.scheduleList()
        val getScheduleListResponse: Response<SirenEntity<ScheduleListInputModel>> = getScheduleListRetrofitCall.execute()

        if (!getScheduleListResponse.isSuccessful) {
            when {
                getScheduleListResponse.code() == 400 -> throw InvalidArgumentException("Invalid arguments")
                else -> throw Exception()
            }
        }

        // Extract all the different id's (schedules with same id correspond to same worker, so we don't need those)
        val scheduleList = getScheduleListResponse.body()?.properties?.list
        if (scheduleList.isNullOrEmpty()) return UsersInOfficeListOutputModel(arrayListOf())

        val distinctScheduleList = scheduleList.distinctBy { it.userId }
        val usersIds = mutableListOf<String>()
        distinctScheduleList.map { schedule -> usersIds.add(schedule.userId) }
        val ids = usersIds.toTypedArray()

        // Access User Management API to get user's information of the ones that are / will go to the office in current day
        val getUsersByIdsListRetrofitCall: Call<SirenEntity<UsersListInputModel>> =
            userService.usersBySeveralIdsList(accessToken = token, usersIds = ids)
        val getUsersByIdsListResponse: Response<SirenEntity<UsersListInputModel>> =
            getUsersByIdsListRetrofitCall.execute()

        if (!getUsersByIdsListResponse.isSuccessful) {
            when {
                getUsersByIdsListResponse.code() == 400 -> throw InvalidArgumentException("Invalid arguments")
                else -> throw Exception()
            }
        }

        val users = getUsersByIdsListResponse.body()?.properties?.users
        val usersInOfficeOutputModel = users?.map { user -> user.toUserInOfficeOutputModel() } as MutableList
        return UsersInOfficeListOutputModel(usersInOfficeOutputModel)
    }

    /**
     * Returns an object with the average % of the collaborators in relation to the total number of collaborators
     * that went to the office in the past week and, an array with each day and each % individually
     */
    fun getInfluxForPastWeek(): InfluxForPastTimeSpanOutputModel?{
        // Get past week schedules booked
        val getScheduleListRetrofitCall: Call<SirenEntity<ScheduleListInputModel>> = scheduleService.scheduleListPastWeek()
        val getScheduleListResponse: Response<SirenEntity<ScheduleListInputModel>> = getScheduleListRetrofitCall.execute()

        if (!getScheduleListResponse.isSuccessful) {
            when {
                //TODO: check which errors could be sent
                getScheduleListResponse.code() == 400 -> throw InvalidArgumentException("Invalid arguments")
                else -> throw Exception()
            }
        }

        // Get total number of workers for the company
        //TODO: put this in an aux method, because it is used in the getAllUsers() method above
        val getAllCollaboratorsRetrofitCall: Call<SirenEntity<UsersListInputModel>> = userService.collaboratorsList()
        val getAllCollaboratorsResponse: Response<SirenEntity<UsersListInputModel>> = getAllCollaboratorsRetrofitCall.execute()

        if (!getAllCollaboratorsResponse.isSuccessful) {
            when {
                //TODO: check which errors could be sent
                getAllCollaboratorsResponse.code() == 400 -> throw InvalidArgumentException("Invalid arguments")
                else -> throw Exception()
            }
        }

        val totalCollaboratorsCount = getAllCollaboratorsResponse.body()?.properties?.users?.size

        // Get all the different workers that made bookings on each day
        val scheduleList = getScheduleListResponse.body()?.properties?.list
        if (scheduleList.isNullOrEmpty()) return InfluxForPastTimeSpanOutputModel(arrayListOf(), 0)
        val groupSchedulesPerDay = scheduleList.groupBy { it.day }
        val distinctUsersSchedulesPerDay: LinkedHashMap<String, List<ScheduleInputModel>> = linkedMapOf()
        groupSchedulesPerDay.forEach { (key, value) ->
            distinctUsersSchedulesPerDay[key] = value.distinctBy { it.userId }
        }

        // Fill the days with no schedules
        val currentDay = Date()
        val lastWeek: LocalDate = currentDay.toInstant().atZone(ZoneId.systemDefault()).toLocalDate().minusWeeks(1)
        val monday = lastWeek.with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY))
        val sunday = lastWeek.with(TemporalAdjusters.nextOrSame(DayOfWeek.SUNDAY))

        val beginDay = monday.dayOfMonth
        val endDay = sunday.dayOfMonth

        var month: String
        var auxDay: String
        for (day in beginDay..endDay){
            month = monday.monthValue.toString()
            if (month.length == 1) month = "0$month"
            auxDay = day.toString()
            if (auxDay.length == 1) auxDay = "0$auxDay"

            val date = "${monday.year}-$month-$auxDay"
            if (!distinctUsersSchedulesPerDay.containsKey(date))
                distinctUsersSchedulesPerDay[date] = arrayListOf()
        }

        // Then, for each day, make the % of total collaborators that went to the office
        val daysOfTheWeekWithInflux : MutableList<DayInflux> = mutableListOf()
        val influxes : MutableList<Double> = mutableListOf()
        distinctUsersSchedulesPerDay.forEach { (key, value) ->
            val influx : Double = ( value.size.toDouble() / totalCollaboratorsCount!! ) * 100
            influxes.add(influx)
            daysOfTheWeekWithInflux.add(DayInflux(key, influx))
        }

        daysOfTheWeekWithInflux.sortBy { it.date }

        return InfluxForPastTimeSpanOutputModel(daysOfTheWeekWithInflux, influxes.average().toInt())
    }

    /**
     * Returns an object with the average % of the collaborators in relation to the total number of collaborators
     * that went to the office in the past month and, an array with each day and each % individually
     */
    fun getInfluxForPastMonth(): InfluxForPastTimeSpanOutputModel?{
        // Get past month schedules booked
        val getScheduleListRetrofitCall: Call<SirenEntity<ScheduleListInputModel>> = scheduleService.scheduleListPastMonth()
        val getScheduleListResponse: Response<SirenEntity<ScheduleListInputModel>> = getScheduleListRetrofitCall.execute()

        if (!getScheduleListResponse.isSuccessful) {
            when {
                getScheduleListResponse.code() == 400 -> throw InvalidArgumentException("Invalid arguments")
                else -> throw Exception()
            }
        }

        // Get total number of workers for the company
        val getAllCollaboratorsRetrofitCall: Call<SirenEntity<UsersListInputModel>> = userService.collaboratorsList()
        val getAllCollaboratorsResponse: Response<SirenEntity<UsersListInputModel>> = getAllCollaboratorsRetrofitCall.execute()

        if (!getAllCollaboratorsResponse.isSuccessful) {
            when {
                getAllCollaboratorsResponse.code() == 400 -> throw InvalidArgumentException("Invalid arguments")
                else -> throw Exception()
            }
        }

        val totalCollaboratorsCount = getAllCollaboratorsResponse.body()?.properties?.users?.size

        // Get all the different workers that made bookings on each day
        val scheduleList = getScheduleListResponse.body()?.properties?.list
        if (scheduleList.isNullOrEmpty()) return InfluxForPastTimeSpanOutputModel(arrayListOf(), 0)
        val groupSchedulesPerDay = scheduleList.groupBy { it.day }
        val distinctUsersSchedulesPerDay: LinkedHashMap<String, List<ScheduleInputModel>> = linkedMapOf()
        groupSchedulesPerDay.forEach { (key, value) ->
            distinctUsersSchedulesPerDay[key] = value.distinctBy { it.userId }
        }

        // Fill the days with no schedules
        val lastMonth: LocalDate = Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().minusMonths(1)
        val firstDay = lastMonth.with(TemporalAdjusters.firstDayOfMonth())
        val lastDay = lastMonth.with(TemporalAdjusters.lastDayOfMonth())

        val beginDay = firstDay.dayOfMonth
        val endDay = lastDay.dayOfMonth

        var month: String
        var auxDay: String
        for (day in beginDay..endDay){
            month = firstDay.monthValue.toString()
            if (month.length == 1) month = "0$month"
            auxDay = day.toString()
            if (auxDay.length == 1) auxDay = "0$auxDay"

            val date = "${firstDay.year}-$month-$auxDay"
            if (!distinctUsersSchedulesPerDay.containsKey(date))
                distinctUsersSchedulesPerDay[date] = arrayListOf()
        }

        // Then, for each day, make the % of total collaborators that went to the office
        val daysOfTheWeekWithInflux : MutableList<DayInflux> = mutableListOf()
        val influxes : MutableList<Double> = mutableListOf()
        distinctUsersSchedulesPerDay.forEach { (key, value) ->
            val influx : Double = ( value.size.toDouble() / totalCollaboratorsCount!! ) * 100
            influxes.add(influx)
            daysOfTheWeekWithInflux.add(DayInflux(key, influx))
        }

        daysOfTheWeekWithInflux.sortBy { it.date }

        return InfluxForPastTimeSpanOutputModel(daysOfTheWeekWithInflux, influxes.average().toInt())
    }

    /**
     * Returns
     */
    fun getRoomOccupancyForPastMonth(): RoomOccupancyListOutputModel? {
        // Get past month schedules booked
        val getScheduleListRetrofitCall: Call<SirenEntity<ScheduleListInputModel>> = scheduleService.scheduleListPastMonth()
        val getScheduleListResponse: Response<SirenEntity<ScheduleListInputModel>> = getScheduleListRetrofitCall.execute()

        if (!getScheduleListResponse.isSuccessful) {
            when {
                getScheduleListResponse.code() == 400 -> throw InvalidArgumentException("Invalid arguments")
                else -> throw Exception()
            }
        }

        val scheduleList = getScheduleListResponse.body()?.properties?.list
        if (scheduleList.isNullOrEmpty()) return RoomOccupancyListOutputModel(mutableListOf(), 0)

        val groupSchedulesPerRoom = scheduleList.groupBy { it.roomId }

        // Get all rooms
        val getRoomListRetrofitCall: Call<SirenEntity<RoomListInputModel>> = officeService.roomsList()
        val getRoomListResponse: Response<SirenEntity<RoomListInputModel>> = getRoomListRetrofitCall.execute()

        if (!getRoomListResponse.isSuccessful) {
            when {
                getScheduleListResponse.code() == 400 -> throw InvalidArgumentException("Invalid arguments")
                else -> throw Exception()
            }
        }

        val roomList = getRoomListResponse.body()?.properties?.rooms
        if (roomList.isNullOrEmpty()) return RoomOccupancyListOutputModel(mutableListOf(), 0)

        // Associate each room to the correspondent counter of bookings during the month
        val roomsWithOccupancyList: MutableList<RoomOccupancyOutputModel> = mutableListOf()
        roomList.map { room ->
            val aux = groupSchedulesPerRoom[room.id]
            var counter = 0
            if ( !aux.isNullOrEmpty() ) counter = aux.size

            roomsWithOccupancyList.add(
                RoomOccupancyOutputModel(
                    roomId = room.id,
                    roomName = room.roomName,
                    roomCapacity = room.roomCapacity,
                    isFullRoom = room.isFullRoom,
                    bookingsCounter = counter
                )
            )
        }

        return RoomOccupancyListOutputModel(roomsWithOccupancyList, scheduleList.size)
    }

    /**
     * ---------------------------- Aux methods --------------------------------------
     */

    fun getTotalCollaborators() {

    }

}