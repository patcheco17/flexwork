package com.example.statisticsapi

import com.example.common.SirenEntity
import com.example.statisticsapi.dto.RoomListInputModel
import retrofit2.Call
import retrofit2.http.GET

interface OfficeService {
    @GET("rooms")
    fun roomsList(): Call<SirenEntity<RoomListInputModel>>
}