package com.example.statisticsapi

class User (
    var userId: String,
    val firstName: String,
    val lastName: String,
    val email: String,
    val projectName: String?,
    val position: String?
)