package com.example.statisticsapi

import com.example.common.SirenEntity
import com.example.statisticsapi.dto.UsersListInputModel
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST

interface UserService {
    @GET("users")
    fun collaboratorsList(): Call<SirenEntity<UsersListInputModel>>

    @POST("users/ids")
    fun usersBySeveralIdsList(
        @Header("Authorization") accessToken: String,
        @Body usersIds: Array<String>
    ): Call<SirenEntity<UsersListInputModel>>


}