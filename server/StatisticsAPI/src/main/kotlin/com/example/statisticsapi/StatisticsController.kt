package com.example.statisticsapi

import ProblemJson
import com.example.common.*
import com.example.exceptions.*
import com.example.statisticsapi.dto.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.http.converter.HttpMessageNotReadableException
import org.springframework.web.bind.annotation.*

@RestController
@CrossOrigin(origins = arrayOf("*"))
class StatisticsController @Autowired constructor(private val statisticsServices: StatisticsServices){
    /**
     * Get collaborators count for company
     */

    @CrossOrigin(origins = arrayOf("*"))
    @GetMapping(COLLABORATORS_COUNT)
    @ResponseBody
    fun getCollaboratorsCount(@RequestHeader("authorization") token: String): Int? {
        return statisticsServices.getCollaboratorsCount()
    }

    /**
     * Get collaborators in office for current day
     */
    @CrossOrigin(origins = arrayOf("*"))
    @GetMapping(COLLABORATORS_IN_OFFICE)
    @ResponseBody
    fun getCollaboratorsInOfficeForCurrentDay(
        @RequestHeader("authorization") token: String
    ): SirenEntity<UsersInOfficeListOutputModel>? {
        val collaboratorsInOfficeCurrentDay: UsersInOfficeListOutputModel? =
            statisticsServices.getCollaboratorsInOfficeCurrentDay(token)
        return collaboratorsInOfficeCurrentDay?.toSirenObject(
            links = listOf(selfLink("/collaborators-in-office/")),
            actions = null
        )
    }

    /**
     * Get collaborators influx for past week
     */
    @CrossOrigin(origins = arrayOf("*"))
    @GetMapping(INFLUX_PAST_WEEK)
    @ResponseBody
    fun getInfluxForPastWeek(
        @RequestHeader("authorization") token: String
    ): SirenEntity<InfluxForPastTimeSpanOutputModel>? {
        val influxForPastWeek: InfluxForPastTimeSpanOutputModel? = statisticsServices.getInfluxForPastWeek()
        return influxForPastWeek?.toSirenObject(
            links = listOf(selfLink("/occupancy/week")),
            actions = null
        )
    }


    /**
     * Get collaborators influx for past month
     */
    @CrossOrigin(origins = arrayOf("*"))
    @GetMapping(INFLUX_PAST_MONTH)
    @ResponseBody
    fun getInfluxForPastMonth(
        @RequestHeader("authorization") token: String
    ): SirenEntity<InfluxForPastTimeSpanOutputModel>? {
        val influxForPastMonth: InfluxForPastTimeSpanOutputModel? = statisticsServices.getInfluxForPastMonth()
        return influxForPastMonth?.toSirenObject(
            links = listOf(selfLink("/occupancy/month")),
            actions = null
        )
    }

    /**
     * Get rooms occupancy for past month
     */
    @CrossOrigin(origins = arrayOf("*"))
    @GetMapping(ROOMS_OCCUPANCY_PAST_MONTH)
    @ResponseBody
    fun getRoomsOccupancyForPastMonth(
        @RequestHeader("authorization") token: String
    ): SirenEntity<RoomOccupancyListOutputModel>? {
        val occupancyForPastMonth: RoomOccupancyListOutputModel? =
            statisticsServices.getRoomOccupancyForPastMonth()
        return occupancyForPastMonth?.toSirenObject(
            links = listOf(selfLink("/room-occupancy/month")),
            actions = null
        )
    }



    @ExceptionHandler(value = [
        InvalidArgumentException::class,
        HttpMessageNotReadableException::class])
    fun handle400Exception(e : Exception) = ResponseEntity
        .badRequest()
        .contentType(MediaType.APPLICATION_PROBLEM_JSON)
        .body(
            when(e::class){
                InvalidArgumentException::class -> ProblemJson(
                    type = "/users",
                    title = "Bad Request",
                    detail = "${e.message}",
                    status = 400
                )
                HttpMessageNotReadableException::class -> ProblemJson(
                    type = "/users",
                    title = "Bad Request",
                    detail = "Invalid value inserted",
                    status = 400
                )
                else -> null
            }
        )

    @ExceptionHandler(value = [
        DataBaseException::class ,
        NoDataBaseSelectedException::class])
    fun handle500Exception(e : Exception) = ResponseEntity
        .internalServerError()
        .contentType(MediaType.APPLICATION_PROBLEM_JSON)
        .body(
            when(e::class){
                DataBaseException::class -> ProblemJson(
                    type = "/users",
                    title = "Database Error",
                    detail = "Something went wrong when fetching data from database",
                    status = 500
                )
                NoDataBaseSelectedException::class -> ProblemJson(
                    type = "/users",
                    title = "Database Error",
                    detail = "Could not establish connection to the database",
                    status = 500
                )
                else -> null
            }
        )

    @ExceptionHandler(value = [Exception::class])
    fun handleException(e : Exception) = ResponseEntity
        .badRequest()
        .contentType(MediaType.APPLICATION_PROBLEM_JSON)
        .body(
            when(e::class){
                DataBaseException::class -> ProblemJson(
                    type = "/users",
                    title = "Internal Server Error",
                    detail = "Something went wrong",
                    status = 500
                )
                else -> null
            }
        )
}