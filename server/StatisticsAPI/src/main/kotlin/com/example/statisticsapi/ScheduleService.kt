package com.example.statisticsapi

import com.example.common.SirenEntity
import com.example.statisticsapi.dto.ScheduleListInputModel
import retrofit2.Call
import retrofit2.http.GET

interface ScheduleService {
    @GET("schedule/day")
    fun scheduleList(): Call<SirenEntity<ScheduleListInputModel>>

    @GET("schedule/past-week")
    fun scheduleListPastWeek(): Call<SirenEntity<ScheduleListInputModel>>

    @GET("schedule/past-month")
    fun scheduleListPastMonth(): Call<SirenEntity<ScheduleListInputModel>>
}