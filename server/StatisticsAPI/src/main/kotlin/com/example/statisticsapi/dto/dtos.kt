package com.example.statisticsapi.dto

import com.example.common.SirenAction
import com.example.common.SirenEntity
import com.example.common.SirenLink
import com.example.exceptions.InvalidArgumentException
import com.example.statisticsapi.User
import com.fasterxml.jackson.annotation.JsonCreator

class UserInputModel @JsonCreator constructor(
    private val userId: String,
    private val firstName: String,
    private val lastName: String,
    private val email: String,
    private val projectName: String?,
    private val position: String?
)  {
    fun toUser(): User {
        if (userId.isEmpty() || userId.isBlank() ||
            firstName.isEmpty() || firstName.isBlank() ||
            lastName.isEmpty() || lastName.isBlank() ||
            email.isEmpty() || email.isBlank())
                throw InvalidArgumentException("All arguments must have valid input")

        return User(
            userId,
            firstName,
            lastName,
            email,
            projectName,
            position
        )
    }

    fun toUserInOfficeOutputModel(): UserInOfficeOutputModel {
        return UserInOfficeOutputModel(
            userId,
            firstName,
            lastName,
            projectName,
            position
        )
    }
}

class UsersListInputModel(val users: MutableList<UserInputModel>?)

class UserInOfficeOutputModel @JsonCreator constructor(
    val userId: String,
    val firstName: String,
    val lastName: String,
    val projectName: String?,
    val position: String?
)

class UsersInOfficeListOutputModel(val users: MutableList<UserInOfficeOutputModel>?)

fun UsersInOfficeListOutputModel.toSirenObject(links: List<SirenLink>, actions: List<SirenAction>? = null) = SirenEntity(
    properties = this,
    clazz = listOf("users"),
    links = links,
    actions = actions
)

/**
 * Response from Schedule Service with all bookings made for current day
 */
class ScheduleInputModel @JsonCreator constructor(
    val bookingId: Int,
    val userId: String,
    val roomId: Int,
    val day: String
)

fun ScheduleInputModel.toSirenObject(links: List<SirenLink>, actions: List<SirenAction>? = null) = SirenEntity(
    properties = this,
    clazz = listOf("Schedule"),
    links = links,
    actions = actions
)

class ScheduleListInputModel(val list: MutableList<ScheduleInputModel>?, val size: Int)

fun ScheduleListInputModel.toSirenObject(links: List<SirenLink>, actions: List<SirenAction>? = null) = SirenEntity(
    properties = this,
    clazz = listOf("Schedule"),
    links = links,
    actions = actions
)


/**
 *  DTO's related to past week and month influx of workers
 */
class DayInflux @JsonCreator constructor(
    val date: String,
    val occupancy: Double
)

class InfluxForPastTimeSpanOutputModel @JsonCreator constructor(
    val days: MutableList<DayInflux>?,
    val timeSpanAverage: Int
)

fun InfluxForPastTimeSpanOutputModel.toSirenObject(links: List<SirenLink>, actions: List<SirenAction>? = null) = SirenEntity(
    properties = this,
    clazz = listOf("influxForWeek"),
    links = links,
    actions = actions
)


/**
 * Response from Office Management Service that represents a room
 */
class RoomInputModel @JsonCreator constructor(
    val id: Int,
    val roomName: String,
    val roomCapacity: Int,
    val isFullRoom: Boolean
)

/**
 * Response from Office Management Service that represents a list of rooms
 */
class RoomListInputModel(val rooms: MutableList<RoomInputModel>?)

/**
 * --------------------------------------------------------------------------------------------------
 * DTO's related to the occupancy for past month
 */

class RoomOccupancyOutputModel @JsonCreator constructor(
    val roomId: Int,
    val roomName: String,
    val roomCapacity: Int,
    val isFullRoom: Boolean,
    val bookingsCounter: Int
)

class RoomOccupancyListOutputModel(val rooms: MutableList<RoomOccupancyOutputModel>?, val totalBookings: Int)

fun RoomOccupancyListOutputModel.toSirenObject(links: List<SirenLink>, actions: List<SirenAction>? = null) = SirenEntity(
    properties = this,
    clazz = listOf("roomsOccupancyForMonth"),
    links = links,
    actions = actions
)


