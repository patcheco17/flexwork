package com.example.HoursBankAPI

import com.google.gson.GsonBuilder
import common.USERS_MANAGEMENT_API_URL
import exceptions.InternalServiceException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*

@Service
class BankServices {
    private lateinit var bankRepo: BankRepository
    private lateinit var service: UserManagementService

    @Autowired
    fun bankServices(bankRepo: BankRepository) {
        this.bankRepo = bankRepo

        val gson = GsonBuilder()
            .setLenient()
            .create()

        val retrofit = Retrofit.Builder()
            .baseUrl(USERS_MANAGEMENT_API_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()

        service = retrofit.create(UserManagementService::class.java)
    }

    fun postTask(task: Task): TaskOutputModelList? {
        return bankRepo.add(task)
    }

    fun getTasksFromASpecificWeek(date: Date, userId: String): TaskOutputModelList? {
        return bankRepo.getTasksFromASpecificWeek(date, userId)
    }

    fun deleteTask(taskId: Int): Int {
        return bankRepo.deleteTask(taskId)
    }

    fun editTask(task: HashMap<String, String>, taskId: Int) {
        bankRepo.editTask(task, taskId)
    }

    fun getBalanceFromASpecificMonth(date: Date): AllUsersOutputModel? {

        try {
            val call = service.usersList()
            val response = call.execute()

            if (!response.isSuccessful) {
                //what to do here?
            }
            val usersList = response.body().properties?.users
            val balanceList = bankRepo.getBalanceFromASpecificMonth(date).users

            usersList?.forEach { user ->
                user.timeWorked = balanceList?.find { u -> u.userId == user.userId }?.hoursWorked ?: 0
            }
            return AllUsersOutputModel(usersList)


        } catch (e: Exception) {
            throw InternalServiceException("Could not establish connection to the Users Management server")
        }
    }

    /**
     * Get number of hours worked bu the user in current month
     */
    fun getUserBalanceFromSpecificMonth(date: Date, userId: String): BalanceOutputModel? {
        val balanceList = bankRepo.getBalanceFromASpecificMonth(date).users
        if (balanceList.isNullOrEmpty()) return BalanceOutputModel(userId, 0)
        return balanceList.find { balance -> balance.userId.equals(userId) } ?: BalanceOutputModel(userId, 0)
    }

}
