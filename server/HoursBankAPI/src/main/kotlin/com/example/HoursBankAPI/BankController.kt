package com.example.HoursBankAPI

import ProblemJson
import common.*
import exceptions.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.format.annotation.DateTimeFormat
import org.springframework.http.HttpMethod
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.net.URI
import java.util.*
import javax.servlet.http.HttpServletRequest


@RestController
@CrossOrigin(origins = arrayOf("*"))
class BankController {
    private lateinit var bankServices: BankServices

    @RequestMapping
    @Autowired
    fun bankController(_bankServices: BankServices) {
        bankServices = _bankServices
    }

    @CrossOrigin(origins = arrayOf("*"))
    @PostMapping(TASKS)
    @ResponseBody
    fun createTask(
        req: HttpServletRequest,
        @RequestBody newTask: TaskInputModel
    ): SirenEntity<TaskOutputModelList>? {
        val task = newTask.toTask()
        val outputModel: TaskOutputModelList = bankServices.postTask(task) ?: throw Exception()
        return outputModel.toSirenObject(
            links = listOf(selfLink((TASKS)),  SirenLink(rel = listOf("/tasks/rel/weekly-tasks"), href = URI(TASKS_WEEKLY))),
            actions = listOf(
                SirenAction("DELETE", URI(TASKS), "Delete task", listOf("tasks"), HttpMethod.DELETE),
                SirenAction("PATCH", URI(TASKS), "Edit task name", listOf(TASKS), HttpMethod.PATCH),
                SirenAction("GET", URI(TASKS_WEEKLY), "Get weekly tasks", listOf(TASKS_WEEKLY), HttpMethod.GET),
            )
        )
    }

    @CrossOrigin(origins = arrayOf("*"))
    @PatchMapping(TASKS_ID)
    @ResponseBody
    fun editTask(@RequestBody task: HashMap<String, String>, @PathVariable taskId: Int) {
        val userId = task["userId"]
        val taskName = task["taskName"]

        if(taskId == -1) {
            throw TaskNotFoundException("Task id not found")
        }

        if(taskName == null){
            throw InvalidValueInsertedException("Task name not valid")
        }

        if(taskName.length > 12){
            throw InvalidValueInsertedException("Task name can't exceed 12 characters.")
        }

        bankServices.editTask(task, taskId)
    }

    @CrossOrigin(origins = arrayOf("*"))
    @DeleteMapping(TASKS_ID)
    @ResponseBody
    fun deleteTask(req: HttpServletRequest, @PathVariable taskId: Int): Int {
        return bankServices.deleteTask(taskId)
    }

    //Get weekly hours from a specific week
    @CrossOrigin(origins = arrayOf("*"))
    @GetMapping(TASKS_WEEKLY)
    @ResponseBody
    fun getTasksWeekly(
        @RequestParam("date")
        @DateTimeFormat(pattern = "yyyy-MM-dd") date: Date,
        @RequestParam("userId") userId: String
    ): SirenEntity<TaskOutputModelList>? {
        return bankServices.getTasksFromASpecificWeek(date, userId)
            ?.toSirenObject(
                links = listOf(selfLink((TASKS_WEEKLY))),
                actions = listOf(
                    SirenAction("delete-task", URI(TASKS), "Delete task", listOf(TASKS), HttpMethod.DELETE),
                    SirenAction("post-task", URI(TASKS), "Post task", listOf(TASKS), HttpMethod.POST),
                    SirenAction("edit-task-name", URI(TASKS), "Edit task name", listOf(TASKS), HttpMethod.PATCH),
                )
            )
    }


    /**
     * Admin use these resources
     *
     * */
    //Get weekly hours from a specific week
    @CrossOrigin(origins = arrayOf("*"))
    @GetMapping(BALANCE_MONTHLY)
    @ResponseBody
    fun getBalanceFromASpecificMonth(
        @RequestParam("date")
        @DateTimeFormat(pattern = "yyyy-MM-dd") date: Date
    ): SirenEntity<AllUsersOutputModel>? {
        return bankServices.getBalanceFromASpecificMonth(date)
            ?.toSirenObject(
                links = listOf(selfLink((BALANCE_MONTHLY))),
                actions = null
            )
    }

    @ExceptionHandler(value = [InvalidValueInsertedException::class])
    fun handleBadRequestException(e: Exception) = ResponseEntity
        .badRequest()
        .contentType(MediaType.APPLICATION_PROBLEM_JSON)
        .body(
            ProblemJson(
                type = "/tasks/bad-request",
                title = "BAD REQUEST",
                detail = e.message.toString(),
                status = 400
            )
        )

    @ExceptionHandler(value = [TaskNotFoundException::class])
    fun handleNotFoundException(e: Exception) = ResponseEntity.status(404).body(
        ProblemJson(
            type = "/tasks/not-found",
            title = "NOT FOUND",
            detail = e.message.toString(),
            status = 404
        )
    )


    @ExceptionHandler(
        value = [
            DataBaseException::class,
            NoDataBaseSelectedException::class,
            InternalServiceException::class
        ]
    )
    fun handle500Exceptions(e: Exception) = ResponseEntity
        .internalServerError()
        .contentType(MediaType.APPLICATION_PROBLEM_JSON)
        .body(
            when (e::class) {

                DataBaseException::class -> ProblemJson(
                    type = "/tasks",
                    title = "Database Error",
                    detail = "Something went wrong when fetching data from database",
                    status = 500
                )

                NoDataBaseSelectedException::class -> ProblemJson(
                    type = "/tasks",
                    title = "Database Error",
                    detail = "Could not establish connection to the database",
                    status = 500
                )

                InternalServiceException::class -> ProblemJson(
                    type = "/tasks",
                    title = "Internal Error",
                    detail = e.message.toString(),
                    status = 500
                )

                else -> null
            }

        )

}