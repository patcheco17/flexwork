package com.example.HoursBankAPI

import java.time.LocalDate
import java.time.LocalTime

class Task(
    val userId : String,
    val taskName: String,
    val startTime: LocalTime,
    val finishTime: LocalTime,
    val day: LocalDate,
    val hours: Long
)