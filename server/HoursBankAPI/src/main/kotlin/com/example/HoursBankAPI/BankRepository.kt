package com.example.HoursBankAPI

import exceptions.InvalidValueInsertedException
import exceptions.TaskNotFoundException
import org.jdbi.v3.core.Handle
import org.jdbi.v3.core.Jdbi
import org.springframework.stereotype.Repository
import sqlcommands.*
import java.time.DayOfWeek
import java.time.LocalDate
import java.time.ZoneId
import java.time.temporal.TemporalAdjusters
import java.time.temporal.TemporalAdjusters.lastDayOfMonth
import java.util.*


@Repository
class BankRepository (var jdbi: Jdbi?) {
        fun add (task: Task): TaskOutputModelList? {
            var id:  Int  = -1

            val now: LocalDate = task.day
            val first = now.with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY))
            val last = now.with(TemporalAdjusters.nextOrSame(DayOfWeek.SUNDAY));

            val tasks: MutableList<TaskOutputModel>? = jdbi?.withHandle<MutableList<TaskOutputModel>?, RuntimeException>{ handle: Handle ->

                val listOfTasks = handle.createQuery(CHECK_OVERLAPED_TASK)
                    .bind(0, task.userId)
                    .bind(1, task.day)
                    .bind(2, task.finishTime)
                    .bind(3, task.startTime)
                    .mapTo(TaskOutputModel::class.java)
                    .list()

                if(listOfTasks.size > 0) throw InvalidValueInsertedException("Overlaped task")

                handle.createUpdate(INSERT_TASK)
                    .bind(0, task.userId)
                    .bind(1, task.taskName)
                    .bind(2, task.startTime)
                    .bind(3,  task.finishTime)
                    .bind(4, task.day)
                    .bind(5, task.hours)
                    .execute()

                handle.createQuery(GET_ALL_WEEK_TASKS)
                    .bind(0, task.userId)
                    .bind(1, first)
                    .bind(2, last)
                    .mapTo(TaskOutputModel::class.java)
                    .list()
            }

             return TaskOutputModelList(tasks)
        }

    fun getTasksFromASpecificWeek(date: Date, userId : String): TaskOutputModelList? {
        val now: LocalDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate()
        val first = now.with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY))
        val last = now.with(TemporalAdjusters.nextOrSame(DayOfWeek.SUNDAY));



        val tasks: MutableList<TaskOutputModel>? = jdbi?.withHandle<MutableList<TaskOutputModel>?, RuntimeException>{ handle: Handle ->
            handle.createQuery(GET_ALL_WEEK_TASKS)
                .bind(0, userId)
                .bind(1, first)
                .bind(2, last)
                .mapTo(TaskOutputModel::class.java).list()
        }
        return TaskOutputModelList(tasks)
    }

    fun getHoursWorked(tasks : MutableList<TaskOutputModel>? ) : Long {
        var hoursWorked : Long = 0
        tasks?.forEach { task ->  hoursWorked += task.hoursWorked!! }
        return hoursWorked
    }

    fun deleteTask(taskId: Int): Int {

        val operationId = (jdbi?.withHandle<Int, RuntimeException>{ handle: Handle ->
            handle.createUpdate(DELETE_TASK)
                .bind(0, taskId)
                .execute()
        })

        if(operationId == 0) throw TaskNotFoundException("Task id not found");

        return taskId
    }

    fun editTask(task : HashMap<String, String>, taskId: Int) {
        val userId = task["userId"]
        val taskName = task["taskName"]

        if(taskId == -1) {
            throw TaskNotFoundException("Task id not found")
        }

        if(taskName == null){
            throw InvalidValueInsertedException("Task name not valid")
        }


        val id = jdbi?.withHandle<Int, RuntimeException> { handle: Handle ->
               handle.createUpdate( EDIT_TASK)
                   .bind(0, taskName)
                   .bind(1, userId)
                   .bind(2, taskId)
                   .execute()
        }

        if(id == 0){
            throw TaskNotFoundException("The task with id $taskId was not found")
        }
    }

    fun getBalanceFromASpecificMonth(date: Date) : BalanceOutputModelList {
        val now = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate()
        val firstDayOfMonth = now.withDayOfMonth(1)
        val lastDayOfMonth = now.with(lastDayOfMonth())

        val list: MutableList<BalanceOutputModel>? = jdbi?.withHandle<MutableList<BalanceOutputModel>?, RuntimeException>{ handle: Handle ->
            handle.createQuery(GET_WORKERS_BALANCE)
                .bind(0, firstDayOfMonth)
                .bind(1, lastDayOfMonth)
                .mapTo(BalanceOutputModel::class.java).list()
        }
        return BalanceOutputModelList(list)
    }
}
