package com.example.HoursBankAPI

import org.jdbi.v3.core.Jdbi
import org.jdbi.v3.core.kotlin.KotlinPlugin
import org.jdbi.v3.sqlobject.SqlObjectPlugin
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import javax.sql.DataSource



@Configuration
class JdbiConfiguration {
	@Bean
	fun jdbi(datasource: DataSource): Jdbi {
		return Jdbi.create(datasource)
			.installPlugin(SqlObjectPlugin())
			.installPlugin(KotlinPlugin())
	}
}

@SpringBootApplication
class HoursBankApiApplication


fun main(args: Array<String>) {
	runApplication<HoursBankApiApplication>(*args)
}
