package com.example.HoursBankAPI

class User (
    var userId: String?,
    val firstName: String?,
    val lastName: String?,
    val email: String?,
    val password: String?,
    val address: String?,
    val description: String?,
    var projectName: String?,
    val position: String?,
    val role: String?,
    var timeWorked : Long?
)