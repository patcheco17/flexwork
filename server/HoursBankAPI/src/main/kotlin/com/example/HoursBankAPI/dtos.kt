package com.example.HoursBankAPI

import com.fasterxml.jackson.annotation.JsonCreator
import common.SirenAction
import common.SirenEntity
import common.SirenLink
import exceptions.InvalidValueInsertedException
import java.time.Duration
import java.time.LocalDate
import java.time.LocalTime
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeParseException
import java.time.format.FormatStyle
import java.util.*


class TaskInputModel @JsonCreator constructor(
    private val userId: String,
    private val taskName: String,
    private val startTime: String,
    private val finishTime: String,
    private val day: String)  {

    fun toTask(): Task {
        val date: LocalDate;
        try{
            date = LocalDate.parse(day, DateTimeFormatter.ISO_DATE)
        }catch(e : DateTimeParseException){
            throw InvalidValueInsertedException("Invalid date format")
        }

        if(taskName.length > 12){
            throw InvalidValueInsertedException("Task name can't exceed 12 characters.")
        }

        val localStartTime = LocalTime.parse(startTime, DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT).withLocale(
            Locale.FRANCE))
        val localFinishTime= LocalTime.parse(finishTime, DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT).withLocale(
            Locale.FRANCE))

        if(localFinishTime.isBefore(localStartTime)) throw InvalidValueInsertedException("Invalid time inserted. Finish time is before start time!")
        val timeWorked = Duration.between(localStartTime, localFinishTime)

        return Task(
            userId,
            taskName,
            localStartTime,
            localFinishTime,
            date,
            timeWorked.toMinutes()
            )
    }
}


class TaskOutputModel (
            val userId: String?,
            val taskId: Int?,
            val taskName: String?,
            val startTime: LocalTime?,
            val finishTime: LocalTime?,
            val day: LocalDate?,
            val hoursWorked: Long) {
}


class TaskOutputModelList(val tasks: MutableList<TaskOutputModel>?)

class BalanceOutputModel(val userId: String?, val hoursWorked: Long)

fun BalanceOutputModel.toSirenObject(links: List<SirenLink>, actions: List<SirenAction>? = null) = SirenEntity(
    properties = this,
    clazz = listOf("Balance"),
    links = links,
    actions = actions
)

class BalanceOutputModelList(val users: MutableList<BalanceOutputModel>?)

fun BalanceOutputModelList.toSirenObject(links: List<SirenLink>, actions: List<SirenAction>? = null) = SirenEntity(
    properties = this,
    clazz = listOf("Balance"),
    links = links,
    actions = actions
)


fun TaskOutputModelList.toSirenObject(links: List<SirenLink>, actions: List<SirenAction>? = null) = SirenEntity(
    properties = this,
    clazz = listOf("Task"),
    links = links,
    actions = actions
)

fun TaskOutputModel.toSirenObject(links: List<SirenLink>, actions: List<SirenAction>? = null) = SirenEntity(
        properties = this,
        clazz = listOf("Task"),
        links = links,
        actions = actions
)

class AllUsersOutputModel(val users: MutableList<User>?)

/**
 * Extension function that produces the Siren version of the resource's external representation
 *
 * @param [links]  the list of links to be included.
 * @param [actions]  the list of actions to be included.
 * @return The corresponding siren representation
 */
fun AllUsersOutputModel.toSirenObject(links: List<SirenLink>, actions: List<SirenAction>? = null) = SirenEntity(
    properties = this,
    clazz = listOf("User"),
    links = links,
    actions = actions
)

