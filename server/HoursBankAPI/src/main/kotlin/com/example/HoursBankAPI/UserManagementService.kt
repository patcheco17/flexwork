package com.example.HoursBankAPI

import common.SirenEntity
import retrofit2.Call
import retrofit2.http.GET

interface UserManagementService {

    @GET("users")
    fun usersList(): Call<SirenEntity<AllUsersOutputModel>>

}