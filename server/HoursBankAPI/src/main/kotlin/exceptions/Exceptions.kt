package exceptions

class InvalidArgumentException(message: String) : Exception(message)

class RoomNameAlreadyExistsException(message: String) : Exception(message)

class DataBaseException(message: String) : Exception(message)

class NoDataBaseSelectedException(message: String) : Exception(message)

class InvalidValueInsertedException(message: String) : Exception(message)

class TaskNotFoundException(message: String) : Exception(message)

class TaskNotModifiedException(message: String) : Exception(message)

class InternalServiceException(message: String) : Exception(message)