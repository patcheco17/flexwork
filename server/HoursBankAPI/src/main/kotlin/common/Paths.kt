package common

    const val TASKS = "/tasks"
    const val TASKS_ID = "/tasks/{taskId}"
    const val TASKS_WEEKLY = "/tasks/weekly"
    const val BALANCE_MONTHLY= "/balance/monthly"
    const val BALANCE_MONTHLY_USER= "/balance/monthly/user"
    const val CLIENT_URL = "https://frontend-flexwork-4hupxege7q-uc.a.run.app"
    const val USERS_MANAGEMENT_API_URL= "https://user-management-api-4hupxege7q-uc.a.run.app/"



