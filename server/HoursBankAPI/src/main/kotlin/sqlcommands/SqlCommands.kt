package sqlcommands

const val CREATE_HOURS_BANK_TABLE =
    "Create table bank( " +
            "taskid serial, " +
            "taskName varchar(12), " +
            "starttime time, " +
            "finishtine time, " +
            "day date, " +
            "hoursWorked long," +
            "PRIMARY KEY (taskid) );"

const val INSERT_TASK = "insert into bank (userId, taskName, startTime, finishTime, day, hoursWorked) values (?, ?, ?, ?, ?, ?)"
const val CHECK_OVERLAPED_TASK = "SELECT * FROM bank WHERE userId = ? AND day = ? AND startTime <= ? AND finishTime >= ?"
const val GET_ALL_WEEK_TASKS = "SELECT * FROM bank WHERE userId = ? AND day BETWEEN ? AND ? ORDER BY startTime asc"
const val DELETE_TASK = "DELETE FROM bank WHERE taskId = ?;"
const val EDIT_TASK = "UPDATE bank SET taskName= ? WHERE userId= ? AND taskId=?;"
const val GET_WORKERS_BALANCE = "select userId, sum(hoursWorked) as hoursWorked from bank WHERE day BETWEEN ? AND ? group by userId "