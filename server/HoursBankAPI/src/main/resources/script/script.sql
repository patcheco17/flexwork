use bank_hours

create table bank (
                      userId varchar(50),
                      taskId serial PRIMARY KEY,
                      taskName varchar(12),
                      startTime Time,
                      finishTime Time,
                      day Date,
                      hoursWorked int
)

drop table bank
