use office_management_db;

drop table rooms_management;

CREATE TABLE rooms_management (
                                  id int not null AUTO_INCREMENT,
                                  roomName VARCHAR(100) NOT NULL ,
                                  roomCapacity INT NULL,
                                  isFullRoom bit,
                                  constraint roomName unique (roomName),
                                  PRIMARY KEY (id)
);


-----------------------------------------------------


use schedule_db;

drop table booking;

create table booking (
                         bookingId serial primary key,
                         userId varchar(50),
                         startTime Time,
                         finishTime Time,
                         day Date,
                         roomId int
);


select * from booking;

-----------------------------------------------------

USE user_management_db;

DROP TABLE user;

CREATE TABLE user (
                      userId VARCHAR(50) NOT NULL,
                      firstName VARCHAR(50) NOT NULL,
                      lastName VARCHAR(50) NOT NULL,
                      email VARCHAR(50) NOT NULL UNIQUE,
                      address VARCHAR(100),
                      description VARCHAR(250),
                      projectName VARCHAR(50),
                      position VARCHAR(50),
                      PRIMARY KEY (userId)
);

----------------------------------------------------

USE hours_bank_db;

drop table bank ;

create table bank (
                      userId varchar(50),
                      taskId serial PRIMARY KEY,
                      taskName varchar(12),
                      startTime Time,
                      finishTime Time,
                      day Date,
                      hoursWorked float
)


















